#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *innerText(char *inner, char *pText, const char *beginMark, const char *endMark) {
     char *beginStart = strstr(pText, beginMark);
     if (beginStart == NULL) return NULL;
     char *beginEnd = beginStart + strlen(beginMark);
     char *endStart = strstr(beginEnd, endMark);
     if (endStart < 0) return NULL;
     int len = endStart-beginEnd;
     strncpy(inner, beginEnd, len);
     inner[len] = '\0';
}

char* fileToStr(const char *fileName) {
  FILE *file = fopen(fileName, "rb");
  if (file == NULL) return NULL;
  fseek(file, 0 , SEEK_END);
  long size = ftell(file);
  rewind(file);
  char *buffer = (char*) malloc(size+1);
  fread (buffer,size,1,file);
  fclose(file);
  return buffer;
}

