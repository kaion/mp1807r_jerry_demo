#ifndef __RUN_ECC_API_H
#define __RUN_ECC_API_H

char runEccSignOnApi(unsigned int NU);
char runEccReadCardBasicApi(unsigned int NU, char *T0215);
char runEccDeductApi(unsigned int NU, unsigned int T0400, char *T0215, char *T4110, char *T5501, char *T3700, char *T0415, char *T0409, char *T0410);
char runEccSettleApi(unsigned int NU, char *T5501, char *T3912);
void runEccCheckFolder(void);

#endif

