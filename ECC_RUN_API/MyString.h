#ifndef MYSTRING_H_
#define MYSTRING_H_

#include "SetupFile.h"
#include "MyMem.h"
class CStrType {
	private:
		CMem* iMem ;

		// 也可用 binary //
		UChar* i_new_bin ;
		ULInt i_sz ;

		// for binary to string //
		char* i_new_hex_str ;

	public:
		CStrType ( ) ;
		~CStrType ( ) ;

		int Clear ( void ) ;

		// like memset //
		int Assign ( CUInt size , CUChar c ) ;

		// memcpy , 記得在第三個引數加上 ( void* ) //
		/*
		 * CUInt beginNum : 原資料要開始接續的位置
		 * CUInt size     : forceStr 的大小
		 * void* forceStr :
		 */
		int Assign ( CUInt beginNum , CInt size , void* forceStr ) ;

		// snprintf() //
		int Assign ( CChar* format , ... ) ;

		void Cpy ( CStrType* obj ) ;
		void Cpy ( CStr str ) ;
		void Cpy ( CUChar* bin , int binLen ) ;

		void Cat ( CStrType* obj ) ;
		void Cat ( CStr str ) ;
		void Cat ( CUChar* bin , int binLen ) ;

		int Cmp ( CStrType* obj ) ;
		int Cmp ( UChar* bin , int binLen ) ;
		int Cmp ( CStr str ) ;

		// 2 char to 1 value
		int StrToBin ( Str str ) ;   // source
		int Str_to_bin ( Str str ) ; // 2016 04 14

		int Size ( void ) ;

	private:
		void Change_order ( void* data , int byte ) ;

		Str i_NULL_respon ;
	public:
		void Change_order ( void ) ;

		UChar* RtnStr ( void ) ;
		UChar* Rtn_bin ( void ) ;

		// 將 this->nwStr 用 hex string 的方式顯示 //
		char* RtnNewHexStr ( void ) ;
		char* Rtn_hex_str ( void ) ;

		char* RtnNewHexStrLower ( void ) ;
		char* Rtn_hex_to_str_lower_change_order ( void ) ;
} ;

#if 0
e.g.
CStrType obj ;

int a = obj.Assign ( 0 , 11 ,(void*)"@Test@#$%^&" ) ;
std::cout<< "a:" << a <<std::endl ;
std::cout<< obj.RtnStr() << std::endl ;
std::cout<< obj.RtnNewHexStr() << std::endl ;
std::cout<<"Sz:" << obj.Size() << std::endl<< std::endl ;

a = obj.Assign ( obj.Size() , 11 ,(void*)"1234567890+" ) ;
std::cout<< "a:" << a <<std::endl ;
std::cout<< obj.RtnStr() << std::endl ;
std::cout<< obj.RtnNewHexStr() << std::endl ;
std::cout<<"Sz:" << obj.Size() << std::endl<< std::endl ;

while(1) {
//		obj.Assign("123:%d ,%s@@@################################################\n" ,222 ,"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
	int a = obj.Assign(100000 ,0x11) ;
//		std::cout<< obj.RtnStr()<<std::endl;
	std::cout<< obj.RtnNewHexStr() << std::endl ;
	std::cout<< obj.Size() << std::endl ;
	std::cout<< "a:" << a <<std::endl ;
	usleep(100) ;
}
#endif

class CMyFmt {
	public:
		Str iKey ;
		Str iValue ;
} ;

class CMyStrPar {
	private:
		CMem* iMem ;

	public:
		CMyStrPar ( ) ;
		~CMyStrPar ( ) ;

#if 0
		-c ip
		-t pgm run time
		-i next write wait time ms
		-w send 封包大小 byte
#endif
		char* ParserParameter ( char**argv , char* key ) ;
		bool Have_parameter ( char**argv , char* key ) ;   // 是否有使用該參數
		int Cp_file ( Str sour_file , Str target_file ) ;

	private:
		Str iReadFile ;
	public:
		int ReadFile ( CStr filePath ) ;   // source
		Str RtnReadFile ( void ) ;
		Str Read_file_data ( Str file_path ) ;   // 2016 04 11



		// 處理內含有 0x00 的資料
	private:
		CStrType* iReadFileBin ;
	public:
		int ReadFileBin ( CStr filePath ) ;   // source
		CStrType*& RtnReadFileBin ( void ) ;  // source 記得要宣告 reference variable 續承接
		CStrType* Read_file_bin ( CStr file_path ) ;  // 2016 04 11

		/*
		 * write str to file
		 */
		int Write_str_to_file_w ( CStr file_path , Str& data ) ;
		int Write_str_to_file_a ( CStr file_path , Str& data ) ;

		/*
		 * write bin to file
		 */
		int Write_bin_to_file_w ( CStr file_path , void* p_data , int sz ) ;
		int Write_bin_to_file_a ( CStr file_path , void* p_data , int sz ) ;

		LInt Rtn_file_sz ( CStr file_path ) ;
		int Write_to_file_clamp_sz ( CStr file_path , int clamp_sz , Str& data ) ;


		/*
		 * 簡易型
		 * <S>0000001F{"a":"123","b":"456","c":"789"}
		 * char* head          : e.g. "<S>"   <Const>
		 */
	private:
		Str iDecodeMyIpcFormat ;
	public:
		int DecodeMyIpcFormat ( Str& sour_str ) ;   // source
		Str RtnMyIpcFormatStr ( void ) ;
		Str Decode_my_ipc_fmt ( Str& sour_str ) ;   // 2016 04 11



	private:
		Str i_self_fmt_value ;
		std::list < CMyFmt > i_self_fmt_list ;
	public:
		int Parser_self_fmt_all_data ( Str sour ) ;  // 必要執行
		int Serch_self_fmt ( Str key ) ;
		Str Rtn_serch_self_fmt ( void ) ;

		Str Search_self_fmt_and_rtn_value ( Str key ) ;

		/*
		 * 從頭開始搜尋
		 * const unsigned char* parserStr : 被搜尋的資料
		 * int sz                         : 被搜尋的資料大小
		 * unsigned char c                : 要搜尋的 Byte
		 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
		 */
		int MyStrchr ( CUChar* parserStr , int sz , UChar c ) ;     // source
		int Bin_strchr ( CUChar* parser_str , int sz , UChar c ) ;  // 2016 04 11

		/*
		 * 從尾開始搜尋
		 * const unsigned char* parserStr : 被搜尋的資料
		 * int sz                         : 被搜尋的資料大小
		 * unsigned char c                : 要搜尋的 Byte
		 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
		 */
		int MyStrrchr ( CUChar* parserStr , int sz , UChar c ) ;

		/*
		 * 從頭開始搜尋
		 * const unsigned char* parserStr : 被搜尋的資料
		 * int sz                         : 被搜尋的資料大小
		 * const unsigned char* str       : 要搜尋的資料
		 * int strSz                      : 要搜尋的資料大小
		 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
		 */
		int MyStrstr ( CUChar* parserStr , int sz , CUChar* str , int strSz ) ;

		/*
		 * 從尾開始搜尋
		 * const unsigned char* parserStr : 被搜尋的資料
		 * int sz                         : 被搜尋的資料大小
		 * const unsigned char* str       : 要搜尋的資料
		 * int strSz                      : 要搜尋的資料大小
		 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
		 */
		int MyStrrstr ( CUChar* parserStr , int sz , CUChar* str , int strSz ) ;

// About FILE //

		int MyMkdir ( CStr path ) ;
		int Mkdir_func ( CStr path ) ;

		/*
		 * 刪除大於 sz 大小的檔案
		 */
		int DelFileTooBig ( CStr path , LInt sz ) ;

		/*
		 * Tar 大於 sz 大小的檔案
		 */
		int TarFileTooBig ( CStr filePath , FILE*& sFp , LInt sz , CStr tarName ) ;

} ;

//
//
//#if 0
//DT_BLK      This is a block device.
//DT_CHR      This is a character device.
//DT_DIR      This is a directory.
//DT_FIFO     This is a named pipe (FIFO).
//DT_LNK      This is a symbolic link.
//DT_REG      This is a regular file.
//DT_SOCK     This is a UNIX domain socket.
//DT_UNKNOWN  The file type is unknown.
//
//enum
//  {
//    DT_UNKNOWN = 0,
//# define DT_UNKNOWN	DT_UNKNOWN
//    DT_FIFO = 1,
//# define DT_FIFO	DT_FIFO
//    DT_CHR = 2,
//# define DT_CHR		DT_CHR
//    DT_DIR = 4,
//# define DT_DIR		DT_DIR
//    DT_BLK = 6,
//# define DT_BLK		DT_BLK
//    DT_REG = 8,
//# define DT_REG		DT_REG
//    DT_LNK = 10,
//# define DT_LNK		DT_LNK
//    DT_SOCK = 12,
//# define DT_SOCK	DT_SOCK
//    DT_WHT = 14
//# define DT_WHT		DT_WHT
//  };
//#endif
//class CDir {
//		 , read_dir->d_ino ) ;
//		        printf ( "read_dir->d_name   : %s\n" , read_dir->d_name ) ;
//		        printf ( "read_dir->d_off    : %d\n" , read_dir->d_off ) ;
//		        printf ( "read_dir->d_reclen : %d\n" , read_dir->d_reclen ) ;
//		        printf ( "read_dir->d_type   : %d\n" , read_dir->d_type ) ;
//	private:
//		DIR* i_dir ;
//	public:
//		CDir ( ) {
//			i_dir = NULL ;
//		}
//		~CDir ( ) {
//			Close ( ) ;
//		}
//		int Open ( CStr dit_path ) {
//			i_dir = opendir ( dit_path.c_str ( ) ) ;
//			if ( NULL == i_dir ) {
//				return DF_UnSucc ;  ////////////////////
//			}
//			return DF_Succ ;
//		}
//
//
//
//		void Close ( void ) {
//
//			if ( NULL != i_dir ) {
//				closedir ( i_dir ) ;
//			}
//
//		}
//
//} ;

#endif /* STRINGPARSERABOUT_H_ */

