#ifndef _EZXML_H
#define _EZXML_H

char *innerText(char *inner, char *pText, const char *beginMark, const char *endMark);
char* fileToStr(const char *fileName);

#endif // _EZXML_H
