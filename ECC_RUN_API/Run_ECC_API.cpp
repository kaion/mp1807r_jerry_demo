#include <iostream>
using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>

#include "Run_ECC_API.h"
#include "Log.h"
#include "ezxml.h"

#define ICERAPI_EXE_ADDRESS "cd ecc_cmas && ./icerapi &"
#define ICERAPI_REQ_OK_ADDRESS "./ecc_cmas/ICERData/ICERAPI.REQ.OK"
#define ICERAPI_REQ_ADDRESS "./ecc_cmas/ICERData/ICERAPI.REQ"

#define RM_ICERAPI_RES_OK_ADDRESS "rm -rf ./ecc_cmas/ICERData/ICERAPI.RES.OK"
#define RM_ICERAPI_RES_ADDRESS "rm -rf ./ecc_cmas/ICERData/ICERAPI.RES"
#define ICERAPI_RES_OK_ADDRESS "./ecc_cmas/ICERData/ICERAPI.RES.OK"
#define ICERAPI_RES_ADDRESS "./ecc_cmas/ICERData/ICERAPI.RES"

char g_T0400[8+1] = {0}, g_checkoutSerialNumber = 01;
unsigned int g_T1101_Value = 000001, g_T5591 = 0;

char runEccIcerapiReq(char *XML_Data, int XML_Data_Le)
{
    char XML_Start_Data[77+1] = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<TransXML>\n  <TRANS>\n";
    char XML_End_Data[22+1] = "  </TRANS>\n</TransXML>";
    FILE *pFile = NULL, *pFile2 = NULL;
    
    runEccCheckFolder();
    
    pFile = fopen(ICERAPI_REQ_ADDRESS, "w");
    if(pFile == NULL)
    {
        return -1;
    }
    
    fprintf(pFile, "%s", XML_Start_Data);
    fprintf(pFile, "%s", XML_Data);
    fprintf(pFile, "%s", XML_End_Data);
    
    fflush(pFile);
    fclose(pFile);
    
    Str Data = "[悠] [XML File] [Req] [";
    Data += XML_Start_Data;
    Data += XML_Data;
    Data += XML_End_Data;
    Data += "]";
	writeLog(Data);
    
    pFile2 = fopen(ICERAPI_REQ_OK_ADDRESS, "w");
    if(pFile2 == NULL)
    {
        return -1;
    }
    fclose(pFile2);
    
    system(ICERAPI_EXE_ADDRESS);
    
    return 0;
}

char runEccIcerapiRes()
{
    time_t Time = time(NULL);
    FILE *pFile = NULL;
    Str Data;
    
    while(1)
    {
        if(time(NULL) - Time >= 30)
        {
            Data = "[悠] [XML File] [Res] [Time Out]";
	        writeLog(Data);
            return -1;
        }
        pFile = fopen(ICERAPI_RES_OK_ADDRESS, "r");
        if(pFile == NULL)
        {
            continue;
        }
        fclose(pFile);
        
        Data = "[悠] [XML File] [Res] [OK]";
	    writeLog(Data);
        
        break;
    }
    
    return 0;
}

char runEccSignOnApi(unsigned int NU)
{
    char XML_Data[184+1] = {0}, Time_YYMMDD[8+1] = {0}, Time_hhmmss[6+1] = {0};
    int XML_Data_Le = 0;
    time_t Time = 0;
    struct tm *info = NULL;
    char *ECCFile = NULL, T3901[6+1] = {0}, T1101[6+1] = {0};
    
    Str Data;
    Data = "[悠] [SignOn] [開始]";
	writeLog(Data);
    
    Time = time(NULL);
    info = localtime(&Time);
    sprintf(Time_YYMMDD, "%04d%02d%02d", (1900 + info->tm_year), (1 + info->tm_mon), info->tm_mday);
    sprintf(Time_hhmmss, "%02d%02d%02d", info->tm_hour, info->tm_min, info->tm_sec);
    
    g_T1101_Value = NU;
    g_checkoutSerialNumber = 01;
    sprintf(XML_Data, "    <T0100>0800</T0100>\n    <T0300>881999</T0300>\n    <T1100>%06u</T1100>\n    <T1101>%06u</T1101>\n    <T1200>%s</T1200>\n    <T1300>%s</T1300>\n    <T5501>%s%02d</T5501>\n", NU, g_T1101_Value, Time_hhmmss, Time_YYMMDD, &Time_YYMMDD[2], g_checkoutSerialNumber);
    XML_Data_Le = strlen(XML_Data);
    
    runEccIcerapiReq(XML_Data, XML_Data_Le);
    
    if( runEccIcerapiRes() != 0 )
    {
        Data = "[悠] [SignOn] [失敗]";
	    writeLog(Data);
        return -1;
    }
    
    ECCFile = fileToStr(ICERAPI_RES_ADDRESS);
    if( ECCFile == NULL)
    {
        Data = "[悠] [SignOn] [XML ERROR]";
	    writeLog(Data);
        return -2;
    }
    
    Data = "[悠] [SignOn] [Res] [";
    Data += ECCFile;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T3901, ECCFile, "<T3901>", "</T3901>") == NULL)
    {
        Data = "[悠] [SignOn] [T3901 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [SignOn] [T3901] [";
    Data += T3901;
    Data += "]";
    writeLog(Data);
    
    if(memcmp(T3901, "0", 1) != 0)
    {
        Data = "[悠] [SignOn] [失敗]";
	    writeLog(Data);
        
        free(ECCFile);
        return -4;
    }
    
    if( innerText(T1101, ECCFile, "<T1101>", "</T1101>") == NULL)
    {
        Data = "[悠] [SignOn] [T1101 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [SignOn] [T1101] [";
    Data += T1101;
    Data += "]";
    writeLog(Data);
    
    g_T1101_Value = atoi(T1101) + 1;
    if( g_T1101_Value > 999999 )
    {
        g_T1101_Value = 1;
    }
    
    free(ECCFile);
    
    return 0;
}

char runEccReadCardBasicApi(unsigned int NU, char *T0215)
{
    char XML_Data[204+1] = {0}, Time_YYMMDD[8+1] = {0}, Time_hhmmss[6+1] = {0};
    int XML_Data_Le = 0;
    time_t Time = 0;
    struct tm *info = NULL;
    char *ECCFile = NULL, T3901[6+1] = {0}, T0300[10+1] = {0}, T1101[6+1] = {0}, T0410[8+1] = {0};
    
    Str Data;
    Data = "[悠] [ReadCardBasic] [開始]";
	writeLog(Data);
    
    Time = time(NULL);
    info = localtime(&Time);
    sprintf(Time_YYMMDD, "%04d%02d%02d", (1900 + info->tm_year), (1 + info->tm_mon), info->tm_mday);
    sprintf(Time_hhmmss, "%02d%02d%02d", info->tm_hour, info->tm_min, info->tm_sec);

    sprintf(XML_Data, "    <T0100>0200</T0100>\n    <T0300>296000</T0300>\n    <T0400>000</T0400>\n    <T1100>%06u</T1100>\n    <T1101>%06u</T1101>\n    <T1200>%s</T1200>\n    <T1300>%s</T1300>\n    <T4108>0</T4108>\n", NU, g_T1101_Value, Time_hhmmss, Time_YYMMDD);
    XML_Data_Le = strlen(XML_Data);
    
    runEccIcerapiReq(XML_Data, XML_Data_Le);
    
    if( runEccIcerapiRes() != 0 )
    {
        Data = "[悠] [ReadCardBasic] [失敗]";
	    writeLog(Data);
        return -1;
    }
    
    ECCFile = fileToStr(ICERAPI_RES_ADDRESS);
    if( ECCFile == NULL)
    {
        Data = "[悠] [ReadCardBasic] [XML ERROR]";
	    writeLog(Data);
        return -2;
    }
    
    Data = "[悠] [ReadCardBasic] [Res] [";
    Data += ECCFile;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T3901, ECCFile, "<T3901>", "</T3901>") == NULL)
    {
        Data = "[悠] [ReadCardBasic] [T3901 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [ReadCardBasic] [T3901] [";
    Data += T3901;
    Data += "]";
    writeLog(Data);
    
    if(memcmp(T3901, "0", 1) != 0)
    {
        Data = "[悠] [ReadCardBasic] [失敗]";
	    writeLog(Data);
        
        free(ECCFile);
        return -4;
    }
    
    if( innerText(T0300, ECCFile, "<T0300>", "</T0300>") == NULL)
    {
        Data = "[悠] [ReadCardBasic] [T0300 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [ReadCardBasic] [T0300] [";
    Data += T0300;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T1101, ECCFile, "<T1101>", "</T1101>") == NULL)
    {
        Data = "[悠] [ReadCardBasic] [T1101 NULL]";
        writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [ReadCardBasic] [T1101] [";
    Data += T1101;
    Data += "]";
    writeLog(Data);
    
    g_T1101_Value = atoi(T1101) + 1;
    if( g_T1101_Value > 999999 )
    {
        g_T1101_Value = 1;
    }
    
    if( innerText(T0410, ECCFile, "<T0410>", "</T0410>") == NULL)
    {
        Data = "[悠] [ReadCardBasic] [T0410 NULL]";
    }
    else
    {
        Data = "[悠] [ReadCardBasic] [T0410] [";
        Data += T0410;
        Data += "]";
    }
    writeLog(Data);
    
    if( innerText(T0215, ECCFile, "<T0215>", "</T0215>") == NULL)
    {
        Data = "[悠] [ReadCardBasic] [T0215 NULL]";
    }
    else
    {
        Data = "[悠] [ReadCardBasic] [T0215] [";
        Data += T0215;
        Data += "]";
    }
    writeLog(Data);
    
    free(ECCFile);
    
    return 0;
}

char runEccRetryDeductApi(unsigned int NU, char *T0215, char *T4110, char *T5501, char *T3700, char *T0415, char *T0409, char *T0410)
{
    char XML_Data[254+1] = {0}, Time_YYMMDD[8+1] = {0}, Time_hhmmss[6+1] = {0};
    int XML_Data_Le = 0;
    time_t Time = 0;
    struct tm *info = NULL;
    char *ECCFile = NULL, T3901[6+1] = {0}, T3904[4+1] = {0}, T0300[10+1] = {0}, T1101[6+1] = {0};
    
    Str Data;
    Data = "[悠] [Retry Deduct] [開始]";
	writeLog(Data);
    
    Time = time(NULL);
    info = localtime(&Time);
    sprintf(Time_YYMMDD, "%04d%02d%02d", (1900 + info->tm_year), (1 + info->tm_mon), info->tm_mday);
    sprintf(Time_hhmmss, "%02d%02d%02d", info->tm_hour, info->tm_min, info->tm_sec);
    
    sprintf(T5501, "%s%02d", &Time_YYMMDD[2], g_checkoutSerialNumber);
    
    sprintf(XML_Data, "    <T0100>0200</T0100>\n    <T0300>606100</T0300>\n    <T0400>%s</T0400>\n    <T1100>%06u</T1100>\n    <T1101>%06u</T1101>\n    <T1200>%s</T1200>\n    <T1300>%s</T1300>\n    <T4108>0</T4108>\n    <T4830>0</T4830>\n    <T5501>%s</T5501>\n", g_T0400, NU, g_T1101_Value, Time_hhmmss, Time_YYMMDD, T5501);
    XML_Data_Le = strlen(XML_Data);
    
    runEccIcerapiReq(XML_Data, XML_Data_Le);
    
    if( runEccIcerapiRes() != 0 )
    {
        Data = "[悠] [Retry Deduct] [失敗]";
	    writeLog(Data);
        
        Data = "[悠] [Retry Deduct] [Retry]";
	    writeLog(Data);
        
        return -1;
    }
    
    ECCFile = fileToStr(ICERAPI_RES_ADDRESS);
    if( ECCFile == NULL)
    {
        Data = "[悠] [Retry Deduct] [XML ERROR]";
	    writeLog(Data);
        
        return -2;
    }
    
    Data = "[悠] [Retry Deduct] [Res] [";
    Data += ECCFile;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T3901, ECCFile, "<T3901>", "</T3901>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T3901 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Retry Deduct] [T3901] [";
    Data += T3901;
    Data += "]";
    writeLog(Data);
    
    if(memcmp(T3901, "0", 1) != 0)
    {
        Data = "[悠] [Retry Deduct] [失敗]";
	    writeLog(Data);
        
        if( innerText(T3904, ECCFile, "<T3904>", "</T3904>") == NULL)
        {
            Data = "[悠] [Retry Deduct] [T3904 NULL]";
	        writeLog(Data);
            
            free(ECCFile);
            return -3;
        }
        
        free(ECCFile);
        
        if(memcmp(T3901, "-125", 4) == 0)
        {
            Data = "[悠] [Retry Deduct] [Retry]";
    	    writeLog(Data);
            
            return -1;
        }
        if( (memcmp(T3901, "-119", 4) == 0) && (memcmp(T3904, "62", 2) == 0) )
        {
            Data = "[悠] [Retry Deduct] [Retry]";
    	    writeLog(Data);
            
            return -1;
        }
        
        return -4;
    }
    
    if( innerText(T1101, ECCFile, "<T1101>", "</T1101>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T1101 NULL]";
        writeLog(Data);
    }
    else
    {
        Data = "[悠] [Retry Deduct] [T1101] [";
        Data += T1101;
        Data += "]";
        writeLog(Data);
        g_T1101_Value = atoi(T1101) + 1;
        if( g_T1101_Value > 999999 )
        {
            g_T1101_Value = 1;
        }
    }
    
    if( innerText(T0410, ECCFile, "<T0410>", "</T0410>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T0410 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Retry Deduct] [T0410] [";
    Data += T0410;
    Data += "]";
    writeLog(Data);
    
    if( innerText(g_T0400, ECCFile, "<T0400>", "</T0400>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T0400 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Retry Deduct] [T0400] [";
    Data += g_T0400;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T0415, ECCFile, "<T0415>", "</T0415>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T0415 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Retry Deduct] [T0415] [";
    Data += T0415;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T0409, ECCFile, "<T0409>", "</T0409>") == NULL)
    {
        g_T5591 = 1;
        memcpy(T0409, "0", sizeof("0"));
        
        Data = "[悠] [Retry Deduct] [T0409 NULL]";
    }
    else
    {
        g_T5591 = 2;
        sprintf(g_T0400, "%u", ( atoi(g_T0400) + atoi(T0409)) );
        
        Data = "[悠] [Retry Deduct] [T0409] [";
        Data += T0409;
        Data += "]";
    }
    writeLog(Data);
    
    if( innerText(T0215, ECCFile, "<T0215>", "</T0215>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T0215 NULL]";
    }
    else
    {
        Data = "[悠] [Retry Deduct] [T0215] [";
        Data += T0215;
        Data += "]";
    }
    writeLog(Data);
    
    if( innerText(T4110, ECCFile, "<T4110>", "</T4110>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T4110 NULL]";
    }
    else
    {
        Data = "[悠] [Retry Deduct] [T4110] [";
        Data += T4110;
        Data += "]";
    }
    writeLog(Data);
    
    if( innerText(T3700, ECCFile, "<T3700>", "</T3700>") == NULL)
    {
        Data = "[悠] [Retry Deduct] [T3700 NULL]";
    }
    else
    {
        Data = "[悠] [Retry Deduct] [T3700] [";
        Data += T3700;
        Data += "]";
    }
    writeLog(Data);
    
    free(ECCFile);
    
    return 0;
}

char runEccDeductApi(unsigned int NU, unsigned int T0400, char *T0215, char *T4110, char *T5501, char *T3700, char *T0415, char *T0409, char *T0410)
{
    char XML_Data[254+1] = {0}, Time_YYMMDD[8+1] = {0}, Time_hhmmss[6+1] = {0};
    int XML_Data_Le = 0;
    time_t Time = 0;
    struct tm *info = NULL;
    char *ECCFile = NULL, T3901[6+1] = {0}, T0300[10+1] = {0}, T1101[6+1] = {0};
    
    Str Data;
    Data = "[悠] [Deduct] [開始]";
	writeLog(Data);
    
    Time = time(NULL);
    info = localtime(&Time);
    sprintf(Time_YYMMDD, "%04d%02d%02d", (1900 + info->tm_year), (1 + info->tm_mon), info->tm_mday);
    sprintf(Time_hhmmss, "%02d%02d%02d", info->tm_hour, info->tm_min, info->tm_sec);
    
    sprintf(T5501, "%s%02d", &Time_YYMMDD[2], g_checkoutSerialNumber);
    sprintf(g_T0400, "%u00", T0400);
    sprintf(XML_Data, "    <T0100>0200</T0100>\n    <T0300>606100</T0300>\n    <T0400>%s</T0400>\n    <T1100>%06u</T1100>\n    <T1101>%06u</T1101>\n    <T1200>%s</T1200>\n    <T1300>%s</T1300>\n    <T4108>0</T4108>\n    <T4830>0</T4830>\n    <T5501>%s</T5501>\n", g_T0400, NU, g_T1101_Value, Time_hhmmss, Time_YYMMDD, T5501);
    XML_Data_Le = strlen(XML_Data);
    
    runEccIcerapiReq(XML_Data, XML_Data_Le);
    
    if( runEccIcerapiRes() != 0 )
    {
        Data = "[悠] [Deduct] [失敗]";
	    writeLog(Data);
        
        Data = "[悠] [Deduct] [Retry]";
	    writeLog(Data);
        
        char i = 0;
        for(i=0; i<3; i++)
        {
            char ecc_res = runEccRetryDeductApi(NU, T0215, T4110, T5501, T3700, T0415, T0409, T0410);
            if( ecc_res != -1 )
            {
                if(ecc_res == 0)
                {
                    return 0;
                }
                break;
            }
        }
        
        return -1;
    }
    
    ECCFile = fileToStr(ICERAPI_RES_ADDRESS);
    if( ECCFile == NULL)
    {
        Data = "[悠] [Deduct] [XML ERROR]";
	    writeLog(Data);
        
        return -2;
    }
    
    Data = "[悠] [Deduct] [Res] [";
    Data += ECCFile;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T3901, ECCFile, "<T3901>", "</T3901>") == NULL)
    {
        Data = "[悠] [Deduct] [T3901 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Deduct] [T3901] [";
    Data += T3901;
    Data += "]";
    writeLog(Data);
    
    if(memcmp(T3901, "0", 1) != 0)
    {
        Data = "[悠] [Deduct] [失敗]";
	    writeLog(Data);
        
        free(ECCFile);
        
        if(memcmp(T3901, "-125", 4) == 0)
        {
            Data = "[悠] [Deduct] [Retry]";
    	    writeLog(Data);
            
            char i = 0;
            for(i=0; i<3; i++)
            {
                char ecc_res = runEccRetryDeductApi(NU, T0215, T4110, T5501, T3700, T0415, T0409, T0410);
                if( ecc_res != -1 )
                {
                    if(ecc_res == 0)
                    {
                        return 0;
                    }
                    break;
                }
            }
        }
        return -4;
    }
    
    if( innerText(T0300, ECCFile, "<T0300>", "</T0300>") == NULL)
    {
        Data = "[悠] [Deduct] [T0300 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Deduct] [T0300] [";
    Data += T0300;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T1101, ECCFile, "<T1101>", "</T1101>") == NULL)
    {
        Data = "[悠] [Deduct] [T1101 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Deduct] [T1101] [";
    Data += T1101;
    Data += "]";
    writeLog(Data);
    g_T1101_Value = atoi(T1101) + 1;
    if( g_T1101_Value > 999999 )
    {
        g_T1101_Value = 1;
    }
    
    if( innerText(T0410, ECCFile, "<T0410>", "</T0410>") == NULL)
    {
        Data = "[悠] [Deduct] [T0410 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Deduct] [T0410] [";
    Data += T0410;
    Data += "]";
    writeLog(Data);
    
    if( innerText(g_T0400, ECCFile, "<T0400>", "</T0400>") == NULL)
    {
        Data = "[悠] [Deduct] [T0400 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Deduct] [T0400] [";
    Data += g_T0400;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T0415, ECCFile, "<T0415>", "</T0415>") == NULL)
    {
        Data = "[悠] [Deduct] [T0415 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Deduct] [T0415] [";
    Data += T0415;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T0409, ECCFile, "<T0409>", "</T0409>") == NULL)
    {
        g_T5591 = 1;
        memcpy(T0409, "0", sizeof("0"));
        
        Data = "[悠] [Deduct] [T0409 NULL]";
    }
    else
    {
        g_T5591 = 2;
        //sprintf(g_T0400, "%u", ( (T0400*100) + atoi(T0409)) );
        sprintf(g_T0400, "%u", ( atoi(g_T0400) + atoi(T0409)) );
        
        Data = "[悠] [Deduct] [T0409] [";
        Data += T0409;
        Data += "]";
    }
    writeLog(Data);
    
    if( innerText(T0215, ECCFile, "<T0215>", "</T0215>") == NULL)
    {
        Data = "[悠] [Deduct] [T0215 NULL]";
    }
    else
    {
        Data = "[悠] [Deduct] [T0215] [";
        Data += T0215;
        Data += "]";
    }
    writeLog(Data);
    
    if( innerText(T4110, ECCFile, "<T4110>", "</T4110>") == NULL)
    {
        Data = "[悠] [Deduct] [T4110 NULL]";
    }
    else
    {
        Data = "[悠] [Deduct] [T4110] [";
        Data += T4110;
        Data += "]";
    }
    writeLog(Data);
    
    if( innerText(T3700, ECCFile, "<T3700>", "</T3700>") == NULL)
    {
        Data = "[悠] [Deduct] [T3700 NULL]";
    }
    else
    {
        Data = "[悠] [Deduct] [T3700] [";
        Data += T3700;
        Data += "]";
    }
    writeLog(Data);
    
    free(ECCFile);
    
    return 0;
}

char runEccSettleApi(unsigned int NU, char *T5501, char *T3912)
{
    char XML_Data[259+1] = {0}, Time_YYMMDD[8+1] = {0}, Time_hhmmss[6+1] = {0};
    int XML_Data_Le = 0;
    time_t Time = 0;
    struct tm *info = NULL;
    char *ECCFile = NULL, T3901[6+1] = {0}, T0300[10+1] = {0}, T1101[6+1] = {0};
    
    Str Data;
    Data = "[悠] [Settle] [開始]";
	writeLog(Data);
    
    Time = time(NULL);
    info = localtime(&Time);
    sprintf(Time_YYMMDD, "%04d%02d%02d", (1900 + info->tm_year), (1 + info->tm_mon), info->tm_mday);
    sprintf(Time_hhmmss, "%02d%02d%02d", info->tm_hour, info->tm_min, info->tm_sec);
    
    sprintf(T5501, "%s%02d", &Time_YYMMDD[2], g_checkoutSerialNumber);
    
    sprintf(XML_Data, "    <T0100>0500</T0100>\n    <T0300>900099</T0300>\n    <T1100>%06u</T1100>\n    <T1101>%06u</T1101>\n    <T1200>%s</T1200>\n    <T1300>%s</T1300>\n    <T5501>%s</T5501>\n    <T5591>%d</T5591>\n    <T5592>\n      <T559201>%s</T559201>\n    </T5592>\n", NU, g_T1101_Value, Time_hhmmss, Time_YYMMDD, T5501, g_T5591, g_T0400);
    XML_Data_Le = strlen(XML_Data);
    
    runEccIcerapiReq(XML_Data, XML_Data_Le);
    
    if( runEccIcerapiRes() != 0 )
    {
        Data = "[悠] [Settle] [失敗]";
	    writeLog(Data);
        
        return -1;
    }
    
    ECCFile = fileToStr(ICERAPI_RES_ADDRESS);
    if( ECCFile == NULL)
    {
        Data = "[悠] [Settle] [XML ERROR]";
	    writeLog(Data);
        
        return -2;
    }
    
    Data = "[悠] [Settle] [Res] [";
    Data += ECCFile;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T3901, ECCFile, "<T3901>", "</T3901>") == NULL)
    {
        Data = "[悠] [Settle] [T3901 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Settle] [T3901] [";
    Data += T3901;
    Data += "]";
    writeLog(Data);
    
    if(memcmp(T3901, "0", 1) != 0)
    {
        Data = "[悠] [Settle] [失敗]";
	    writeLog(Data);
        
        free(ECCFile);
        return -4;
    }
    
    if( innerText(T0300, ECCFile, "<T0300>", "</T0300>") == NULL)
    {
        Data = "[悠] [Settle] [T0300 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Settle] [T0300] [";
    Data += T0300;
    Data += "]";
    writeLog(Data);
    
    if( innerText(T1101, ECCFile, "<T1101>", "</T1101>") == NULL)
    {
        Data = "[悠] [Settle] [T1101 NULL]";
	    writeLog(Data);
        
        free(ECCFile);
        return -3;
    }
    Data = "[悠] [Settle] [T1101] [";
    Data += T1101;
    Data += "]";
    writeLog(Data);

    g_T1101_Value = atoi(T1101) + 1;
    if( g_T1101_Value > 999999 )
    {
        g_T1101_Value = 1;
    }
    
    if( innerText(T3912, ECCFile, "<T3912>", "</T3912>") == NULL)
    {
        Data = "[悠] [Settle] [T3912 NULL]";
    }
    else
    {
        Data = "[悠] [Settle] [T3912] [";
        Data += T3912;
        Data += "]";
    }
    writeLog(Data);
    
    free(ECCFile);
    g_checkoutSerialNumber++;
    if(g_checkoutSerialNumber > 99)
    {
        g_checkoutSerialNumber = 01;
    }
    return 0;
}

void runEccCheckFolder()
{
    struct stat buf = {0};
    FILE *pFile = NULL;
    
    if(stat("./ecc_cmas/ICERData", &buf) != 0)
    {
        //printf("No Folder\n");
        system(ICERAPI_EXE_ADDRESS);
    }
    else
    {
        ;//printf("Ok Folder\n");
    }
    
    pFile = fopen(ICERAPI_RES_OK_ADDRESS, "r");
    if(pFile != NULL)
    {
        fclose(pFile);
        system(RM_ICERAPI_RES_OK_ADDRESS);
    }
    
    pFile = fopen(ICERAPI_RES_ADDRESS, "r");
    if(pFile != NULL)
    {
        fclose(pFile);
        system(RM_ICERAPI_RES_ADDRESS);
    }
    return ;
}

