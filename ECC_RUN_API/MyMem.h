#ifndef MYMEM_H_
#define MYMEM_H_

#include "SetupFile.h"

class CMem {
	public:
		template < typename T >  T*   New     ( int sz ) ;
		template < typename T1 > T1*  Realloc ( T1*& ptr , int oldSz , int newSz ) ;
		template < typename T2 > void Delete  ( T2*& addr ) ;
} ;
/*
 * 不可用 memset 去清空 ,因為有時候是借 class ,若是一清空會出問題
 */
template < typename T > T* CMem::New ( int sz ) {

	T* ptrTmp = NULL ;
	try {
		ptrTmp = new T [ sz ] ;
	} catch ( std::bad_alloc & exc ) {
		return NULL ;
	} ;
	return ptrTmp ;
}

template < typename T1 > T1* CMem::Realloc ( T1*& sPtr , int oldSz , int newSz ) {
	if ( ( 0 >= newSz ) ) {
		Delete ( sPtr ) ;
		return ( sPtr = NULL ) ;
	}

	T1* ptrTmp = New < T1 > ( newSz ) ;
	if ( NULL == ptrTmp ) {
		Delete ( sPtr ) ;
		return ( sPtr = NULL ) ;
	}

	// 要借記憶體空間 //
	if ( NULL == sPtr ) {
		return ptrTmp ;
	}

	if ( oldSz > newSz ) {
		oldSz = newSz ;
	}
	memcpy ( ptrTmp , sPtr , oldSz * sizeof(T1) ) ;

	Delete ( sPtr ) ;

	return ptrTmp ;
}

template < typename T2 > void CMem::Delete ( T2*& addr ) {
	if ( NULL != addr ) {
		delete [ ] addr ;
		addr = NULL ;
	}
	return ;
}
#endif /* ABOUTMEMORY_H_ */
