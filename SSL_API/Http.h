#ifndef HTTP_H_
#define HTTP_H_

#include "SetupFile.h"
#include "MyMem.h"
#include "MyString.h"

class CRdWrTmO {
	public:
		int SetsockoptMethod ( int iFd , UInt wrMs , UInt rdMs ) ;
} ;

class CClient {
	private:
		SSL_CTX* iCtx ;
		SSL * iSsl ;
		CMem* iMem ;

		char FromHex ( char ch ) ;
		char ToHex ( char code ) ;

	public:
		CClient ( ) ;
		~CClient ( ) ;

	private:
		int iSocketFd ;
	public:
		int Connect ( CStr ip , CStr port ) ;
		int RuternSocketFd ( void ) ;
		void CloseSocketFd ( void ) ;

		int WriteBin ( CStrType*& buf ) ;
		int WriteStr ( CStr& str ) ;

		int ReadToFile ( bool keepAlived , CStr filePath ) ;
		/*
		 *  bool keepAlived = true 表示對方不斷線
		 *  bool keepAlived = false 表示對方傳完資料斷線
		 *
		 *  bool dataContinue = true 表示續接資料
		 *  bool dataContinue = false 表示不續接資料
		 */
	private:
		Str iReadStr ;
	public:
		int ReadStr ( bool keepAlived ) ;
		Str RtnReadStr ( void ) ;   // 記得要宣告 reference variable 續承接

	private:
		CStrType* iReadBin ;
	public:
		int ReadBin ( bool keepAlived , bool dataContinue ) ;
		CStrType* RtnReadBin ( void ) ;   // 記得要宣告 reference variable 續承接

		// url //
	private:
		Str iUrlDecode ;
	public:
		int UrlDecode ( CStr& input ) ;
		Str RtnUrlDecode ( void ) ;

	private:
		Str iUrlEncode ;
	public:
		int UrlEncode ( CStr& input ) ;
		Str RtnUrlEncode ( void ) ;

		//~~~~~~~~~~~~~~~~~~~~~~~~~~~//
		// socket //
		// ssl //
		/*
		 * SSLv23_client_method()
		 */
		int SslConnect ( void ) ;

		int Ssl_write_bin ( CStrType*& buf ) ;
		int Ssl_write_str ( CStr& str ) ;

	private:
		Str i_Ssl_read_str ;
	public:
		int Ssl_read_str ( void ) ;
		Str Rtn_Ssl_read_str ( void ) ;   // 記得要宣告 reference variable 續承接
		
		Str Rtn_IP_str ( void );

	private:
		CStrType* i_Ssl_read_bin ;
	public:
		int Ssl_read_bin ( bool data_continue ) ;
		CStrType* Rtn_Ssl_read_bin ( void ) ;   // 記得要宣告 reference variable 續承接

		void CloseSsl ( void ) ;

} ;

#endif /* ABOUTHTTP_H_ */
