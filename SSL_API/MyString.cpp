#include "MyString.h"

/*
 * for My string C++ code
 */
CStrType::CStrType ( ) {
	i_new_bin = NULL ;
	i_sz = 0 ;
	i_new_hex_str = NULL ;
	iMem = new CMem [ 1 ] ;
	i_NULL_respon = "" ;
}

CStrType::~CStrType ( ) {
	iMem->Delete ( i_new_bin ) ;
	iMem->Delete ( i_new_hex_str ) ;
	if ( NULL != iMem ) {
		delete [ ] iMem ;
	}
}

int CStrType::Clear ( void ) {
	iMem->Delete ( i_new_bin ) ;
	i_sz = 0 ;
	return D_succ ;
}

// like memset //
int CStrType::Assign ( CUInt size , CUChar c ) {
	Clear ( ) ;
	if ( 0 == size ) {
		return D_succ ;
	}
	i_new_bin = iMem->New < UChar > ( size + 1 ) ;
	if ( NULL == i_new_bin ) {
		return DF_CStrType_Assign_CMem_New ;
	}
	memset ( i_new_bin , c , size ) ;
	i_new_bin [ size ] = 0 ;
	i_sz = size ;
	return D_succ ;
}

// memcpy , 記得在第三個引數加上 ( void* ) //
/*
 * CUInt beginNum : 原資料要開始接續的位置
 * CUInt size     : forceStr 的大小
 * void* forceStr :
 */
int CStrType::Assign ( CUInt beginNum , CInt size , void* forceStr ) {

	if ( 0 >= size ) {
		return D_succ ;

	} else if ( beginNum > i_sz ) {
		Clear ( ) ;
		return DF_CStrType_Assign_SZ_Big_Source ;
	}

	int newSz = beginNum + size ;
	UChar* ptrtmp = iMem->New < UChar > ( newSz + 1 ) ;
	if ( NULL == ptrtmp ) {
		Clear ( ) ;
		return DF_CStrType_Assign_CMem_New ;
	}

	if ( 0 != beginNum ) {
		memcpy ( ptrtmp , i_new_bin , beginNum ) ;
	}
	Clear ( ) ;
	memcpy ( ptrtmp + beginNum , forceStr , size ) ;
	i_new_bin = ptrtmp ;
	i_new_bin [ newSz ] = 0 ;
	i_sz = newSz ;

	return D_succ ;
}

int CStrType::Size ( void ) {
	return i_sz ;
}

void CStrType::Change_order ( void* data , int byte ) {
	int s = 0 ;
	-- byte ;
	UChar* p_data = ( UChar* ) data ;
	for ( ; s < byte ; ++ s , -- byte ) {
		p_data [ s ] ^= p_data [ byte ] ;
		p_data [ byte ] ^= p_data [ s ] ;
		p_data [ s ] ^= p_data [ byte ] ;
	}
}

void CStrType::Change_order ( void ) {
	Change_order ( i_new_bin , i_sz ) ;
	return ;
}

int CStrType::Assign ( CChar* format , ... ) {
	va_list list ;

	va_start ( list , format ) ;
	char strButTmp [ 0 ] ;   // 為的是要取得長度
	int len = vsnprintf ( strButTmp , sizeof ( strButTmp ) , format , list ) ;
	va_end ( list ) ;

	Clear ( ) ;

	va_start ( list , format ) ;
	i_new_bin = iMem->New < UChar > ( len + 1 ) ;
	if ( NULL == i_new_bin ) {
		return DF_CStrType_Assign_CMem_New ;
	}
	i_sz = vsnprintf ( ( char* ) i_new_bin , len + 1 , format , list ) ;
	va_end ( list ) ;

	return D_succ ;
}

void CStrType::Cpy ( CStrType* obj ) {
	Assign ( 0 , obj->Size ( ) , ( void* ) obj->RtnStr ( ) ) ;
	return ;
}
void CStrType::Cpy ( CStr str ) {
	Assign ( 0 , str.size ( ) , ( void* ) str.c_str ( ) ) ;
	return ;
}
void CStrType::Cpy ( CUChar* bin , int binLen ) {
	Assign ( 0 , binLen , ( void* ) bin ) ;
	return ;
}

void CStrType::Cat ( CStrType* obj ) {
	Assign ( i_sz , obj->Size ( ) , ( void* ) obj->RtnStr ( ) ) ;
	return ;
}
void CStrType::Cat ( CStr str ) {
	Assign ( i_sz , str.size ( ) , ( void* ) str.c_str ( ) ) ;
	return ;
}
void CStrType::Cat ( CUChar* bin , int binLen ) {
	Assign ( i_sz , binLen , ( void* ) bin ) ;
	return ;
}

int CStrType::Cmp ( CStrType* obj ) {
	if ( ( ULInt ) obj->Size ( ) != i_sz ) {
		return D_fail ;
	}
	UChar* ptr = obj->RtnStr ( ) ;
	int rtn = memcmp ( i_new_bin , ptr , i_sz ) ;
	if ( 0 != rtn ) {
		return D_fail ;
	}
	return D_succ ;
}

int CStrType::Cmp ( UChar* bin , int binLen ) {
	if ( ( ULInt ) binLen != i_sz ) {
		return D_fail ;
	}

	int rtn = memcmp ( i_new_bin , bin , i_sz ) ;
	if ( 0 != rtn ) {
		return D_fail ;
	}
	return D_succ ;
}

int CStrType::Cmp ( CStr str ) {
	if ( str.size ( ) != i_sz ) {
		return D_fail ;
	}

	int rtn = memcmp ( i_new_bin , str.c_str ( ) , i_sz ) ;
	if ( 0 != rtn ) {
		return D_fail ;
	}
	return D_succ ;
}

int CStrType::StrToBin ( Str str ) {
	Clear ( ) ;   // 先行清空 i_new_bin

	int cardNoLen = str.size ( ) ;

	if ( 0 == cardNoLen ) {
		return D_fail ;
	} else if ( cardNoLen & 0x01 ) {   // 表示該值非雙數
		++ cardNoLen ;
	}
	char cardNoBuf [ cardNoLen + 1 ] ;
	memset ( cardNoBuf , 0 , sizeof ( cardNoBuf ) ) ;
	if ( str.size ( ) & 0x01 ) {
		cardNoBuf [ 0 ] = '0' ;
	}
	snprintf ( cardNoBuf + strlen ( cardNoBuf ) , sizeof ( cardNoBuf ) , "%s" , str.c_str ( ) ) ;
	cardNoLen = strlen ( cardNoBuf ) ;

	i_sz = cardNoLen >> 1 ;
	i_new_bin = iMem->New < UChar > ( i_sz ) ;
	for ( int i = 0 ; i < ( int ) i_sz ; ++ i ) {
		char tmp [ 2 + 1 ] = { 0 } ;
		memcpy ( tmp , cardNoBuf + ( i << 1 ) , 2 ) ;
		ULInt num = strtoul ( tmp , NULL , 16 ) ;
		i_new_bin [ i ] = ( ( UChar* ) & num ) [ 0 ] ;
	}
	return D_succ ;
}

int CStrType::Str_to_bin ( Str str ) {
	return StrToBin ( str ) ;
}

UChar* CStrType::RtnStr ( void ) {
	if ( NULL == i_new_bin ) {
		return ( UChar* ) i_NULL_respon.c_str ( ) ;   // 防止 printf crash
	}
	return i_new_bin ;
}

UChar* CStrType::Rtn_bin ( void ) {
	return RtnStr ( ) ;
}

char* CStrType::RtnNewHexStr ( void ) {
	iMem->Delete ( i_new_hex_str ) ;
	if ( 0 >= i_sz ) {
		return ( char* ) i_NULL_respon.c_str ( ) ;   // 防止 printf crash ;
	}

	int nwHexStrLen = ( i_sz << 1 ) + 1 ;
	i_new_hex_str = iMem->New < char > ( nwHexStrLen ) ;

	for ( int i = 0 ; i < ( int ) i_sz ; ++ i ) {
		snprintf ( i_new_hex_str + ( i << 1 ) , nwHexStrLen , "%02X" , i_new_bin [ i ] ) ;
	}
	return i_new_hex_str ;
}

char* CStrType::Rtn_hex_str ( void ) {
	return RtnNewHexStr ( ) ;
}


char* CStrType::RtnNewHexStrLower ( void ) {
	iMem->Delete ( i_new_hex_str ) ;
	if ( 0 > i_sz ) {
		return ( char* ) i_NULL_respon.c_str ( ) ;   // 防止 printf crash ;
	}

	int nwHexStrLen = ( i_sz << 1 ) + 1 ;
	i_new_hex_str = iMem->New < char > ( nwHexStrLen ) ;
	memset ( i_new_hex_str , 0 , nwHexStrLen ) ;

	for ( int i = 0 ; i < ( int ) i_sz ; ++ i ) {
		snprintf ( i_new_hex_str + ( i << 1 ) , nwHexStrLen , "%02x" , i_new_bin [ i ] ) ;
	}
	return i_new_hex_str ;
}

char* CStrType::Rtn_hex_to_str_lower_change_order ( void ) {
	Change_order ( ) ;
	char* p_tmp = RtnNewHexStrLower ( ) ;
	Change_order ( ) ;
	return p_tmp ;
}

CMyStrPar::CMyStrPar ( ) {
	iMem = new CMem [ 1 ] ;
	iReadFileBin = iMem->New < CStrType > ( 1 ) ;
}

CMyStrPar::~CMyStrPar ( ) {
	iMem->Delete ( iReadFileBin ) ;

	if ( NULL != iMem ) {
		delete [ ] iMem ;
	}
}

char* CMyStrPar::ParserParameter ( char** argv , char* key ) {
	for ( int i = 0 ; NULL != argv [ i ] ; ++ i ) {
		if ( 0 == memcmp ( key , argv [ i ] , strlen ( key ) + 1 ) ) {
			if ( NULL == argv [ ++ i ] ) {
				return NULL ;
			}
			return argv [ i ] ;
		}
	}
	return NULL ;
}

bool CMyStrPar::Have_parameter ( char**argv , char* key ) {
	for ( int i = 0 ; argv [ i ] ; ++ i )
		if ( ! memcmp ( key , argv [ i ] , strlen ( key ) + 1 ) )
			return true ;

	return false ;
}


int CMyStrPar::Cp_file ( Str sour_file , Str target_file ) {
	FILE* fp1 = fopen ( sour_file.c_str ( ) , "rb+" ) ;
	if ( NULL == fp1 ) {
		return - 1 ;
	}
	FILE* fp2 = fopen ( target_file.c_str ( ) , "wb+" ) ;
	if ( NULL == fp2 ) {
		fclose ( fp1 ) ;
		return - 2 ;
	}

	fseek ( fp1 , 0 , SEEK_END ) ;
	int sz = ftell ( fp1 ) ;
	fseek ( fp1 , 0 , SEEK_SET ) ;

	UChar* buf = iMem->New < UChar > ( sz ) ;
	if ( NULL == buf ) {
		fclose ( fp1 ) ;
		fclose ( fp2 ) ;
		remove ( target_file.c_str ( ) ) ;
		return - 3 ;
	}

	int rtn = fread ( buf , 1 , sz , fp1 ) ;
	fclose ( fp1 ) ;
	if ( rtn != sz ) {
		fclose ( fp2 ) ;
		iMem->Delete < UChar > ( buf ) ;
		remove ( target_file.c_str ( ) ) ;
		return - 4 ;
	}

	fwrite ( buf , 1 , sz , fp2 ) ;

	iMem->Delete < UChar > ( buf ) ;
	fclose ( fp2 ) ;
	return D_succ ;
}

//Str CMyStrPar::RtnSerchMyFormat ( void ) {
//	Str str = iSerchMyFormat ;
//	iSerchMyFormat.clear ( ) ;
//	return str ;
//}

int CMyStrPar::ReadFile ( CStr filePath ) {

	iReadFile.clear ( ) ;

	FILE* fp = fopen ( filePath.c_str ( ) , "r+" ) ;
	if ( NULL == fp ) {
		return DF_CMyStrPar_ReadFile_Fopen ;
	}

	while ( 1 ) {
		char buf [ 8191 + 1 ] = { 0 } ;
		int rtn = fread ( buf , 1 , sizeof ( buf ) - 1 , fp ) ;
		if ( - 1 == rtn ) {
			fclose ( fp ) ;
			return DF_CMyStrPar_ReadFile_FREAD ;
		}
		iReadFile += buf ;
		if ( rtn < ( ( int ) sizeof ( buf ) - 1 ) ) {
			break ;
		}
	}
	fclose ( fp ) ;

	std::string::size_type poz = iReadFile.find ( '\n' , iReadFile.size ( ) - 1 ) ;
	if ( poz != iReadFile.npos ) {
		iReadFile.assign ( iReadFile , 0 , poz ) ;
	}
	return D_succ ;
}
Str CMyStrPar::RtnReadFile ( void ) {
	Str str = iReadFile ;
	iReadFile.clear ( ) ;
	return str ;
}


Str CMyStrPar::Read_file_data ( Str file_path ) { // 2016 04 11
	if ( D_succ != ReadFile ( file_path ) ) {
		return "" ;
	}
	return RtnReadFile ( ) ;
}


/*
 * write str to file
 */
int CMyStrPar::Write_str_to_file_w ( CStr file_path , Str& data ) {
	FILE* fp = fopen ( file_path.c_str ( ) , "w+" ) ;
	if ( NULL == fp ) {
		return D_CMyStrPar_open_file_error ;
	}
	fwrite ( data.c_str ( ) , 1 , data.size ( ) , fp ) ;
	fclose ( fp ) ;
	return D_succ ;
}

int CMyStrPar::Write_str_to_file_a ( CStr file_path , Str& data ) {
	FILE* fp = fopen ( file_path.c_str ( ) , "a+" ) ;
	if ( NULL == fp ) {
		return D_CMyStrPar_open_file_error ;
	}
	fseek ( fp , 0 , SEEK_END ) ;
	fwrite ( data.c_str ( ) , 1 , data.size ( ) , fp ) ;
	fclose ( fp ) ;
	return D_succ ;
}

/*
 * write bin to file
 */
int CMyStrPar::Write_bin_to_file_w ( CStr file_path , void* p_data , int sz ) {
	FILE* fp = fopen ( file_path.c_str ( ) , "wb+" ) ;
	if ( NULL == fp ) {
		return D_CMyStrPar_open_file_error ;
	}
	fwrite ( p_data , 1 , sz , fp ) ;
	fclose ( fp ) ;
	return D_succ ;
}

int CMyStrPar::Write_bin_to_file_a ( CStr file_path , void* p_data , int sz ) {
	FILE* fp = fopen ( file_path.c_str ( ) , "ab+" ) ;
	if ( NULL == fp ) {
		return D_CMyStrPar_open_file_error ;
	}
	fseek ( fp , 0 , SEEK_END ) ;
	fwrite ( p_data , 1 , sz , fp ) ;
	fclose ( fp ) ;
	return D_succ ;
}
LInt CMyStrPar::Rtn_file_sz ( CStr file_path ) {
	FILE* fp = fopen ( file_path.c_str ( ) , "a+" ) ;
	if ( NULL == fp ) {
		return - 1 ;
	}
	fseek ( fp , 0 , SEEK_END ) ;
	LInt sz = ftell ( fp ) ;
	fclose ( fp ) ;
	return sz ;
}
int CMyStrPar::Write_to_file_clamp_sz ( CStr file_path , int clamp_sz , Str& data ) {

	int rtn = Rtn_file_sz ( file_path ) ;
	if ( - 1 == rtn ) {
		return - 2 ;
	}
	if ( rtn > clamp_sz ) {
		remove ( file_path.c_str ( ) ) ;
	}
	return Write_str_to_file_a ( file_path , data ) ;
}















#if 0
e.g.
FILE* fp = fopen("./ttttt" ,"w") ;
if(NULL == fp) {
	std::cerr << "EEEEEEEEEEEe" << std::endl ;
	return 0 ;
}

fprintf ( fp , "\x12\x23\x34\x45\x56" ) ;
fflush ( fp ) ;
fclose ( fp ) ;

CMyStrPar obj ;
int aa= obj.ReadFileBin1("./ttttt") ;
std::cout << "aa:" << aa << std::endl ;

CStrType& tmp = obj.RtnReadFileBin1() ;
std::cout<< "@@@:"<< tmp.RtnStr() << std::endl ;
std::cout<< "@@@:"<< tmp.RtnNewHexStr() << std::endl ;
#endif
int CMyStrPar::ReadFileBin ( CStr filePath ) {

	iReadFileBin->Clear ( ) ;

	FILE* fp = fopen ( filePath.c_str ( ) , "r+" ) ;
	if ( NULL == fp ) {
		return DF_CMyStrPar_ReadFileBin_Fopen ;
	}

	fseek ( fp , 0 , SEEK_END ) ;
	LInt fileSz = ftell ( fp ) ;
	if ( - 1 == fileSz ) {
		fclose ( fp ) ;
		return DF_CMyStrPar_ReadFileBin_Ftell ;
	}
	fseek ( fp , 0 , SEEK_SET ) ;

	UChar* nwBuf = iMem->New < UChar > ( fileSz + 1 ) ;
	if ( NULL == nwBuf ) {
		fclose ( fp ) ;
		return DF_CMyStrPar_ReadFileBin_CMem_New ;
	}

	size_t sz = fread ( nwBuf , 1 , fileSz , fp ) ;
	fclose ( fp ) ;
	if ( sz != ( size_t ) fileSz ) {
		iMem->Delete < UChar > ( nwBuf ) ;
		return DF_CMyStrPar_ReadFileBin_Fread ;
	}

	iReadFileBin->Assign ( 0 , sz , ( void* ) nwBuf ) ;
	return D_succ ;
}
CStrType*& CMyStrPar::RtnReadFileBin ( void ) {
	return iReadFileBin ;
}
CStrType* CMyStrPar::Read_file_bin ( CStr file_path ) {  // 2016 04 11
	if ( D_succ != ReadFileBin ( file_path ) ) {
		return NULL ;
	}
	return iReadFileBin ;
}

#if 0
// 資料錯誤會導致崩潰 在看看
int CMyStrPar::DecodeMyIpcFormat ( Str& sourStr ) {
	iDecodeMyIpcFormat.clear ( ) ;

#define DF_HEAD_1         "<S>"
#define DF_HEAD_SZ_1      ( 3 )
#define DF_DATA_STR_LEN_1 ( 8 )
	// 基本大小不足夠 3 = "<s>" , 8 = 後面資料長度//
	if ( ( DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 ) > sourStr.size ( ) ) {
		return DF_CMyStrPar_DecodeMyIpcFormat_Data_Less ;
	}

	Str::size_type findPos = sourStr.find ( DF_HEAD_1 ) ;
	if ( sourStr.npos == findPos ) {
		sourStr.clear ( ) ;
		return DF_CMyStrPar_DecodeMyIpcFormat_No_Head ;
	}
	// 先作第一次移動 ,後面流程比較好作 //

	// 取得資料度 //
	Str lenStrBuf ;
	lenStrBuf.assign ( sourStr , findPos + DF_HEAD_SZ_1 , DF_DATA_STR_LEN_1 ) ;
	ULInt dataSz = strtoul ( lenStrBuf.c_str ( ) , NULL , 16 ) ;
	if ( 0 >= dataSz ) {   // 表示資料有誤,拋棄資料
		sourStr.assign ( sourStr , findPos + DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 , sourStr.size ( ) - ( findPos + DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 ) ) ;
		return DF_CMyStrPar_DecodeMyIpcFormat_Data_SZ_0 ;// 因為下次近來就會在找尋下一個<S>
	}

	if ( ( DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 + dataSz ) > sourStr.size ( ) ) {
		return DF_CMyStrPar_DecodeMyIpcFormat_Data_Less ;
	}
	sourStr.assign ( sourStr , findPos + DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 , sourStr.size ( ) - ( findPos + DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 ) ) ;
	iDecodeMyIpcFormat.assign ( sourStr , 0 , dataSz ) ;
	// 先作第二次移動 ,後面流程比較好作 //
	sourStr.assign ( sourStr , dataSz , sourStr.size ( ) - dataSz ) ;

	return D_succ ;
}
#endif
#if 1
int CMyStrPar::DecodeMyIpcFormat ( Str& sour_str ) {
	iDecodeMyIpcFormat.clear ( ) ;

#define DF_HEAD_1         "<S>"
#define DF_HEAD_SZ_1      ( 3 )
#define DF_DATA_STR_LEN_1 ( 8 )

	if ( ( DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 )> sour_str.size() ) {
		return DF_CMyStrPar_DecodeMyIpcFormat_Data_Less ;
	}

	char buf [ sour_str.size ( ) + 1 ] ;
	memcpy ( buf , sour_str.c_str ( ) , sizeof ( buf ) - 1 ) ;
	sour_str.clear ( ) ;
	buf [ sizeof ( buf ) - 1 ] = 0 ;

	char* p_s = strstr ( buf , DF_HEAD_1 ) ;
	if ( NULL == p_s ) {
		sour_str.clear ( ) ;
		return DF_CMyStrPar_DecodeMyIpcFormat_No_Head ;
	}

	if ( ( DF_HEAD_SZ_1 + DF_DATA_STR_LEN_1 )> strlen ( p_s ) ){
		return DF_CMyStrPar_DecodeMyIpcFormat_Data_Less ;
	}

	p_s += DF_HEAD_SZ_1 ;

	char sz_buf [ DF_DATA_STR_LEN_1 + 1 ] ;
	memcpy ( sz_buf , p_s , sizeof ( sz_buf ) - 1 ) ;
	sz_buf [ sizeof ( sz_buf ) - 1 ] = 0 ;
	ULInt sz = strtoul ( sz_buf , NULL , 16 ) ;
	if ( 0 >= sz ) {   // 表示資料有誤,拋棄資料
		sour_str.assign ( p_s , 0 , strlen ( p_s ) ) ;
		return DF_CMyStrPar_DecodeMyIpcFormat_Data_SZ_0 ;   // 因為下次近來就會在找尋下一個<S>
	}

	p_s += DF_DATA_STR_LEN_1 ;
	if ( sz > strlen ( p_s ) ) {
		sour_str.assign ( buf , 0 , strlen ( buf ) ) ;
		return DF_CMyStrPar_DecodeMyIpcFormat_Data_Less ;
	}
	sour_str.assign ( p_s + sz , 0 , strlen ( p_s + sz ) ) ;
	iDecodeMyIpcFormat.assign ( p_s , 0 , sz ) ;

	return D_succ ;
}
#endif

Str CMyStrPar::RtnMyIpcFormatStr ( void ) {
	Str str = iDecodeMyIpcFormat ;
	iDecodeMyIpcFormat.clear ( ) ;
	return str ;
}

Str CMyStrPar::Decode_my_ipc_fmt ( Str& sour_str ) {   // 2016 04 11
	if ( D_succ != DecodeMyIpcFormat ( sour_str ) ) {
		return "" ;
	}
	return RtnMyIpcFormatStr ( ) ;
}


int CMyStrPar::Parser_self_fmt_all_data ( Str sour ) {
	i_self_fmt_value.clear ( ) ;
	i_self_fmt_list.clear ( ) ;
	if ( 0 == sour.size ( ) ) {
		return D_fail ;
	}

	sour += "\n";   // 為了方便作業.

	char sourBuf [ ( int ) sour.size ( ) + 1 ] ;
	snprintf ( sourBuf , sizeof ( sourBuf ) , "%s" , sour.c_str ( ) ) ;

	char* ptrBegin = sourBuf ;
	bool isLast = false ;
	while ( false == isLast ) {

		char * ptrEnd = strchr ( ptrBegin , '\n' ) ;
		if ( NULL == ptrEnd ) {
			if ( ptrEnd != & sourBuf [ strlen ( sourBuf ) ] ) {
				ptrEnd = & sourBuf [ strlen ( sourBuf ) ] ;
				isLast = true ;
			}
		}
		* ptrEnd = 0 ;

		// 將資料取出處理 //
		char bufTmp [ ( int ) ( ptrEnd - ptrBegin ) + 1 ] ;
		snprintf ( bufTmp , sizeof ( bufTmp ) , "%s" , ptrBegin ) ;

		if ( false == isLast ) {
			ptrBegin = ++ ptrEnd ;   // 為了下次搜尋
		}

		char* ptrKeyBegin = strpbrk ( bufTmp , "$#" ) ;
		if ( ( NULL == ptrKeyBegin ) || ( * ptrKeyBegin == '#' ) ) {
			continue ;   // 往下一個處理
		}

		char* ptrKeyEnd = strpbrk ( ptrKeyBegin , " :=" ) ;
		if ( NULL == ptrKeyEnd ) {
			continue ;   // 往下一個處理
		}
		* ptrKeyEnd = 0 ;

		CMyFmt obj ;
		obj.iKey = ptrKeyBegin ;
		++ ptrKeyEnd ;
		char* ptrValBegin = strchr ( ptrKeyEnd , '\"' ) ;
		if ( NULL == ptrValBegin ) {
			continue ;   // 往下一個處理
		}
		++ ptrValBegin ;

		char* ptrValEnd = strchr ( ptrValBegin , '\"' ) ;
		if ( NULL == ptrValEnd ) {
			continue ;   // 往下一個處理
		}
		* ptrValEnd = 0 ;
		obj.iValue = ptrValBegin ;
		i_self_fmt_list.push_back ( obj ) ;
	}
	if ( true == i_self_fmt_list.empty ( ) ) {
		return D_fail ;
	}

	return D_succ ;
}

int CMyStrPar::Serch_self_fmt ( Str key ) {
	i_self_fmt_value.clear ( ) ;
	std::list < CMyFmt >::iterator ptr = i_self_fmt_list.begin ( ) ;
	while ( ptr != i_self_fmt_list.end ( ) ) {
		if ( ptr->iKey == key ) {
			i_self_fmt_value = ptr->iValue ;
			return D_succ ;
		}
		++ ptr ;
	}
	return D_fail ;
}
Str CMyStrPar::Rtn_serch_self_fmt ( void ) {
	return i_self_fmt_value ;
}

Str CMyStrPar::Search_self_fmt_and_rtn_value ( Str key ) {
	int rtn = Serch_self_fmt ( key ) ;
	if ( D_succ != rtn ) {
		return "" ;
	}
	return Rtn_serch_self_fmt ( ) ;
}

/*
 * 從頭開始搜尋
 * CUChar* parserStr : 被搜尋的資料
 * int sz                         : 被搜尋的資料大小
 * UChar c                : 要搜尋的 Byte
 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
 */
int CMyStrPar::MyStrchr ( CUChar* parserStr , int sz , UChar c ) {
	/* 確認參數是否正確 */
	bool prmtBool = ( NULL == parserStr ) ;
	prmtBool |= ( 0 >= sz ) ;
	if ( true == prmtBool ) {
		return DF_Parameter ;
	}
	/* END 確認參數是否正確 */

	for ( int i = 0 ; i < sz ; ++ i ) {
		if ( c == parserStr [ i ] ) {
			return i ;
		}
	}
	return DF_CMyStrPar_MyStrchr ;
}
int CMyStrPar::Bin_strchr ( CUChar* parser_str , int sz , UChar c ) { // 2016 04 11
	return MyStrchr ( parser_str , sz , c ) ;
}

/*
 * 從尾開始搜尋
 * CUChar* parserStr : 被搜尋的資料
 * int sz                         : 被搜尋的資料大小
 * UChar c                : 要搜尋的 Byte
 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
 */
int CMyStrPar::MyStrrchr ( CUChar* parserStr , int sz , UChar c ) {
	/* 確認參數是否正確 */
	bool prmtBool = ( NULL == parserStr ) ;
	prmtBool |= ( 0 >= sz ) ;
	if ( true == prmtBool ) {
		return DF_Parameter ;
	}
	/* END 確認參數是否正確 */

	while ( sz ) {
		if ( c == parserStr [ -- sz ] ) {
			return sz ;
		}
	}
	return DF_CMyStrPar_MyStrrchr ;
}

/*
 * 從頭開始搜尋
 * CUChar* parserStr : 被搜尋的資料
 * int sz                         : 被搜尋的資料大小
 * CUChar* str       : 要搜尋的資料
 * int strSz                      : 要搜尋的資料大小
 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
 */
int CMyStrPar::MyStrstr ( CUChar* parserStr , int sz , CUChar* str , int strSz ) {
	/* 確認參數是否正確 */
	bool prmtBool = ( NULL == parserStr ) ;
	prmtBool |= ( 0 >= sz ) ;
	prmtBool |= ( NULL == str ) ;
	prmtBool |= ( 0 >= strSz ) ;
	if ( true == prmtBool ) {
		return DF_Parameter ;
	}
	/* END 確認參數是否正確 */

	int i = 0 ;
	for ( i = 0 ; i < sz ; ++ i ) {
		if ( * str == parserStr [ i ] ) {
			if ( ( sz - i ) < strSz ) {
				return - 1 ;
			} else if ( 0 == memcmp ( str , parserStr + i , strSz ) ) {
				return i ;
			}
		}
	}
	return DF_CMyStrPar_MyStrstr ;
}

/*
 * 從尾開始搜尋
 * CUChar* parserStr : 被搜尋的資料
 * int sz                         : 被搜尋的資料大小
 * CUChar* str       : 要搜尋的資料
 * int strSz                      : 要搜尋的資料大小
 * return                         : 回吐搜尋到的資料(從開頭開始算第幾個位置) 0 start
 */
int CMyStrPar::MyStrrstr ( CUChar* parserStr , int sz , CUChar* str , int strSz ) {
	/* 確認參數是否正確 */
	bool prmtBool = ( NULL == parserStr ) ;
	prmtBool |= ( 0 >= sz ) ;
	prmtBool |= ( NULL == str ) ;
	prmtBool |= ( 0 >= strSz ) ;
	if ( true == prmtBool ) {
		return DF_Parameter ;
	}
	/* END 確認參數是否正確 */

	sz = sz - strSz + 1 ;
	while ( sz ) {
		if ( * str == parserStr [ -- sz ] ) {
			if ( 0 == memcmp ( str , parserStr + sz , strSz ) ) {
				return sz ;
			}
		}
	}
	return DF_CMyStrPar_MyStrrstr ;
}

int CMyStrPar::Mkdir_func ( CStr path ) {
	Str str = path ;
	if ( '/' != str [ str.size ( ) - 1 ] ) {
		str += "/" ;
	}

	Str::size_type pz = 0 ;
	Str str_tmp = "" ;
	while ( 1 ) {
		pz = str.find ( '/' , pz ) ;
		if ( pz == Str::npos ) {
			break ;
		}
		str_tmp = str.substr ( 0 , pz ) ;
		mkdir ( str_tmp.c_str ( ) , 0750 ) ;
		++ pz ;
	}

	return D_succ ;
}

int CMyStrPar::MyMkdir ( CStr path ) {
	Str cmmdStr = "mkdir -p " + path ;
	if ( 0 != system ( cmmdStr.c_str ( ) ) ) {
		return DF_CMyStrPar_MyMkdir ;
	}
	return D_succ ;
}

/*
 * 刪除大於 sz 大小的檔案
 */
int CMyStrPar::DelFileTooBig ( CStr path , LInt sz ) {
	FILE* fp = fopen ( path.c_str ( ) , "r+" ) ;
	if ( NULL == fp ) {
		return DF_CMyStrPar_DelFileTooBig_Fopen ;
	}
	fseek ( fp , 0 , SEEK_END ) ;
	int file_sz = ftell ( fp ) ;
	fclose ( fp ) ;
	if ( file_sz >= sz ) {
		remove ( path.c_str ( ) ) ;
	}
	return D_succ ;
}

/*
 * Tar 大於 sz 大小的檔案
 */
int CMyStrPar::TarFileTooBig ( CStr filePath , FILE*& sFp , LInt sz , CStr tarName ) {
	fseek ( sFp , 0 , SEEK_END ) ;
	int fileSz = ftell ( sFp ) ;
	if ( fileSz >= sz ) {
		fclose ( sFp ) ;
		Str commdStr ;
		commdStr = "tar -zcf " + tarName + " " + filePath ;
		if ( 0 != system ( commdStr.c_str ( ) ) ) {
			return DF_CMyStrPar_TarFileTooBig_System ;
		}
		sFp = fopen ( filePath.c_str ( ) , "w+" ) ;
		if ( NULL == sFp ) {
			return DF_CMyStrPar_TarFileTooBig_Fopen ;
		}
	}
	return D_succ ;
}

