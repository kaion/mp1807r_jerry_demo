#include <iostream>
#include <pthread.h>

using namespace std;

#include <time.h>
#include "CPS_API.h"
#include "OLEDLib.h"
#include "Run_ECC_API.h"
#include "Log.h"
#include "LEDLib.h"

pthread_mutex_t g_mutex;

CCPS_API obj;

#define g_Version "ECoinAccpetor_v1_00_b2"

#define LED_B_ON   setLedValue(18, 1)
#define LED_B_OFF  setLedValue(18, 0)
#define LED_G_ON   setLedValue(19, 1)
#define LED_G_OFF  setLedValue(19, 0)
#define LED_Y_ON   setLedValue(20, 1)
#define LED_Y_OFF  setLedValue(20, 0)
#define LED_R_ON   setLedValue(21, 1)
#define LED_R_OFF  setLedValue(21, 0)

char Word_Cnt = 0, g_Word[255+1] = {0}, g_flag_Marquee = 1;
void *runOledAp(void *)
{

    OLED_Open();
    inital();

    while(1)
    {
    	if(g_flag_Marquee == 1)
    	{
			CleanDDR();
			Run_Marquee_32X32_API(Word_Cnt, g_Word);
    	}
        usleep(500000);
    }
    OLED_Close();

    pthread_exit(NULL);
}

void *runHeartbeatAp(void *)
{

    while(1)
    {
    	if( obj.i_hearBeatCycleTime != "" )
		{
			sleep( ( atoi(obj.i_hearBeatCycleTime.c_str()) * 60 ));
		}
    	while(1)
    	{
			if(pthread_mutex_lock(&g_mutex) != 0)
			{
				continue;
			}
			obj.Get_HeartBeat ( ) ;

			pthread_mutex_unlock(&g_mutex);
			break;
    	}
    }

    pthread_exit(NULL);
}

int main()
{
	//Word_Cnt = strlen(SETFONTS0);
	//memcpy(g_Word, SETFONTS0, Word_Cnt);
	g_Word[0] = '0';

	setUsbPowerInit();
	setUsbPowerValue(500, 0);
	setUsbPowerValue(0, 1);

    Str cf = "./config/cps.cf" ;
	FILE *fp = fopen ( cf.c_str ( ) , "r" ) ;
	if ( NULL == fp ) {
		fp = fopen ( cf.c_str ( ) , "w" ) ;
		fprintf ( fp , "$deviceNo       : \"1001\"\n" ) ;
		fprintf ( fp , "$CinPulseTime    : \"100\"\n" ) ;
		fprintf ( fp , "$cpsserver      : \"10.0.0.67\"\n" ) ;
		fprintf ( fp , "$port           : \"443\"\n" ) ;
	}
	if ( NULL != fp ) {
		fclose ( fp ) ;
	}

	setLedInit();
	setCinInit();

	LED_Y_ON;
    int rtn = 0 ;
	while ( 1 ) {
		rtn = obj.Import_path ( cf ) ;
		if ( D_succ == rtn ) {
			break ;
		}
		Str Data = "[Main] [Import_path] [file error]";
		writeLog(Data);
		sleep ( 5 ) ;
	}

	pthread_t p_OLED = 0;

	int res_flag = pthread_create(&p_OLED, NULL, runOledAp, NULL);
	if( res_flag != 0 )
	{
		Str Data = "[Main] [OLED] [pthread error]";
		writeLog(Data);
		LED_Y_OFF;
		return 0;
	}

	obj.i_deviceVersion = g_Version;

	rtn = obj.Get_apiLogin ( ) ;
	if ( D_succ != rtn ) {
		Str Data = "[Main] [CPS] [apiLogin error]";
		writeLog(Data);
		LED_Y_OFF;
		return rtn ;
	}

	Str cardNo = "", recodrCode = "A001", remark = "系統開機";
	rtn = obj.Set_uploadSystemLog ( cardNo, recodrCode, remark ) ;
	if ( D_succ != rtn ) {
		Str Data = "[Main] [CPS] [uploadSystemLog error]";
		writeLog(Data);
		LED_Y_OFF;
		sleep(2);
		//return rtn ;
	}

	rtn = obj.Get_ProxySetting ( ) ;
	if ( D_succ != rtn ) {
		Str Data = "[Main] [CPS] [ProxySetting error]";
		writeLog(Data);
		LED_Y_OFF;
		cardNo = "";
		recodrCode = "A003";
		remark = "取得Proxy參數失敗";
		rtn = obj.Set_uploadSystemLog ( cardNo, recodrCode, remark ) ;
		if ( D_succ != rtn ) {
			Str Data = "[Main] [CPS] [uploadSystemLog error]";
			writeLog(Data);
			LED_Y_OFF;
			sleep(2);
			//return rtn ;
		}
		return rtn ;
	}

	cardNo = "";
	recodrCode = "A002";
	remark = "取得Proxy參數成功";
	rtn = obj.Set_uploadSystemLog ( cardNo, recodrCode, remark ) ;
	if ( D_succ != rtn ) {
		Str Data = "[Main] [CPS] [uploadSystemLog error]";
		writeLog(Data);
		LED_Y_OFF;
		sleep(2);
		//return rtn ;
	}

	rtn = obj.Get_ECCSetting ( ) ;
	if ( D_succ != rtn ) {
		Str Data = "[Main] [CPS] [ECCSetting error]";
		writeLog(Data);
		LED_Y_OFF;
		cardNo = "";
		recodrCode = "A004";
		remark = "取得ECC參數失敗";
		rtn = obj.Set_uploadSystemLog ( cardNo, recodrCode, remark ) ;
		if ( D_succ != rtn ) {
			Str Data = "[Main] [CPS] [uploadSystemLog error]";
			writeLog(Data);
			LED_Y_OFF;
			sleep(2);
			//return rtn ;
		}
		return rtn ;
	}

	cardNo = "";
	recodrCode = "A004";
	remark = "取得ECC參數成功";
	rtn = obj.Set_uploadSystemLog ( cardNo, recodrCode, remark ) ;
	if ( D_succ != rtn ) {
		Str Data = "[Main] [CPS] [uploadSystemLog error]";
		writeLog(Data);
		LED_Y_OFF;
		sleep(2);
		//return rtn ;
	}

	if( pthread_mutex_init(&g_mutex, NULL) != 0)
	{
		Str Data = "[Main] [pthread] [mutex init error]";
		writeLog(Data);
		LED_Y_OFF;
		return 0;
	}

	pthread_t p_HeartBeat = 0;
	int res_p_HeartBeat = pthread_create(&p_HeartBeat, NULL, runHeartbeatAp, NULL);
	if( res_p_HeartBeat != 0 )
	{
		Str Data = "[Main] [pthread] [HeartBeat error]";
		writeLog(Data);
		LED_Y_OFF;
		return 0;
	}
	//rtn = obj.Get_ConfigSetting ( ) ;
	//if ( D_succ != rtn ) {
	//	fprintf ( stderr , "main Get_ConfigSetting error\n\n" ) ;
	//	sleep(2);
		//return rtn ;
	//}

	unsigned int NU = 1;
	int res = 0;
	while(1)
	{
		res = runEccSignOnApi(NU);
		NU++;
		if(res == 0)
		{
			break;
		}
		else
		{
			Str Data = "[Main] [悠] [SignOn error]";
			writeLog(Data);
			sleep(1);
			continue;
		}
	}

	char Tmp_T0215[40+1] = {0}, flagShow = 0;
	time_t uploadMonitorDataTime = 0;
	LED_Y_OFF;
	g_flagMarqueeStop = 1;
	g_Word[0] = '1';
	while(1)
	{
		LED_B_ON;
		if( (time(NULL) - uploadMonitorDataTime) > (10 * 60) )
		{
			while(1)
			{
				if(pthread_mutex_lock(&g_mutex) != 0)
				{
					continue;
				}

				rtn = obj.Set_uploadMonitorData (  ) ;
				pthread_mutex_unlock(&g_mutex);
				if ( D_succ != rtn ) {
					Str Data = "[Main] [CPS] [uploadMonitorData error]";
					writeLog(Data);
				}
				else
				{
					Str Data = "[Main] [CPS] [uploadMonitorData OK]";
					writeLog(Data);
				}
				uploadMonitorDataTime = time(NULL);
				break;
			}
		}
		//Word_Cnt = 8;
		//memset(g_Word, 0x00, 8);

		char T0215[40+1] = {0};
		res = runEccReadCardBasicApi(NU, T0215);
		NU++;
		LED_B_OFF;
		if(res == 0)
		{
			Str Data = "[Main] [悠] [ReadCardBasic ok]";
			writeLog(Data);
		}
		else
		{
			Str Data = "[Main] [悠] [ReadCardBasic error]";
			writeLog(Data);

			memset(Tmp_T0215, 0, sizeof(Tmp_T0215));
			sleep(2);
			if(flagShow == 1)
			{
				flagShow = 0;
				g_flagMarqueeStop = 1;
				g_Word[0] = '1';
			}
			continue;
		}

		if( (obj.i_deductionMode[0] == '1') && (memcmp(T0215, Tmp_T0215, sizeof(T0215)) == 0) )
		{
			if(flagShow == 0)
			{
				g_flagMarqueeStop = 1;
				flagShow = 1;
			}
			g_Word[0] = '3';
			continue;
		}
		memcpy(Tmp_T0215, T0215, sizeof(T0215));

		//Word_Cnt = 10;
		//memset(g_Word, 0x01, 10);
		g_flagMarqueeStop = 1;
		g_Word[0] = '2';

		LED_Y_ON;
		char T4110[50] = {0}, T5501[8+1] = {0}, T3700[50] = {0}, T0415[8+1] = {0}, T0409[8+1] = {0}, T0410[8+1] = {0};
		res = runEccDeductApi(NU, atoi(obj.i_chargeAmount.c_str()), T0215, T4110, T5501, T3700, T0415, T0409, T0410 );
		LED_Y_OFF;
		NU++;
		if(res == 0)
		{
			Str Data = "[Main] [悠] [Deduct ok]";
			writeLog(Data);
			LED_G_ON;
			setCinValue(atoi(obj.i_CinPulseTime.c_str()), 1);
		}
		else
		{
			Str Data = "[Main] [悠] [Deduct Error]";
			writeLog(Data);
			g_flagMarqueeStop = 1;
			g_Word[0] = '1';
			sleep(2);
			continue;
		}

		while(1)
		{
			if(pthread_mutex_lock(&g_mutex) != 0)
			{
				continue;
			}

			cardNo = "";
			recodrCode = "B001";
			remark = obj.i_chargeAmount;
			rtn = obj.Set_uploadSystemLog ( cardNo, recodrCode, remark ) ;
			pthread_mutex_unlock(&g_mutex);
			if ( D_succ != rtn ) {
				Str Data = "[Main] [CPS] [uploadSystemLog error]";
				writeLog(Data);
				continue;
			}
			Str Data = "[Main] [CPS] [uploadSystemLog OK]";
			writeLog(Data);
			break;
		}

		//Word_Cnt = 10;
		//memset(g_Word, 0x03, 10);
		g_flagMarqueeStop = 1;
		g_Word[0] = '4';
		usleep(500000);

		Str payment = "1";
		Str readerDeviceNo = T4110;
		Str batchNo = T5501;
		Str RRN = T3700;
		T0415[strlen(T0415)-2] = 0;
		Str beforBalance = T0415;
		if(atoi(T0409) != 0)
		{
			T0409[strlen(T0409)-2] = 0;
		}
		Str autoLoadAmout = T0409;
		T0410[strlen(T0410)-2] = 0;
		Str afterBalance = T0410;

		cardNo = T0215;
		setCinValue(0, 0);
		while(1)
		{
			if(pthread_mutex_lock(&g_mutex) != 0)
			{
				continue;
			}

			rtn = obj.Set_uploadTransactionRecord ( cardNo, payment, readerDeviceNo, batchNo, RRN, beforBalance, autoLoadAmout, afterBalance ) ;
			pthread_mutex_unlock(&g_mutex);
			if ( D_succ != rtn ) {
				Str Data = "[Main] [CPS] [uploadTransactionRecord error]";
				writeLog(Data);
				continue;
			}
			Str Data = "[Main] [CPS] [uploadTransactionRecord OK]";
			writeLog(Data);
			break;
		}

		//Word_Cnt = 9;
		//memset(g_Word, 0x05, 9);
		g_flagMarqueeStop = 1;
		g_Word[0] = '6';

		char T3912[50] = {0};
		memset(T5501, 0, sizeof(T5501));

		while(1)
		{
			res = runEccSettleApi(NU, T5501, T3912);
			NU++;
			if(res == 0)
			{
				Str Data = "[Main] [悠] [Settle OK]";
				writeLog(Data);
				break;
			}
			else
			{
				Str Data = "[Main] [悠] [Settle Error]";
				writeLog(Data);
				continue;
			}
		}

		//cardNo = "00000000000000000000";
		Str checkoutBatchNo = T5501;
		Str accountingStatus = T3912;
		Str numbersPurchase = "1";
		Str numbersRefund = "0";
		Str numbersAutoLoad = "0";
		if( atoi(T0409) != 0 )
		{
			numbersAutoLoad = "1";
		}
		char Tmp_Nu[10+1] = {0};
		sprintf(Tmp_Nu, "%d", atoi(numbersPurchase.c_str()) + atoi(numbersAutoLoad.c_str()));
		Str numbersTotal = Tmp_Nu;
		extern char g_T0400[8+1];
		g_T0400[strlen(g_T0400)-2] = 0;
		sprintf(Tmp_Nu, "%d", atoi(g_T0400) - atoi(T0409));
		Str amountPurchase = Tmp_Nu;
		Str amountRefund = "0";
		Str amountAutoLoad = T0409;
		Str amountTotal = g_T0400;
		Str transactionNumbers = numbersTotal;
		Str transactionAmount = amountTotal;
		Str transactionNetAmount = amountPurchase;

		while(1)
		{
			if(pthread_mutex_lock(&g_mutex) != 0)
			{
				continue;
			}

			rtn = obj.Set_uploadCheckoutRecord ( cardNo, checkoutBatchNo, accountingStatus, readerDeviceNo, numbersPurchase, numbersRefund, numbersAutoLoad, numbersTotal, amountPurchase, amountRefund, amountAutoLoad, amountTotal, transactionNumbers, transactionAmount, transactionNetAmount ) ;
			pthread_mutex_unlock(&g_mutex);
			if ( D_succ != rtn ) {
				// log
				Str Data = "[Main] [CPS] [uploadTransactionRecord error]";
				writeLog(Data);

				continue;
			}

			Str Data = "[Main] [CPS] [uploadTransactionRecord OK]";
			writeLog(Data);
			break;
		}

		int Balance = 0;
		g_flag_Marquee = 0;
		Balance = atoi(T0410);
		g_flagMarqueeStop = 1;
		runBalance_API(Balance);
		sleep(2);
		LED_G_OFF;
		g_flag_Marquee = 1;
		g_flagMarqueeStop = 1;
		g_Word[0] = '1';
	}

	pthread_mutex_destroy(&g_mutex);

    return 0;
}
