#ifndef __OLEDLIB_H
#define __OLEDLIB_H

#ifdef __cplusplus
extern "C"
{
#endif
extern char g_flagMarqueeStop;
extern char OLED_Open(void);
extern void OLED_Close(void);
extern void inital(void);
extern void CleanDDR(void);
extern void OLEDShowFont_16X32(unsigned char cLine, unsigned char cColumn, unsigned int iLen, unsigned char *cpString);
extern void OLEDShowFont_32X32(unsigned char cLine, unsigned char cColumn, unsigned int iLen, unsigned char *cpString);
extern void Run_Marquee_32X32_API(char Word_conut, char * Word);
extern void runBalance_API(int Balance);
#ifdef __cplusplus
}
#endif

#endif

