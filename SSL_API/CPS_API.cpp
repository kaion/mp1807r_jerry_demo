#include <iostream>
#include <stdio.h>

#include "CPS_API.h"
#include "jsonxx.h"
#include "Log.h"

using namespace std;
using namespace jsonxx;

CCPS_API::CCPS_API ( ) {
	i_mem = new CMem [ 1 ] ;
	i_parser = i_mem->New < CMyStrPar > ( 1 ) ;
	i_client = i_mem->New < CClient > ( 1 ) ;
	i_cpto = i_mem->New < CCrypto > ( 1 ) ;

	i_client->Connect ( "www.google.com" , "443" ) ;
	i_LocalIP = i_client->Rtn_IP_str ( ) ;
	i_client->CloseSsl ( ) ;
}
CCPS_API::~CCPS_API ( ) {
	i_mem->Delete < CMyStrPar > ( i_parser ) ;
	i_mem->Delete < CClient > ( i_client ) ;
	i_mem->Delete < CCrypto > ( i_cpto ) ;
	if ( NULL != i_mem ) {
		delete [ ] i_mem ;
	}
}

int CCPS_API::Import_path ( Str conf_file ) {
	i_conf_file = conf_file ;
	Str Data;

	int rtn = i_parser->ReadFile ( conf_file ) ;
	if ( D_succ != rtn ) {
		Data = "[API] [";
		Data += "Import_path";
		Data += "] [file error]";
		writeLog(Data);
		return - 1 ;
	}
	Str cf_str = i_parser->RtnReadFile ( ) ;

	rtn = i_parser->Parser_self_fmt_all_data ( cf_str ) ;
	if ( D_succ != rtn ) {
		Data = "[API] [";
		Data += "Import_path";
		Data += "] [format error]";
		writeLog(Data);
		return - 2 ;
	}
    
	i_deviceNo = i_parser->Search_self_fmt_and_rtn_value ( DF_cf__deviceNo ) ;
	if ( 0 == i_deviceNo.size ( ) ) {
		Data = "[API] [";
		Data += "Import_path";
		Data += "] [deviceNo error]";
		writeLog(Data);
		return - 5 ;
	}
    
	i_CinPulseTime = i_parser->Search_self_fmt_and_rtn_value ( DF_cf__CinPulseTime ) ;
	if ( 0 == i_CinPulseTime.size ( ) ) {
		Data = "[API] [";
		Data += "Import_path";
		Data += "] [CinPulseTime error]";
		writeLog(Data);
		return - 5 ;
	}

	i_CPSserver = i_parser->Search_self_fmt_and_rtn_value ( DF_cf__cpsserver ) ;
	if ( 0 == i_CPSserver.size ( ) ) {
		Data = "[API] [";
		Data += "Import_path";
		Data += "] [cpsserver error]";
		writeLog(Data);
		return - 5 ;
	}
    
	i_port = i_parser->Search_self_fmt_and_rtn_value ( DF_cf__Port ) ;
	if ( 0 == i_port.size ( ) ) {
		Data = "[API] [";
		Data += "Import_path";
		Data += "] [cpsport error]";
		writeLog(Data);
		return - 5 ;
	}
    
	Data = "[API] [";
	Data += "Import_path";
	Data += "] [" + i_deviceNo + "]";
	writeLog(Data);

	Data = "[API] [";
	Data += "Import_path";
	Data += "] [" + i_CinPulseTime + "]";
	writeLog(Data);

	Data = "[API] [";
	Data += "Import_path";
	Data += "] [" + i_CPSserver + "]";
	writeLog(Data);

	Data = "[API] [";
	Data += "Import_path";
	Data += "] [" + i_port + "]";
	writeLog(Data);

	return D_succ ;
}

Str CCPS_API::setWriteHttpsData ( Str https_API, Str bady, Str Content_Type  ) {
	Str https = "POST " + https_API + " HTTP/1.0\r\nContent-Length: ";
	char len [ 7 + 1 ] = { 0 } ;
	snprintf ( len , sizeof ( len ) , "%d" , ( int ) bady.size ( ) ) ;
	https += len ;

	https += "\r\nHost: " ;
	https += i_CPSserver ;
	https += "\r\nConnection: close\r\nContent-Type: " + Content_Type + "\r\n\r\n" ;
	https += bady ;

	Str Data;
	Data += "[API] [";
	Data += https_API.substr(5, https_API.size());
	Data += "] [request] [" + bady + "]";
	writeLog(Data);

	struct timeval startTime = {0}, endTime = {0};
	gettimeofday(&startTime, NULL);
	int rtn = i_client->Connect ( i_CPSserver , i_port ) ;
	if ( D_succ != rtn ) {
		Data = "[API] [";
		Data += https_API.substr(5, https_API.size());
		Data += "] [require] [TCP Connect ERROR]";
		writeLog(Data);
		return "" ;
	}

	rtn = i_client->SslConnect ( ) ;
	if ( D_succ != rtn ) {
		Data = "[API] [";
		Data += https_API.substr(5, https_API.size());
		Data += "] [require] [SSL Connect ERROR]";
		writeLog(Data);
		return "" ;
	}

	rtn = i_client->Ssl_write_str ( https ) ;
	if ( D_succ != rtn ) {
		i_client->CloseSsl ( ) ;
		Data = "[API] [";
		Data += https_API.substr(5, https_API.size());
		Data += "] [require] [SSL write ERROR]";
		writeLog(Data);
		return "" ;
	}

	rtn = i_client->Ssl_read_str ( ) ;
	i_client->CloseSsl ( ) ;
	if ( D_succ != rtn ) {
		Data = "[API] [";
		Data += https_API.substr(5, https_API.size());
		Data += "] [require] [SSL Close ERROR]";
		writeLog(Data);
		return "" ;
	}
	Str str = i_client->Rtn_Ssl_read_str ( ) ;
	gettimeofday(&endTime, NULL);
	char Tmp[20+1] = {0};
	sprintf(Tmp, "%ld.%ld",(endTime.tv_sec - startTime.tv_sec), (endTime.tv_usec - startTime.tv_usec));
	Str tmpTmp = Tmp;
	Data = "[API] [";
	Data += https_API.substr(5, https_API.size());
	Data += "] [Times: " + tmpTmp + " s]";
	writeLog(Data);

	Str::size_type pz = str.find ( "[{\"retCode\":" ) ;
	if ( pz == str.npos ) {
		Data = "[API] [";
		Data += https_API.substr(5, https_API.size());
		Data += "] [require] [JSON ERROR]";
		writeLog(Data);
		return "" ;
	}

	Str::size_type end_pz = str.find ( "}]", pz ) ;
	if ( pz == str.npos ) {
		Data = "[API] [";
		Data += https_API.substr(5, https_API.size());
		Data += "] [require] [JSON ERROR]";
		writeLog(Data);
		return "" ;
	}

	Str teststr ;
	teststr.assign ( str , pz + 1, end_pz - ( pz ) ) ;

	Data = "[API] [";
	Data += https_API.substr(5, https_API.size());
	Data += "] [require] [" + teststr + "]";
	writeLog(Data);

	return teststr;
}

int CCPS_API::Get_apiLogin ( void ) {

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\"}" ;

    Str teststr = setWriteHttpsData("/api/apiLogin", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }

    istringstream input(teststr);
    Object o;
    o.parse(input);
    
    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;
    if( o.has<Object>("retData") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retData= " << o.get<Object>("retData") << endl;
    if( o.get<Object>("retData").has<String>("token") == 0)
    {
        return DF_UnSucc ;
    }
    i_token = o.get<Object>("retData").get<String>("token");
//cout << "token= " << i_token << endl;
    if( o.get<Object>("retData").has<Number>("tokenEffectTime") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "tokenEffectTime= " << o.get<Object>("retData").get<Number>("tokenEffectTime") << endl;
    
	return D_succ ;
}

int CCPS_API::Get_HeartBeat ( void ) {

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\"}" ;
    
    Str teststr = setWriteHttpsData("/api/heartBeat", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }
    
    istringstream input(teststr);
    Object o;
    o.parse(input);
    
    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;
    if( o.has<Object>("retData") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retData= " << o.get<Object>("retData") << endl;
    if( o.get<Object>("retData").has<Number>("chargeNo") == 0)
    {
        return DF_UnSucc ;
    }
    char tmp[2+1] = {0};
    memset(tmp, 0, sizeof(tmp));
    sprintf(tmp, "%d", o.get<Object>("retData").get<Number>("chargeNo") );
    i_chargeNo = tmp;
//cout << "chargeNo= " << i_chargeNo << endl;
    if( o.get<Object>("retData").has<Number>("chargeMode") == 0)
    {
        return DF_UnSucc ;
    }
    memset(tmp, 0, sizeof(tmp));
    sprintf(tmp, "%d", o.get<Object>("retData").get<Number>("chargeMode") );
    i_chargeMode = tmp;
//cout << "chargeMode= " << i_chargeMode << endl;
    if( o.get<Object>("retData").has<Number>("chargeAmount") == 0)
    {
        return DF_UnSucc ;
    }
    memset(tmp, 0, sizeof(tmp));
    sprintf(tmp, "%d", o.get<Object>("retData").get<Number>("chargeAmount") );
    i_chargeAmount = tmp;
//cout << "chargeAmount= " << i_chargeAmount << endl;
    if( o.get<Object>("retData").has<Number>("chargeTimes") == 0)
    {
        return DF_UnSucc ;
    }
    memset(tmp, 0, sizeof(tmp));
    sprintf(tmp, "%d", o.get<Object>("retData").get<Number>("chargeTimes") );
    i_chargeTimes = tmp;
//cout << "chargeTimes= " << i_chargeTimes << endl;
    if( o.get<Object>("retData").has<String>("heartBeatCycleTime") == 0)
    {
        return DF_UnSucc ;
    }
    i_hearBeatCycleTime = o.get<Object>("retData").get<String>("heartBeatCycleTime");
//cout << "hearBeatCycleTime= " << i_hearBeatCycleTime << endl;
    if( o.get<Object>("retData").has<Number>("deductionMode") == 0)
	{
		return DF_UnSucc ;
	}
    sprintf(tmp, "%d", o.get<Object>("retData").get<Number>("deductionMode") );
    i_deductionMode = tmp;
//cout << "deductionMode= " << i_deductionMode << endl;
	return D_succ ;
}

int CCPS_API::Get_ProxySetting ( void ) {

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\"}" ;
    
    Str teststr = setWriteHttpsData("/api/getProxySetting", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }
    
    istringstream input(teststr);
    Object o;
    o.parse(input);
    
    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;
    if( o.has<Object>("retData") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retData= " << o.get<Object>("retData") << endl;
    if( o.get<Object>("retData").has<String>("proxyIp") == 0)
    {
        return DF_UnSucc ;
    }
    i_ProxyIp = o.get<Object>("retData").get<String>("proxyIp");
//cout << "i_ProxyIp= " << i_ProxyIp << endl;
    if( o.get<Object>("retData").has<String>("proxyPort") == 0)
    {
        return DF_UnSucc ;
    }
    i_ProxyPort = o.get<Object>("retData").get<String>("proxyPort");
//cout << "i_ProxyPort= " << i_ProxyPort << endl;
    if( o.get<Object>("retData").has<String>("proxyUserid") == 0)
    {
        return DF_UnSucc ;
    }
    i_ProxyUserid = o.get<Object>("retData").get<String>("proxyUserid");
//cout << "proxyUserid= " << i_ProxyUserid << endl;
    if( o.get<Object>("retData").has<String>("poxyPasswd") == 0)
    {
        return DF_UnSucc ;
    }
    i_ProxyPassed = o.get<Object>("retData").get<String>("poxyPasswd");
//cout << "poxyPasswd= " << i_ProxyPassed << endl;
    if( o.get<Object>("retData").has<String>("r6Ip") == 0)
    {
        return DF_UnSucc ;
    }
    i_r6IP = o.get<Object>("retData").get<String>("r6Ip");
//cout << "r6Ip= " << i_r6IP << endl;
    if( o.get<Object>("retData").has<String>("r6Port") == 0)
    {
        return DF_UnSucc ;
    }
    i_r6Port = o.get<Object>("retData").get<String>("r6Port");
//cout << "i_r6Port= " << i_r6Port << endl;
    if( o.get<Object>("retData").has<String>("eccBlcName") == 0)
    {
        return DF_UnSucc ;
    }
    i_eccBlcName = o.get<Object>("retData").get<String>("eccBlcName");
//cout << "i_eccBlcName= " << i_eccBlcName << endl;
    
	if ( D_succ != Get_Proxy_blc_download ( ) ) {
		Str Data;
		Data += "[API] [";
		Data += "ProxySetting";
		Data += "] [blc_download error]";
		writeLog(Data);
		return DF_UnSucc ;
	}

	return D_succ ;
}

int CCPS_API::Get_ECCSetting ( void ) {

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\"}" ;
    
    Str teststr = setWriteHttpsData("/api/getEccSetting", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }
    
    istringstream input(teststr);
    Object o;
    o.parse(input);
    
    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;
    if( o.has<Object>("retData") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retData= " << o.get<Object>("retData") << endl;
    
	Str ICERINI;
    
    ICERINI += "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n<TransXML>\n";
    
    if( o.get<Object>("retData").has<String>("LogFlag") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <LogFlag>";
    ICERINI += o.get<Object>("retData").get<String>("LogFlag");
    ICERINI += "</LogFlag>\n";
    
    if( o.get<Object>("retData").has<String>("DLLVersion") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <DLLVersion>";
    ICERINI += o.get<Object>("retData").get<String>("DLLVersion");
    ICERINI += "</DLLVersion>\n";
    
    if( o.get<Object>("retData").has<String>("TCPIPTimeOut") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <TCPIPTimeOut>";
    ICERINI += o.get<Object>("retData").get<String>("TCPIPTimeOut");
    ICERINI += "</TCPIPTimeOut>\n";
    
    if( o.get<Object>("retData").has<String>("LogCnt") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <LogCnt>";
    ICERINI += o.get<Object>("retData").get<String>("LogCnt");
    ICERINI += "</LogCnt>\n";
    
    if( o.get<Object>("retData").has<String>("ComPort") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ComPort>";
    ICERINI += o.get<Object>("retData").get<String>("ComPort");
    ICERINI += "</ComPort>\n";
    
    if( o.get<Object>("retData").has<String>("ComPort2") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ComPort2>";
    ICERINI += o.get<Object>("retData").get<String>("ComPort2");
    ICERINI += "</ComPort2>\n";
    
    if( o.get<Object>("retData").has<String>("ECC_IP") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ECC_IP>";
    ICERINI += o.get<Object>("retData").get<String>("ECC_IP");
    ICERINI += "</ECC_IP>\n";
    
    if( o.get<Object>("retData").has<String>("ECC_Port") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ECC_Port>";
    ICERINI += o.get<Object>("retData").get<String>("ECC_Port");
    ICERINI += "</ECC_Port>\n";
    
    if( o.get<Object>("retData").has<String>("ICER_IP") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ICER_IP>";
    ICERINI += o.get<Object>("retData").get<String>("ICER_IP");
    ICERINI += "</ICER_IP>\n";
    
    if( o.get<Object>("retData").has<String>("ICER_Port") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ICER_Port>";
    ICERINI += o.get<Object>("retData").get<String>("ICER_Port");
    ICERINI += "</ICER_Port>\n";
    
    if( o.get<Object>("retData").has<String>("CMAS_IP") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <CMAS_IP>";
    ICERINI += o.get<Object>("retData").get<String>("CMAS_IP");
    ICERINI += "</CMAS_IP>\n";
    
    if( o.get<Object>("retData").has<String>("CMAS_Port") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <CMAS_Port>";
    ICERINI += o.get<Object>("retData").get<String>("CMAS_Port");
    ICERINI += "</CMAS_Port>\n";
    
    if( o.get<Object>("retData").has<String>("TMLocationID") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <TMLocationID>";
    ICERINI += o.get<Object>("retData").get<String>("TMLocationID");
    ICERINI += "</TMLocationID>\n";
    
    if( o.get<Object>("retData").has<String>("TMID") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <TMID>";
    ICERINI += o.get<Object>("retData").get<String>("TMID");
    ICERINI += "</TMID>\n";
    
    if( o.get<Object>("retData").has<String>("TMAgentNumber") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <TMAgentNumber>";
    ICERINI += o.get<Object>("retData").get<String>("TMAgentNumber");
    ICERINI += "</TMAgentNumber>\n";
    
    if( o.get<Object>("retData").has<String>("LocationID") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <LocationID>";
    ICERINI += o.get<Object>("retData").get<String>("LocationID");
    ICERINI += "</LocationID>\n";
    
    if( o.get<Object>("retData").has<String>("NewLocationID") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <NewLocationID>";
    ICERINI += o.get<Object>("retData").get<String>("NewLocationID");
    ICERINI += "</NewLocationID>\n";
    
    if( o.get<Object>("retData").has<String>("SPID") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <SPID>";
    ICERINI += o.get<Object>("retData").get<String>("SPID");
    ICERINI += "</SPID>\n";
    
    if( o.get<Object>("retData").has<String>("NewSPID") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <NewSPID>";
    ICERINI += o.get<Object>("retData").get<String>("NewSPID");
    ICERINI += "</NewSPID>\n";
    
    if( o.get<Object>("retData").has<String>("Slot") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <Slot>";
    ICERINI += o.get<Object>("retData").get<String>("Slot");
    ICERINI += "</Slot>\n";
    
    if( o.get<Object>("retData").has<String>("BaudRate") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <BaudRate>";
    ICERINI += o.get<Object>("retData").get<String>("BaudRate");
    ICERINI += "</BaudRate>\n";
    
    if( o.get<Object>("retData").has<String>("OpenCom") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <OpenCom>";
    ICERINI += o.get<Object>("retData").get<String>("OpenCom");
    ICERINI += "</OpenCom>\n";
    
    if( o.get<Object>("retData").has<String>("MustSettleDate") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <MustSettleDate>";
    ICERINI += o.get<Object>("retData").get<String>("MustSettleDate");
    ICERINI += "</MustSettleDate>\n";
    
    if( o.get<Object>("retData").has<String>("ReaderMode") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ReaderMode>";
    ICERINI += o.get<Object>("retData").get<String>("ReaderMode");
    ICERINI += "</ReaderMode>\n";
    
    if( o.get<Object>("retData").has<String>("BatchFlag") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <BatchFlag>";
    ICERINI += o.get<Object>("retData").get<String>("BatchFlag");
    ICERINI += "</BatchFlag>\n";
    
    if( o.get<Object>("retData").has<String>("OnlineFlag") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <OnlineFlag>";
    ICERINI += o.get<Object>("retData").get<String>("OnlineFlag");
    ICERINI += "</OnlineFlag>\n";
    
    if( o.get<Object>("retData").has<String>("ICERDataFlag") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ICERDataFlag>";
    ICERINI += o.get<Object>("retData").get<String>("ICERDataFlag");
    ICERINI += "</ICERDataFlag>\n";
    
    if( o.get<Object>("retData").has<String>("MessageHeader") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <MessageHeader>";
    ICERINI += o.get<Object>("retData").get<String>("MessageHeader");
    ICERINI += "</MessageHeader>\n";
    
    if( o.get<Object>("retData").has<String>("DLLMode") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <DLLMode>";
    ICERINI += o.get<Object>("retData").get<String>("DLLMode");
    ICERINI += "</DLLMode>\n";
    
    if( o.get<Object>("retData").has<String>("AutoLoadMode") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <AutoLoadMode>";
    ICERINI += o.get<Object>("retData").get<String>("AutoLoadMode");
    ICERINI += "</AutoLoadMode>\n";
    
    if( o.get<Object>("retData").has<String>("MaxALAmt") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <MaxALAmt>";
    ICERINI += o.get<Object>("retData").get<String>("MaxALAmt");
    ICERINI += "</MaxALAmt>\n";
    
    if( o.get<Object>("retData").has<String>("Dev_Info") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <Dev_Info>";
    ICERINI += o.get<Object>("retData").get<String>("Dev_Info");
    ICERINI += "</Dev_Info>\n";
    
    if( o.get<Object>("retData").has<String>("TCPIP_SSL") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <TCPIP_SSL>";
    ICERINI += o.get<Object>("retData").get<String>("TCPIP_SSL");
    ICERINI += "</TCPIP_SSL>\n";
    
    if( o.get<Object>("retData").has<String>("CMASAdviceVerify") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <CMASAdviceVerify>";
    ICERINI += o.get<Object>("retData").get<String>("CMASAdviceVerify");
    ICERINI += "</CMASAdviceVerify>\n";
    
    if( o.get<Object>("retData").has<String>("AutoSignOnPercnet") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <AutoSignOnPercnet>";
    ICERINI += o.get<Object>("retData").get<String>("AutoSignOnPercnet");
    ICERINI += "</AutoSignOnPercnet>\n";
    
    if( o.get<Object>("retData").has<String>("AutoLoadFunction") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <AutoLoadFunction>";
    ICERINI += o.get<Object>("retData").get<String>("AutoLoadFunction");
    ICERINI += "</AutoLoadFunction>\n";
    
    if( o.get<Object>("retData").has<String>("VerificationCode") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <VerificationCode>";
    ICERINI += o.get<Object>("retData").get<String>("VerificationCode");
    ICERINI += "</VerificationCode>\n";
    
    if( o.get<Object>("retData").has<String>("ReSendReaderAVR") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ReSendReaderAVR>";
    ICERINI += o.get<Object>("retData").get<String>("ReSendReaderAVR");
    ICERINI += "</ReSendReaderAVR>\n";
    
    if( o.get<Object>("retData").has<String>("XMLHeaderFlag") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <XMLHeaderFlag>";
    ICERINI += o.get<Object>("retData").get<String>("XMLHeaderFlag");
    ICERINI += "</XMLHeaderFlag>\n";
    
    if( o.get<Object>("retData").has<String>("FolderCreatFlag") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <FolderCreatFlag>";
    ICERINI += o.get<Object>("retData").get<String>("FolderCreatFlag");
    ICERINI += "</FolderCreatFlag>\n";
    
    if( o.get<Object>("retData").has<String>("BLCName") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <BLCName>";
    ICERINI += i_BLCName;
    ICERINI += "</BLCName>\n";
    
    if( o.get<Object>("retData").has<String>("CMASMode") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <CMASMode>";
    ICERINI += o.get<Object>("retData").get<String>("CMASMode");
    ICERINI += "</CMASMode>\n";
    
    if( o.get<Object>("retData").has<String>("POS_ID") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <POS_ID>";
    ICERINI += o.get<Object>("retData").get<String>("POS_ID");
    ICERINI += "</POS_ID>\n";
    
    if( o.get<Object>("retData").has<String>("AdditionalTcpipData") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <AdditionalTcpipData>";
    ICERINI += o.get<Object>("retData").get<String>("AdditionalTcpipData");
    ICERINI += "</AdditionalTcpipData>\n";
    
    if( o.get<Object>("retData").has<String>("PacketLenFlag") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <PacketLenFlag>";
    ICERINI += o.get<Object>("retData").get<String>("PacketLenFlag");
    ICERINI += "</PacketLenFlag>\n";
    
    if( o.get<Object>("retData").has<String>("CRT_FileName") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <CRT_FileName>";
    ICERINI += o.get<Object>("retData").get<String>("CRT_FileName");
    ICERINI += "</CRT_FileName>\n";
    
    if( o.get<Object>("retData").has<String>("Key_FileName") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <Key_FileName>";
    ICERINI += o.get<Object>("retData").get<String>("Key_FileName");
    ICERINI += "</Key_FileName>\n";
    
    if( o.get<Object>("retData").has<String>("ICERFlowDebug") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ICERFlowDebug>";
    ICERINI += o.get<Object>("retData").get<String>("ICERFlowDebug");
    ICERINI += "</ICERFlowDebug>\n";
    
    if( o.get<Object>("retData").has<String>("ReaderUartDebug") == 0)
    {
        return DF_UnSucc ;
    }
    ICERINI += "  <ReaderUartDebug>";
    ICERINI += o.get<Object>("retData").get<String>("ReaderUartDebug");
    ICERINI += "</ReaderUartDebug>\n";
    
    ICERINI += "</TransXML>\n";
    
    FILE* fp = fopen ( "./ecc_cmas/ICERINI.xml" , "w" ) ;
	if ( NULL == fp ) {
		Str Data;
		Data += "[API] [";
		Data += "ECCSetting";
		Data += "] [ecc_cmas/ICERINI.xml error]";
		writeLog(Data);
		return DF_UnSucc ;
	}
    
    fprintf(fp, "%s", ICERINI.c_str ( ));
    fclose ( fp ) ;
    
	return D_succ ;
}

int CCPS_API::Get_ConfigSetting ( void ) {

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\"}" ;
    
    Str teststr = setWriteHttpsData("/api/getConfigSetting", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }
    
    istringstream input(teststr);
    Object o;
    o.parse(input);
    
    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;
    if( o.has<Object>("retData") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retData= " << o.get<Object>("retData") << endl;
    if( o.get<Object>("retData").has<String>("upgradeTime") == 0)
    {
        return DF_UnSucc ;
    }
    i_upgradeTime = o.get<Object>("retData").get<String>("upgradeTime");
//cout << "i_upgradeTime= " << i_upgradeTime << endl;
    if( o.get<Object>("retData").has<String>("deviceErrorLogUrl") == 0)
    {
        return DF_UnSucc ;
    }
    i_deviceErrorLogUrl = o.get<Object>("retData").get<String>("deviceErrorLogUrl");
//cout << "deviceErrorLogUrl= " << i_deviceErrorLogUrl << endl;
    
	return D_succ ;
}

int CCPS_API::Set_uploadTransactionRecord ( Str cardNo, Str payment, Str readerDeviceNo, Str batchNo, Str RRN, Str beforeBalance, Str autoLoadAmount, Str afterBalance ) {

	time_t t = time(NULL);
	struct tm *local = localtime(&t);
	char tmp_transactionTime[14+1] = {0};
	Str transactionTime;
	sprintf(&tmp_transactionTime[0], "%04d%02d%02d%02d%02d%02d", (local->tm_year + 1900), (local->tm_mon + 1), local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	transactionTime = tmp_transactionTime;
	i_transactionTime = transactionTime;

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\",\"cardNo\":\"" ;
	bady     += cardNo ;
    bady     += "\",\"chargeNo\":\"" ;
	bady     += i_chargeNo ;
    bady     += "\",\"chargeMode\":\"" ;
	bady     += i_chargeMode ;
    bady     += "\",\"chargeAmount\":\"" ;
	bady     += i_chargeAmount ;
    bady     += "\",\"chargeTimes\":\"" ;
	bady     += i_chargeTimes ;
    bady     += "\",\"payment\":\"" ;
	bady     += payment ;
    bady     += "\",\"transactionTime\":\"" ;
	bady     += transactionTime ;
    bady     += "\",\"readerDeviceNo\":\"" ;
	bady     += readerDeviceNo ;
    bady     += "\",\"batchNo\":\"" ;
	bady     += batchNo ;
    bady     += "\",\"RRN\":\"" ;
	bady     += RRN ;
    bady     += "\",\"beforeBalance\":\"" ;
	bady     += beforeBalance ;
    bady     += "\",\"autoLoadAmount\":\"" ;
	bady     += autoLoadAmount ;
    bady     += "\",\"afterBalance\":\"" ;
	bady     += afterBalance ;
    bady     += "\"}" ;

    Str teststr = setWriteHttpsData("/api/uploadTransactionRecord", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }

    istringstream input(teststr);
    Object o;
    o.parse(input);

    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;

	return D_succ ;
}

int CCPS_API::Set_uploadCheckoutRecord ( Str cardNo, Str checkoutBatchNo, Str accountingStatus, Str readerDeviceNo, Str numbersPurchase, Str numbersRefund, Str numbersAutoLoad, Str numbersTotal, Str amountPurchase, Str amountRefund, Str amountAutoLoad, Str amountTotal, Str transactionNumbers, Str transactionAmount, Str transactionNetAmount ) {

	time_t t = time(NULL);
	struct tm *local = localtime(&t);
	char tmp_transactionTime[14+1] = {0};
	Str transactionTime;
	sprintf(&tmp_transactionTime[0], "%04d%02d%02d%02d%02d%02d", (local->tm_year + 1900), (local->tm_mon + 1), local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	transactionTime = tmp_transactionTime;

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\",\"cardNo\":\"" ;
	bady     += cardNo ;
    bady     += "\",\"chargeAmount\":\"" ;
	bady     += i_chargeAmount ;
    bady     += "\",\"transactionTime\":\"" ;
	bady     += i_transactionTime ;
    bady     += "\",\"checkoutTime\":\"" ;
	bady     += transactionTime ;
    bady     += "\",\"checkoutBatchNo\":\"" ;
	bady     += checkoutBatchNo ;
    bady     += "\",\"accountingStatus\":\"" ;
	bady     += accountingStatus ;
    bady     += "\",\"readerDeviceNo\":\"" ;
	bady     += readerDeviceNo ;
    bady     += "\",\"numbersPurchase\":\"" ;
	bady     += numbersPurchase ;
    bady     += "\",\"numbersRefund\":\"" ;
	bady     += numbersRefund ;
    bady     += "\",\"numbersAutoLoad\":\"" ;
	bady     += numbersAutoLoad ;
    bady     += "\",\"numbersTotal\":\"" ;
	bady     += numbersTotal ;
    bady     += "\",\"amountPurchase\":\"" ;
	bady     += amountPurchase ;
    bady     += "\",\"amountRefund\":\"" ;
	bady     += amountRefund ;
    bady     += "\",\"amountAutoLoad\":\"" ;
	bady     += amountAutoLoad ;
    bady     += "\",\"amountTotal\":\"" ;
	bady     += amountTotal ;
    bady     += "\",\"transactionNumbers\":\"" ;
	bady     += transactionNumbers ;
    bady     += "\",\"transactionAmount\":\"" ;
	bady     += transactionAmount ;
    bady     += "\",\"transactionNetAmount\":\"" ;
	bady     += transactionNetAmount ;
    bady     += "\"}" ;

    Str teststr = setWriteHttpsData("/api/uploadCheckoutRecord", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }

    istringstream input(teststr);
    Object o;
    o.parse(input);

    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;

	return D_succ ;
}

int CCPS_API::Set_uploadSystemLog ( Str cardNo, Str recordCode, Str remark ) {

	time_t t = time(NULL);
	struct tm *local = localtime(&t);
	char tmp_transactionTime[14+1] = {0};
	Str recordTime;
	sprintf(&tmp_transactionTime[0], "%04d%02d%02d%02d%02d%02d", (local->tm_year + 1900), (local->tm_mon + 1), local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	recordTime = tmp_transactionTime;

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\",\"cardNo\":\"" ;
	bady     += cardNo ;
    bady     += "\",\"recordCode\":\"" ;
	bady     += recordCode ;
    bady     += "\",\"recordTime\":\"" ;
	bady     += recordTime ;
    bady     += "\",\"remark\":\"" ;
	bady     += remark ;
    bady     += "\"}" ;

    Str teststr = setWriteHttpsData("/api/uploadSystemLog", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }

    istringstream input(teststr);
    Object o;
    o.parse(input);

    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;

	return D_succ ;
}

int CCPS_API::Set_uploadMonitorData ( void ) {

	Str homeMemorySpace;
	Str DF_H_Path = "df /tmp/mmcblk0p1 -h | grep /tmp";
	//Str DF_H_Path = "df -h | grep /dev/sda1";

	char buffer[200+1] = {0};
	FILE *fp = popen(DF_H_Path.c_str ( ), "r");
	if(fp == NULL)
	{
		return DF_UnSucc ;
	}
	fread(buffer, 1, sizeof(buffer)-1, fp);
	pclose(fp);
	buffer[strlen(buffer)-1] = 0;
	int i, cnt = 0, flag_cnt = 1;
	for(i=0; i<(int)strlen(buffer); i++)
	{
		if(buffer[i] == ' ')
		{
			flag_cnt = 1;
			continue;
		}
		else if(cnt == 4)
		{
			homeMemorySpace = &buffer[i-1];
			break;
		}
		else if(flag_cnt == 1)
		{
			flag_cnt = 0;
			cnt++;
		}
	}
	Str::size_type pz = homeMemorySpace.find(" ");
	if ( pz == homeMemorySpace.npos ) {
		Str Data;
		Data += "[API] [";
		Data += "uploadMonitorData";
		Data += "] [homeMemorySpace error]";
		writeLog(Data);
		return DF_UnSucc ;
	}

	if( (homeMemorySpace[pz-1] == 'K') || ((homeMemorySpace[pz-1] == 'M') && (atof(&homeMemorySpace[0]) < 2.0)) )
	{
		time_t t = time(NULL);
		struct tm *local = localtime(&t);
		char runCommand[60] = {0};

		sprintf(runCommand, "cd ./syslogs && ls |grep -v %04d%02d%02d|xargs rm -f", (local->tm_year + 1900), (local->tm_mon + 1), local->tm_mday );
		system(runCommand);

		Str Data;
		Data += "[API] [";
		Data += "uploadMonitorData";
		Data += "] [Capacity error]";
		writeLog(Data);
	}

	homeMemorySpace = homeMemorySpace.substr(0, pz-1);

	Str bady =  "{\"deviceNo\":\"" ;
	bady     += i_deviceNo ;
	bady     += "\",\"deviceIp\":\"" ;
	bady     += i_LocalIP ;
    bady     += "\",\"token\":\"" ;
	bady     += i_token ;
    bady     += "\",\"homeMemorySpace\":\"" ;
	bady     += homeMemorySpace ;
    bady     += "\",\"deviceVersion\":\"" ;
	bady     += i_deviceVersion ;
    bady     += "\"}" ;

    Str teststr = setWriteHttpsData("/api/uploadMonitorData", bady, "application/json");
    if(teststr == "")
    {
    	return DF_UnSucc ;
    }

    istringstream input(teststr);
    Object o;
    o.parse(input);

    if( o.has<Number>("retCode") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retCode= " << o.get<Number>("retCode") << endl;
    if( o.get<Number>("retCode") != 1 )
    {
        return DF_UnSucc ;
    }
    if( o.has<String>("retMsg") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retMsg= " << o.get<String>("retMsg") << endl;
    if( o.has<Number>("retVal") == 0)
    {
        return DF_UnSucc ;
    }
//cout << "retVal= " << o.get<Number>("retVal") << endl;

	return D_succ ;
}

int CCPS_API::Get_Proxy_blc_download ( void )
{
	//i_eccBlcName = "BIG.TGZ";
	//i_eccBlcName = "KBLN.ZIP";

	Str bady =  "data=[{\"type\":\"" ;
	bady     += i_eccBlcName ;
	bady     += "\"}]" ;

	Str TmpIP = i_CPSserver;
	i_CPSserver = i_ProxyIp;
	//i_CPSserver = "59.120.234.79";
	//i_CPSserver = "apis_test.lifeplus.tw";
	Str TmpPort = i_port;
	i_port = i_ProxyPort;
	srand(time(NULL));
	sleep( ((rand()%60)+1) );

	Str teststr = setWriteHttpsData("/api/sys2Construction/blc_download", bady, "application/x-www-form-urlencoded");
	i_CPSserver = TmpIP;
	i_port = TmpPort;
	if(teststr == "")
	{
		return DF_UnSucc ;
	}

	istringstream input(teststr);
	Object o;
	o.parse(input);

	if( o.has<Number>("retCode") == 0)
	{
		return DF_UnSucc ;
	}
//cout << "retCode= " << o.get<Number>("retCode") << endl;
	if( o.get<Number>("retCode") != 1 )
	{
		return DF_UnSucc ;
	}
	if( o.has<Object>("retVal") == 0)
	{
		return DF_UnSucc ;
	}
//cout << "retVal= " << o.get<Object>("retVal") << endl;
	if( o.get<Object>("retVal").has<String>("sig") == 0)
	{
		return DF_UnSucc ;
	}
	Str Sig = o.get<Object>("retVal").get<String>("sig");
//cout << "sig= " << Sig << endl;

	if( o.get<Object>("retVal").has<String>("data") == 0)
	{
		return DF_UnSucc ;
	}
	Str Blc_Data = o.get<Object>("retVal").get<String>("data");
//cout << "data= " << Blc_Data << endl;

	unsigned char digest[SHA_DIGEST_LENGTH];
	SHA1((unsigned char*)&Blc_Data[0], Blc_Data.size(), (unsigned char*)&digest);
	char mdString[SHA_DIGEST_LENGTH*2+1];
	for(int i = 0; i < SHA_DIGEST_LENGTH; i++)
		sprintf(&mdString[i*2], "%02x", (unsigned int)digest[i]);
//cout << "SHA1 mdString: " << mdString << endl;
	if(memcmp(&Sig[0], mdString, SHA_DIGEST_LENGTH*2) != 0)
	{
		Str Data;
		Data += "[API] [";
		Data += "blc_download";
		Data += "] [SHA1 error]";
		writeLog(Data);
		return DF_UnSucc ;
	}

	Str Blc_Path = "./ecc_cmas/ICERData/BlcFile/";
	Str Blc_Path_File = Blc_Path + i_eccBlcName;

	Str rm_Blc_Path = "rm -rf " + Blc_Path + "*";
	system(rm_Blc_Path.c_str ( ));

	if ( D_succ != i_cpto->DecodeB64 ( Blc_Data ,  Blc_Path_File, false ) ) {
		Str Data;
		Data += "[API] [";
		Data += "blc_download";
		Data += "] [base64 error]";
		writeLog(Data);
		return DF_UnSucc ;
	}

	Str tar_Blc_Path = "tar -zxv -f " + Blc_Path_File + " -C " + Blc_Path;
	//system(tar_Blc_Path.c_str ( ));
	char buffer[50+1] = {0};
	FILE *fp = popen(tar_Blc_Path.c_str ( ), "r");
	if(fp == NULL)
	{
		return DF_UnSucc ;
	}
    fread(buffer, 1, sizeof(buffer)-1, fp);
    pclose(fp);
    buffer[strlen(buffer)-1] = 0;
    i_BLCName = buffer;

	return D_succ ;
}
