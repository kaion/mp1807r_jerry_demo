#include "Http.h"

int CRdWrTmO::SetsockoptMethod ( int iFd , UInt wrMs , UInt rdMs ) {
	bool prmtBool = ( 0 >= iFd ) ;
	prmtBool |= ( 0 >= wrMs ) ;
	prmtBool |= ( 0 >= rdMs ) ;
	if ( true == prmtBool ) {
		return DF_Parameter ;
	}
	timeval setTimeStu = { 0 } ;

	setTimeStu.tv_sec = wrMs / 1000 ;
	setTimeStu.tv_usec = ( wrMs - ( setTimeStu.tv_sec * 1000 ) ) * 1000 ;
	if ( 0 != setsockopt ( iFd , SOL_SOCKET , SO_SNDTIMEO , & setTimeStu , sizeof(timeval) ) ) {
		return DF_CRdWrTmO_SetsockoptMethod_Setsockopt_Write ;
	}

	setTimeStu.tv_sec = rdMs / 1000 ;
	setTimeStu.tv_usec = ( rdMs - ( setTimeStu.tv_sec * 1000 ) ) * 1000 ;
	if ( 0 != setsockopt ( iFd , SOL_SOCKET , SO_RCVTIMEO , & setTimeStu , sizeof(timeval) ) ) {
		return DF_CRdWrTmO_SetsockoptMethod_Setsockopt_Read ;
	}
	return DF_Succ ;
}

CClient::CClient ( ) {
	iMem = new CMem [ 1 ] ;
	iReadBin = iMem->New < CStrType > ( 1 ) ;
	i_Ssl_read_bin = iMem->New < CStrType > ( 1 ) ;
	iSocketFd = - 1 ;
	iCtx = NULL ;
	iSsl = NULL ;

}
CClient::~CClient ( ) {
	CloseSsl ( ) ;

	iMem->Delete < CStrType > ( iReadBin ) ;
	iMem->Delete < CStrType > ( i_Ssl_read_bin ) ;

	if ( NULL != iMem ) {
		delete [ ] iMem ;
	}
}

int CClient::RuternSocketFd ( void ) {
	return iSocketFd ;
}

int CClient::WriteBin ( CStrType*& buf ) {
	int rtn = write ( iSocketFd , buf->RtnStr ( ) , buf->Size ( ) ) ;
	if ( 0 >= rtn ) {
		return DF_CClient_WriteBin_Write ;
	}
	return DF_Succ ;
}

int CClient::Connect ( CStr ip , CStr port ) {
	addrinfo req = { 0 } ;   //設定用
	addrinfo* pai = NULL ;   //取得傳回資訊用
	req.ai_family = AF_UNSPEC ;
	req.ai_socktype = SOCK_STREAM ;
	if ( 0 != getaddrinfo ( ip.c_str ( ) , port.c_str ( ) , & req , & pai ) ) {
		return DF_CClient_Connect_Getaddrinfo ;
	}

	iSocketFd = socket ( pai->ai_family , pai->ai_socktype , pai->ai_protocol ) ;
	if ( 0 > iSocketFd ) {
		iSocketFd = - 1 ;
		freeaddrinfo ( pai ) ;
		return DF_CClient_Connect_Socket ;
	}

	if ( 0 > connect ( iSocketFd , pai->ai_addr , pai->ai_addrlen ) ) {
		CloseSocketFd ( ) ;
		freeaddrinfo ( pai ) ;
		return DF_CClient_Connect_Connect ;
	}

	freeaddrinfo ( pai ) ;
	return DF_Succ ;
}

int CClient::WriteStr ( CStr& str ) {
	int rtn = write ( iSocketFd , str.c_str ( ) , str.size ( ) ) ;
	if ( - 1 == rtn ) {
		return DF_CClient_WriteStr_Write ;
	}
	return DF_Succ ;
}

int CClient::ReadStr ( bool keepAlived ) {

	iReadStr.clear ( ) ;

	while ( 1 ) {
		errno = 0 ;
		char buf [ 8191 + 1 ] = { 0 } ;
		int rtn = read ( iSocketFd , buf , sizeof ( buf ) - 1 ) ;

		if ( - 1 == rtn ) {
			if ( EAGAIN == errno ) {
				continue ;
			}
			return DF_CClient_ReadStr_Read ;

		} else if ( 0 == rtn ) {
			break ;
		}
		iReadStr += buf ;

		if ( true == keepAlived ) {
			if ( rtn < ( ( int ) sizeof ( buf ) - 1 ) ) { // 基本上應該是沒有資料了 , 在這種沒頭尾的資料這樣就當作沒資料了
				break ;
			}
		}
	}
	return DF_Succ ;
}
Str CClient::RtnReadStr ( void ) {
	Str str = iReadStr ;
	iReadStr.clear ( ) ;
	return str ;
}

int CClient::ReadBin ( bool keepAlived , bool dataContinue ) {

	if ( false == dataContinue ) {
		iReadBin->Clear ( ) ;
	}

	while ( 1 ) {
		errno = 0 ;
		UChar buf [ 8191 + 1 ] = { 0 } ;
		int rtn = read ( iSocketFd , buf , sizeof ( buf ) - 1 ) ;

		if ( - 1 == rtn ) {
			if ( EAGAIN == errno ) {
				continue ;
			}
			return DF_CClient_ReadBin_Read ;
		} else if ( 0 == rtn ) {
			break ;
		}
		iReadBin->Assign ( iReadBin->Size ( ) , rtn , ( void* ) buf ) ;
		if ( true == keepAlived ) {
			if ( rtn < ( ( int ) sizeof ( buf ) - 1 ) ) { // 基本上應該是沒有資料了 , 在這種沒頭尾的資料這樣就當作沒資料了
				break ;
			}
		}
	}
	return DF_Succ ;
}
CStrType* CClient::RtnReadBin ( void ) {
	return iReadBin ;
}

int CClient::ReadToFile ( bool keepAlived , CStr filePath ) {

	FILE* fp = fopen ( filePath.c_str ( ) , "wb+" ) ;
	if ( NULL == fp ) {
		return DF_CClient_ReadToFile_Fopen ;
	}
	int rtn = 0 ;
	while ( 1 ) {
		errno = 0 ;
		UChar buf [ 8191 + 1 ] = { 0 } ;
		rtn = read ( iSocketFd , buf , sizeof ( buf ) - 1 ) ;
		if ( - 1 == rtn ) {
			if ( EAGAIN == errno ) {
				continue ;
			}
			return DF_CClient_ReadToFile_Read ;

		} else if ( 0 == rtn ) {
			fflush ( fp ) ;
			rtn = ftell ( fp ) ;
			fclose ( fp ) ;
			return rtn ;
		}

		fwrite ( buf , sizeof(UChar) , rtn , fp ) ;

		if ( true == keepAlived ) {
			if ( rtn < ( ( int ) sizeof ( buf ) - 1 ) ) { // 基本上應該是沒有資料了 , 在這種沒頭尾的資料這樣就當作沒資料了
				break ;
			}
		}
	}
	fflush ( fp ) ;
	rtn = ftell ( fp ) ;
	fclose ( fp ) ;
	return rtn ;
}

void CClient::CloseSocketFd ( void ) {
	if ( 0 < iSocketFd ) {
		close ( iSocketFd ) ;
		iSocketFd = - 1 ;
	}
	return ;
}

void CClient::CloseSsl ( void ) {
	if ( NULL != iCtx ) {
		SSL_CTX_free ( iCtx ) ;
		iCtx = NULL ;
	}

	if ( NULL != iSsl ) {
		SSL_shutdown ( iSsl ) ;
		SSL_free ( iSsl ) ;
		iSsl = NULL ;
	}

	CloseSocketFd ( ) ;
}

int CClient::SslConnect ( void ) {
	/* ssl */
	SSL_library_init ( ) ;   //對OpenSSL進行初始化
	SSL_load_error_strings ( ) ;   //對錯誤訊息進行初始化

	// 申請SSL會話的環境CTX(建立本次會話連接所使用的協議()) //
	iCtx = SSL_CTX_new ( SSLv23_client_method ( ) ) ;
	if ( NULL == iCtx ) {
		CloseSocketFd ( ) ;
		return DF_CClient_SslConnect_Ssl_Ctx_New ;
	}

	// 申請一個SSL 套節字 //
	iSsl = SSL_new ( iCtx ) ;
	if ( NULL == iSsl ) {
		CloseSsl ( ) ;
		return DF_CClient_SslConnect_Ssl_New ;
	}

	// 綁定讀寫Socket //
	if ( 0 == SSL_set_fd ( iSsl , iSocketFd ) ) {
		CloseSsl ( ) ;
		return DF_CClient_SslConnect_Ssl_Set_fd ;
	}

	// SSL 握手動作 //
	if ( 0 >= SSL_connect ( iSsl ) ) {
		CloseSsl ( ) ;
		return DF_CClient_SslConnect_Ssl_Connect ;
	}
	/* END ssl */
	return DF_Succ ;
}

Str CClient::Rtn_IP_str ( void ) {
#define NAME_NU 4
	int i = 0;
	struct ifreq ifr = {0};
	Str str, Name[NAME_NU+1] = {"br-lan", "eth0", "eth0.1", "enp0s3"};
	for(i=0; i<NAME_NU; i++)
	{
		memcpy(ifr.ifr_name, &Name[i][0], Name[i].length());
		if( ioctl(iSocketFd, SIOCGIFADDR, &ifr) < 0 )
		{
			str = "127.0.0.1";
		}
		else
		{
			str = inet_ntoa( ((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr);
			break;
		}
	}
	return str ;
}

int CClient::Ssl_write_bin ( CStrType*& buf ) {
	UChar* ptr = buf->RtnStr ( ) ;
	while ( 1 ) {
		int rtnCode = SSL_write ( iSsl , ptr , buf->Size ( ) ) ;
		int errCode = SSL_get_error ( iSsl , rtnCode ) ;
		if ( SSL_ERROR_NONE == errCode ) {
			break ;

		} else if ( SSL_ERROR_WANT_WRITE == errCode ) {
			continue ;

		} else {
			return DF_CClient_SslWrite_Ssl_Write ;
		}
	}
	return DF_Succ ;
}

int CClient::Ssl_write_str ( CStr& str ) {
	while ( 1 ) {
		int rtnCode = SSL_write ( iSsl , str.c_str ( ) , str.size ( ) ) ;
		int errCode = SSL_get_error ( iSsl , rtnCode ) ;
		if ( SSL_ERROR_NONE == errCode ) {
			break ;

		} else if ( SSL_ERROR_WANT_WRITE == errCode ) {
			continue ;

		} else {
			return DF_CClient_SslWriteStr_Ssl_Write ;
		}
	}
	return DF_Succ ;
}

int CClient::Ssl_read_str ( void ) {
	i_Ssl_read_str.clear ( ) ;
	char buf [ 8191 + 1 ] = { 0 } ;
	while ( 1 ) {
		int rtnCode = SSL_read ( iSsl , buf , sizeof ( buf ) - 1 ) ;
		int errCode = SSL_get_error ( iSsl , rtnCode ) ;
		if ( SSL_ERROR_NONE == errCode ) {
			buf [ rtnCode ] = 0 ;
			i_Ssl_read_str += buf ;
			continue ;
		} else if ( SSL_ERROR_WANT_READ == errCode ) {
			continue ;
		} else {
			if ( 0 < i_Ssl_read_str.size ( ) ) {
				break ;
			}
			return DF_CClient_SslRead_Ssl_Read ;
		}
	}
	return DF_Succ ;
}
Str CClient::Rtn_Ssl_read_str ( void ) {
	Str str = i_Ssl_read_str ;
	i_Ssl_read_str.clear ( ) ;
	return str ;
}

int CClient::Ssl_read_bin ( bool data_continue ) {

	if ( false == data_continue ) {
		i_Ssl_read_bin->Clear ( ) ;
	}
	UChar buf [ 8191 + 1 ] = { 0 } ;
	while ( 1 ) {
		int rtnCode = SSL_read ( iSsl , buf , sizeof ( buf ) - 1 ) ;
		int errCode = SSL_get_error ( iSsl , rtnCode ) ;
		if ( SSL_ERROR_NONE == errCode ) {
			i_Ssl_read_bin->Cat ( buf , rtnCode ) ;
			continue ;

		} else if ( SSL_ERROR_WANT_READ == errCode ) {
			continue ;

		} else {
			if ( 0 < i_Ssl_read_bin->Size ( ) ) {
				break ;
			}
			return DF_CClient_SslRead_Ssl_Read ;
		}
	}
	return DF_Succ ;
}
CStrType* CClient::Rtn_Ssl_read_bin ( void ) {
	return i_Ssl_read_bin ;
}

char CClient::FromHex ( char ch ) {
	return isdigit ( ch ) ? ( ch - '0' ) : ( tolower ( ch ) - 'a' + 10 ) ;
}

char CClient::ToHex ( char code ) {
	static char hex [ ] = "0123456789abcdef" ;
	return hex [ code & 15 ] ;
}

int CClient::UrlDecode ( CStr& input ) {
	iUrlDecode.clear ( ) ;

	int inputI = 0 ;

	while ( inputI < ( int ) input.size ( ) ) {

		if ( input [ inputI ] == '%' ) {

			if ( input [ inputI + 1 ] && input [ inputI + 2 ] ) {
				iUrlDecode += FromHex ( input [ inputI + 1 ] ) << 4 | FromHex ( input [ inputI + 2 ] ) ;
				inputI += 2 ;
			}

		} else if ( input [ inputI ] == '+' ) {
			iUrlDecode += ' ' ;

		} else {
			iUrlDecode += input [ inputI ] ;
		}

		++ inputI ;
	}
	return DF_Succ ;
}
Str CClient::RtnUrlDecode ( void ) {
	Str str = iUrlDecode ;
	iUrlDecode.clear ( ) ;
	return str ;
}

int CClient::UrlEncode ( CStr& input ) {
	iUrlEncode.clear ( ) ;

	int inputI = 0 ;
	while ( inputI < ( int ) input.size ( ) ) {

		if ( isalnum ( input [ inputI ] ) || ( input [ inputI ] == '-' ) || ( input [ inputI ] == '_' ) || ( input [ inputI ] == '.' ) ) {
			iUrlEncode += input [ inputI ] ;

		} else if ( input [ inputI ] == ' ' ) {
			iUrlEncode += '+' ;

		} else {
			iUrlEncode += '%' ;
			iUrlEncode += ToHex ( input [ inputI ] >> 4 ) ;
			iUrlEncode += ToHex ( input [ inputI ] & 15 ) ;
		}
		++ inputI ;
	}

	return DF_Succ ;
}
Str CClient::RtnUrlEncode ( void ) {
	Str str = iUrlEncode ;
	iUrlEncode.clear ( ) ;
	return str ;
}
