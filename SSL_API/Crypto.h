#ifndef CRYPTO_H_
#define CRYPTO_H_

#include "SetupFile.h"
#include "MyMem.h"
#include "MyString.h"
class CCrypto {

	private:
		CMyStrPar* iMyStrPar ;
		CMem* iMem ;
	public:
		CCrypto ( ) ;
		~CCrypto ( ) ;

	private:
		Str iSha1ForFileBin ;
	public:
		int Sha1ForFileBin ( CStr filePath ) ;
		Str RtnSha1ForFileBin ( void ) ;

		/*
		 * CStr filePath : 要壓縮的檔案絕對路徑
		 * bool forUrl   : 若為 true 則 編碼出來的資料 ('+' 轉換成 '_') ('=' 轉換成 '-')
		 */
	private:
		Str iEncodeB64Bin ;
	public:
		int EncodeB64Bin ( CStr surFilePath , bool forUrl ) ;
		Str RtnEncodeB64Bin ( void ) ;

		/*
		 * CChar* source     : 要解壓縮的 base 64 資料
		 * CChar* targetPath : 解壓縮後要寫入的檔案絕對路徑
		 * bool forUrl          : 若為 true 則 編碼出來的資料 ('+' 轉換成 '_') ('=' 轉換成 '-')
		 */
		int DecodeB64 ( Str surData , CStr targetPath , bool forUrl ) ;

		/*
		 *  CChar* souceFilePath : 要壓縮的檔案路徑
		 *  char* output         : 輸出資料存放位置 ,大小不可以小於 32 + 1
		 */
	private:
		Str iMd5FromFileBin ;
	public:
		int Md5FromFileBin ( CStr surFilePath ) ;
		Str RtnMd5FromFileBin ( void ) ;

	private:
		Str iMd5Bin ;
	public:
		int Md5Bin ( CStrType* surData ) ;
		Str RtnMd5Bin ( void ) ;

	private:
		Str i_md5_str ;
	public:
		int Md5Str ( Str surData ) ;
		Str RtnMd5Str ( void ) ;

		Str Md5_str ( Str input ) ;


//	private:
//		Str iSortMakeMd5 ;
//	public:
//		int SortMakeMd5 ( CStr mac , CStr pillarNum , int mem , ... ) ;
//		Str RtnSortMakeMd5 ( void ) ;
//
//	private:
//		Str iQryJsonFormat ;
//		Str iQryJsonFormatMd5 ;
//	public:
//		int QryJsonFormat ( CStr mac , CStr pillarNum , int mem , ... ) ;
//		Str RtnQryJsonFormat ( void ) ;
//		Str RtnQryJsonFormatMd5 ( void ) ;
//
//
//	private:
//		Str iRspnJsonFormat ;
//		Str iRspnJsonFormatMd5 ;
//	public:
//		int RspnJsonFormat ( CStr mac , CStr pillarNum , int mem , ... ) ;
//		Str RtnRspnJsonFormat ( void ) ;
//		Str RtnRspnJsonFormatMd5 ( void ) ;    // 目前都是用 reqMd5 作區分資料 ,所以這 respon md5 先不用




} ;
#if 0
e.g.
// sha1 //
FILE* fp = fopen ( "./ttt" , "w" ) ;
if ( NULL == fp ) {
	exit ( 1 ) ;
}
fprintf ( fp , "!@#123" ) ;
fflush ( fp ) ;
fclose ( fp ) ;

CCrypto obj ;
int a = obj.Sha1ForFileBin ( "./ttt" ) ;
std::cout << "a:" << a << std::endl ;
if ( DF_SUCCESS != a ) {
	exit ( 1 ) ;
}
std::cout << obj.RtnSha1ForFileBin ( ) << std::endl;

// md5 to file //
FILE* fp = fopen ( "./ttt" , "w" ) ;
if ( NULL == fp ) {
	exit ( 1 ) ;
}
fprintf ( fp , "!@#123" ) ;
fflush ( fp ) ;
fclose ( fp ) ;

CCrypto obj ;
int a = obj.Md5FromFileBin ( "./ttt" ) ;
std::cout << "a:" << a << std::endl ;
if ( DF_SUCCESS != a ) {
	exit ( 1 ) ;
}
std::cout << obj.RtnMd5FromFileBin ( ) << std::endl ;
exit ( 1 ) ;


// md5 //
CStrType oo ;
oo.Assign ( 0 , 20 , ( void* ) "QWERTYUIOPASDFGHJKL:" ) ;

CCrypto obj ;
int a = obj.Md5Bin ( oo ) ;
std::cout << "a:" << a << std::endl ;
if ( DF_SUCCESS != a ) {
	exit ( 1 ) ;
}
std::cout << obj.RtnMd5Bin ( ) << std::endl ;
exit ( 1 ) ;


	// encode and decode b64 //
///media/j/15e63d73-fd45-4012-a263-0f4bc76b51fd/workspace_UBike/Command_2014_11_19/testB64.mp3

CCrypto obj ;
//	int a = obj.EncodeB64Bin ( "/media/j/15e63d73-fd45-4012-a263-0f4bc76b51fd/workspace_UBike/Command_2014_11_19/testB64.mp3" , false ) ;
int a = obj.EncodeB64Bin ( "/media/j/15e63d73-fd45-4012-a263-0f4bc76b51fd/workspace_UBike/Command_2014_11_19/testB64.mp3" , true ) ;
std::cout << "a:" << a << std::endl ;
if ( DF_SUCCESS != a ) {
	exit ( 1 ) ;
}
//	a = obj.DecodeB64 ( obj.RtnEncodeB64Bin ( ) , "/media/j/15e63d73-fd45-4012-a263-0f4bc76b51fd/workspace_UBike/Command_2014_11_19/testB64111.mp3" , false ) ;
obj.DecodeB64 ( obj.RtnEncodeB64Bin ( ) , "/media/j/15e63d73-fd45-4012-a263-0f4bc76b51fd/workspace_UBike/Command_2014_11_19/testB64111.mp3" , true ) ;
std::cout << "a:" << a << std::endl ;
exit ( 1 ) ;
#endif
#endif /* CRYPTO_H_ */
