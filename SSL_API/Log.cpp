#include "Log.h"

#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

char writeLog(Str Data)
{
	mkdir("./syslogs", 0755);

	time_t t = time(NULL);
	struct tm *local = localtime(&t);

	char Path_File_Log[30+1] = {0};
	sprintf(&Path_File_Log[0], "./syslogs/%04d%02d%02d", (local->tm_year + 1900), (local->tm_mon + 1), local->tm_mday);

	FILE *fp = fopen(Path_File_Log, "a+");
	if(fp == NULL)
	{
		return -1;
	}
	fprintf(fp, "[%04d-%02d-%02d %02d:%02d:%02d] ", (local->tm_year + 1900), (local->tm_mon + 1), local->tm_mday, local->tm_hour, local->tm_min, local->tm_sec);
	fprintf(fp, "%s\n", Data.c_str() );
	fflush(fp);
	fclose(fp);
	fp = NULL;

	return 0;
}
