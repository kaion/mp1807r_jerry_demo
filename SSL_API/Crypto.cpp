#include "Crypto.h"

CCrypto::CCrypto ( ) {
	iMem = new CMem [ 1 ] ;
	iMyStrPar = iMem->New < CMyStrPar > ( 1 ) ;
	iSha1ForFileBin.clear ( ) ;
	i_md5_str.clear ( ) ;
}
CCrypto::~CCrypto ( ) {
	iMem->Delete ( iMyStrPar ) ;

	if ( NULL != iMem ) {
		delete [ ] iMem ;
	}
}

int CCrypto::Sha1ForFileBin ( CStr filePath ) {
	iSha1ForFileBin.clear ( ) ;

	int rtn = iMyStrPar->ReadFileBin ( filePath ) ;
	if ( DF_Succ != rtn ) {
		return DF_CCrypto_Sha1ForFileBin_ReadFileBin ;
	}

	CStrType*& sourceBuf = iMyStrPar->RtnReadFileBin ( ) ;

	UChar shaBuf[ 20 ] = { 0 } ;
	SHA1 ( sourceBuf->RtnStr ( ) , sourceBuf->Size ( ) , shaBuf ) ;

	for ( int i = 0 ; i < ( int ) sizeof ( shaBuf ) ; ++i ) {
		char tmp [ 2 + 1 ] = { 0 };
		snprintf ( tmp , sizeof ( tmp ) , "%02x" , shaBuf [ i ] ) ;
		iSha1ForFileBin += tmp ;
	}

	return DF_Succ ;
}
Str CCrypto::RtnSha1ForFileBin ( void ) {
	Str str = iSha1ForFileBin ;
	iSha1ForFileBin.clear ( ) ;
	return str ;
}


int CCrypto::EncodeB64Bin ( CStr surFilePath , bool forUrl ) {
	iEncodeB64Bin.clear ( ) ;

	int rtn = iMyStrPar->ReadFileBin ( surFilePath ) ;
	if ( DF_Succ != rtn ) {
		return DF_CCrypto_EncodeB64Bin_ReadFileBin ;
	}

	CStrType*& sourceBuf = iMyStrPar->RtnReadFileBin ( ) ;

	BIO *bio = BIO_new ( BIO_f_base64 ( ) ) ;     //申請一個以 BIO_f_base64 為基準的 BIO *
	BIO *b64 = BIO_new ( BIO_s_mem ( ) ) ;            //直接把要 decode 的內容先寫到記憶體暫存區
	bio = BIO_push ( bio , b64 ) ;                //把兩者串鍊起來 b64 為 bio 的子節點
	BIO_set_flags ( bio , BIO_FLAGS_BASE64_NO_NL ) ;   //可把 encode 結合成一行
	BIO_write ( bio , sourceBuf->RtnStr ( ) , sourceBuf->Size ( ) ) ;
	BIO_ctrl ( bio , BIO_CTRL_FLUSH , 0 , NULL ) ;   //BIO_flush(bio);   //清空暫存區

	char* willEncodeB64Buf = iMem->New < char > ( bio->next_bio->num_write + 1 ) ;
	if(NULL ==willEncodeB64Buf){
		return DF_CCrypto_EncodeB64Bin_CMem_New ;
	}

	rtn = BIO_read ( bio->next_bio , willEncodeB64Buf , bio->next_bio->num_write ) ;
	BIO_free_all ( bio ) ;
	if ( ( int ) bio->next_bio->num_write != rtn ) {
		return DF_CCrypto_EncodeB64Bin_Bio_Read_Data_Not_Match ;
	}

	if ( true == forUrl ) {
		for ( int i = 0 ; i < rtn ; ++ i ) {
			if ( '+' == willEncodeB64Buf [ i ] ) {
				willEncodeB64Buf [ i ] = '_' ;
			} else if ( '=' == willEncodeB64Buf [ i ] ) {
				willEncodeB64Buf [ i ] = '-' ;
			}
		}
	}

	iEncodeB64Bin = willEncodeB64Buf ;
	delete [ ] willEncodeB64Buf ;

	return DF_Succ ;
}

Str CCrypto::RtnEncodeB64Bin ( void ) {
	Str str = iEncodeB64Bin ;
	iEncodeB64Bin.clear ( ) ;
	return str ;
}


int CCrypto::DecodeB64 ( Str surData , CStr targetPath , bool forUrl ) {

	Str::size_type surSz = surData.size ( ) ;
	if ( true == forUrl ) {
		for ( int i = 0 ; i < ( int ) surSz ; ++ i ) {
			if ( '_' == surData [ i ] ) {
				surData [ i ] = '+' ;
			} else if ( '-' == surData [ i ] ) {
				surData [ i ] = '=' ;
			}
		}
	}

	BIO *b64 = BIO_new ( BIO_f_base64 ( ) ) ;
	BIO *bmem = BIO_new_mem_buf ( ( void* ) surData.c_str ( ) , surSz ) ;
	bmem = BIO_push ( b64 , bmem ) ;
	BIO_set_flags ( b64 , BIO_FLAGS_BASE64_NO_NL ) ;

	UChar* buf = iMem->New < UChar > ( surSz ) ;
	if(NULL ==  buf){
		BIO_free_all ( bmem ) ;
		return DF_CCrypto_DecodeB64_CMem_New ;
	}

	int rtn = BIO_read ( bmem , buf , surSz ) ;
	BIO_free_all ( bmem ) ;
	if ( 0 >= rtn ) {
		delete [ ] buf ;
		return DF_CCrypto_DecodeB64_Bio_Read ;
	}

	FILE* fp = fopen ( targetPath.c_str ( ) , "wb+" ) ;
	if ( NULL == fp ) {
		delete [ ] buf ;
		return DF_CCrypto_DecodeB64_Fopen ;
	}
	fwrite ( buf , 1 , rtn , fp ) ;
	fclose ( fp ) ;
	delete [ ] buf ;
	return DF_Succ ;
}

int CCrypto::Md5Bin ( CStrType* surData ) {
	iMd5Bin.clear ( ) ;
#define DF_MD5_SZ ( 16 )
	UChar buf [ DF_MD5_SZ ] = { 0 } ;
	if ( NULL == MD5 ( surData->RtnStr ( ) , surData->Size ( ) , buf ) ) {
		return DF_CCrypto_Md5Bin_Md5 ;
	}

	for ( int i = 0 ; i < DF_MD5_SZ ; ++ i ) {
		char tmp [ 2 + 1 ] = { 0 } ;
		snprintf ( tmp , sizeof ( tmp ) , "%02x" , buf [ i ] ) ;
		iMd5Bin += tmp ;
	}

	return DF_Succ ;
}
Str CCrypto::RtnMd5Bin ( void ) {
	Str str = iMd5Bin ;
	iMd5Bin.clear ( ) ;
	return str ;
}

int CCrypto::Md5FromFileBin ( CStr surFilePath ) {
	iMd5FromFileBin.clear ( ) ;

	int rtn = iMyStrPar->ReadFileBin ( surFilePath ) ;
	if ( DF_Succ != rtn ) {
		return DF_CCrypto_Md5FromFileBin_ReadFileBin ;
	}

	CStrType*& sourceBuf = iMyStrPar->RtnReadFileBin ( ) ;

	rtn = Md5Bin ( sourceBuf ) ;
	if ( DF_Succ != rtn ) {
		return DF_CCrypto_Md5FromFileBin_Md5Bin ;
	}
	iMd5FromFileBin = iMd5Bin ;

	return DF_Succ ;
}
Str CCrypto::RtnMd5FromFileBin ( ) {
	Str str = iMd5FromFileBin ;
	iMd5FromFileBin.clear ( ) ;
	return str ;
}



int CCrypto::Md5Str ( Str surData ) {
	i_md5_str.clear ( ) ;
#define DF_MD5_SZ ( 16 )
	UChar buf [ DF_MD5_SZ ] = { 0 } ;
	if ( NULL == MD5 ( ( UChar* ) surData.c_str ( ) , surData.size ( ) , buf ) ) {
		return DF_CCrypto_Md5Str_Md5 ;
	}

	for ( int i = 0 ; i < DF_MD5_SZ ; ++ i ) {
		char tmp [ 2 + 1 ] = { 0 } ;
		snprintf ( tmp , sizeof ( tmp ) , "%02x" , buf [ i ] ) ;
		i_md5_str += tmp ;
	}

	return DF_Succ ;
}
Str CCrypto::RtnMd5Str ( void ) {
	Str str = i_md5_str ;
	i_md5_str.clear ( ) ;
	return str ;
}
Str CCrypto::Md5_str ( Str input ) {
	Md5Str ( input ) ;
	return RtnMd5Str ( ) ;
}







