#ifndef __CPS_API_H
#define __CPS_API_H

#include "Http.h"
#include "Crypto.h"

#define DF_cf__deviceNo        "$deviceNo"
#define DF_cf__CinPulseTime    "$CinPulseTime"
#define DF_cf__cpsserver       "$cpsserver"
#define DF_cf__Port            "$port"

class CCPS_API {
	private:
		CMem *i_mem ;
		CMyStrPar* i_parser ;
		CClient* i_client ;
		CCrypto* i_cpto ;

	public:
		CCPS_API ( ) ;
		~CCPS_API ( ) ;
	private:
		Str i_LocalIP ;
		Str i_conf_file ;
		Str i_deviceNo  ;
		Str i_CPSserver ;
		Str i_port ;
		Str i_token ;
        
		Str i_chargeNo ;
		Str i_chargeMode ;
		Str i_chargeTimes ;

		Str i_ProxyIp ;
		Str i_ProxyPort ;
		Str i_ProxyUserid ;
		Str i_ProxyPassed ;
		Str i_r6IP ;
		Str i_r6Port ;
		Str i_eccBlcName ;
		Str i_BLCName;
        
        Str i_upgradeTime;
        Str i_deviceErrorLogUrl;

		Str i_transactionTime ;
        
        
	public:
		/*
		 * Str conf_file                  : upgrade ³]©wÀÉ®×
		 * Str save_download_data_tmp_dir : žê®Æ€Užü«á©ÒŒÈ®É©ñžmªºŠìžm
		 * Str save_process_dir           : €Užü§¹Šš«á©Ò­n§ó·sªº®Ú¥Ø¿ý
		 */
		int Import_path ( Str conf_file ) ;

		Str setWriteHttpsData ( Str https_API, Str bady , Str Content_Type ) ;

		int Get_apiLogin ( void ) ;
		int Get_HeartBeat ( void ) ;
		int Get_ProxySetting ( void ) ;
		int Get_ECCSetting ( void ) ;
		int Get_ConfigSetting ( void ) ;
		int Set_uploadTransactionRecord ( Str cardNo, Str payment, Str readerDeviceNo, Str batchNo, Str RRN, Str beforeBalance, Str autoLoadAmount, Str afterBalance ) ;
		int Set_uploadCheckoutRecord ( Str cardNo, Str checkoutBatchNo, Str accountingStatus, Str readerDeviceNo, Str numbersPurchase, Str numbersRefund, Str numbersAutoLoad, Str numbersTotal, Str amountPurchase, Str amountRefund, Str amountAutoLoad, Str amountTotal, Str transactionNumbers, Str transactionAmount, Str transactionNetAmount );
		int Set_uploadSystemLog ( Str cardNo, Str recordCode, Str remark ) ;
		int Set_uploadMonitorData ( void ) ;

		int Get_Proxy_blc_download ( void ) ;

		Str i_CinPulseTime;
		Str i_chargeAmount ;
		Str i_deviceVersion;
		Str i_hearBeatCycleTime ;
		Str i_deductionMode ;
};

#endif

