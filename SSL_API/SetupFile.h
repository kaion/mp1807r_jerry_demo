#ifndef SETUPFILE_H_
#define SETUPFILE_H_

#include <iostream>
#include <list>
#include <fstream>

extern "C" {
	#include <sys/time.h>
	#include <sys/file.h>
	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <time.h>
	#include <termios.h>
	#include <sys/socket.h>
	#include <sys/types.h>
	#include <netdb.h>
	#include <arpa/inet.h>
	#include <openssl/bio.h>
	#include <openssl/ssl.h>
	#include <openssl/err.h>
	#include <sys/epoll.h>
	#include <sys/stat.h>
	#include <sys/wait.h>
	#include <fcntl.h>
	#include <sys/file.h>
	#include <ctype.h>
	#include <stdarg.h>
	#include <sys/mman.h>
	#include <net/if.h>
	#include <sys/ioctl.h>
	#include <dirent.h>
	#include <errno.h>
	#include <netinet/in.h>
	#include <sys/un.h>
	#include <openssl/md5.h>
	#include <stdarg.h>
	#include <pthread.h>
	#include <sys/ioctl.h>
}

#define DF_CCrypto                                                   - 100
#define DF_CEpoll                                                    - 200
#define DF_CFtp                                                      - 300
#define DF_CGpio                                                     - 400
#define DF_CRdWrTmO                                                  - 500
#define DF_CClient                                                   - 600
#define DF_CUxsktSrv                                                 - 700
#define DF_CUxsktClnt                                                - 800
#define DF_CJsonPar                                                  - 900
#define DF_CJsonParV1                                                - 950
#define DF_CLog                                                      - 1000
#define DF_CStrType                                                  - 1100
#define DF_CMyStrPar                                                 - 1200
#define DF_CPgmPath                                                  - 1300
#define DF_CCmprt                                                    - 1400
#define DF_CIoctl                                                    - 1500
#define DF_CMqtt                                                     - 1600

#define D_CLokc                                                      - 1700
#define D_C_timer                                                    - 1750

/*
 * 用來作 unix socket break errno default
 */
#define DF_Errno_Default_Value                                       0XFFFFFFFF

/*
 * 為了和初始化都為 0 刻意用此分別因為目前程式都是包成物件,就是怕有人忘記執行初始
 */
#define DF_Succ                                                      1
#define DF_UnSucc                                                    - 1   // 一般簡易的非成功選線

#define D_succ                                                       DF_Succ
#define D_fail                                                       DF_UnSucc

#define DF_Parameter                                                 - 2


// CCrypto //

#define DF_CCrypto_Sha1ForFileBin_ReadFileBin                        DF_CCrypto - 1

#define DF_CCrypto_EncodeB64Bin_ReadFileBin                          DF_CCrypto_Sha1ForFileBin_ReadFileBin - 1
#define DF_CCrypto_EncodeB64Bin_CMem_New                             DF_CCrypto_EncodeB64Bin_ReadFileBin - 1
#define DF_CCrypto_EncodeB64Bin_Bio_Read_Data_Not_Match              DF_CCrypto_EncodeB64Bin_CMem_New - 1

#define DF_CCrypto_DecodeB64_CMem_New                                DF_CCrypto_EncodeB64Bin_Bio_Read_Data_Not_Match - 1
#define DF_CCrypto_DecodeB64_Bio_Read                                DF_CCrypto_DecodeB64_CMem_New - 1
#define DF_CCrypto_DecodeB64_Fopen                                   DF_CCrypto_DecodeB64_Bio_Read - 1

#define DF_CCrypto_Md5FromFileBin_ReadFileBin                        DF_CCrypto_DecodeB64_Fopen - 1
#define DF_CCrypto_Md5FromFileBin_Md5Bin                             DF_CCrypto_Md5FromFileBin_ReadFileBin - 1

#define DF_CCrypto_Md5Bin_No_Input_Data                              DF_CCrypto_Md5FromFileBin_Md5Bin - 1
#define DF_CCrypto_Md5Bin_Md5                                        DF_CCrypto_Md5Bin_No_Input_Data - 1
#define DF_CCrypto_Md5Str_Md5                                        DF_CCrypto_Md5Bin_Md5 - 1
#define DF_CCrypto_SortMakeMd5_Md5                                   DF_CCrypto_Md5Str_Md5 - 1




// CEpoll //
#define DF_CEpoll_CreateEpoll_Epoll_Number_Less                      DF_CEpoll - 1
#define DF_CEpoll_CreateEpoll_Epoll_Create                           DF_CEpoll_CreateEpoll_Epoll_Number_Less - 1
#define DF_CEpoll_CreateEpoll_New_Event_Buf                          DF_CEpoll_CreateEpoll_Epoll_Create - 1

#define DF_CEpoll_AddEpollCtl_Epoll_Ctl                              DF_CEpoll_CreateEpoll_New_Event_Buf - 1

#define DF_CEpoll_DelEpollCtl_Epoll_Ctl                              DF_CEpoll_AddEpollCtl_Epoll_Ctl - 1

#define DF_CEpoll_ModEpollCtl_Epoll_Ctl                              DF_CEpoll_DelEpollCtl_Epoll_Ctl - 1



// CFtp //
#define DF_CFtp_PasvConnect_CClient_WriteBin                         DF_CFtp - 1
#define DF_CFtp_PasvConnect_CClient_ReadBin                          DF_CFtp_PasvConnect_CClient_WriteBin - 1
#define DF_CFtp_PasvConnect_Sscanf                                   DF_CFtp_PasvConnect_CClient_ReadBin - 1

#define DF_CFtp_WriteAndReadForCmmd_CClient_WriteStr                 DF_CFtp_PasvConnect_Sscanf - 1
#define DF_CFtp_WriteAndReadForCmmd_CClient_ReadStr                  DF_CFtp_WriteAndReadForCmmd_CClient_WriteStr - 1

#define DF_CFtp_ReadForCmmd_CClient_ReadStr                          DF_CFtp_WriteAndReadForCmmd_CClient_ReadStr - 1

#define DF_CFtp_BeginConnect_CClient_Connect                         DF_CFtp_ReadForCmmd_CClient_ReadStr - 1
#define DF_CFtp_BeginConnect_CRdWrTmO_SetsockoptMethod               DF_CFtp_BeginConnect_CClient_Connect - 1
#define DF_CFtp_BeginConnect_ReadForCmmd_Not_220                     DF_CFtp_BeginConnect_CRdWrTmO_SetsockoptMethod - 1
#define DF_CFtp_BeginConnect_WriteAndReadForCmmd_Not_331             DF_CFtp_BeginConnect_ReadForCmmd_Not_220 - 1
#define DF_CFtp_BeginConnect_WriteAndReadForCmmd_Not_230             DF_CFtp_BeginConnect_WriteAndReadForCmmd_Not_331 - 1

#define DF_CFtp_List_PasvConnect                                     DF_CFtp_BeginConnect_WriteAndReadForCmmd_Not_230 - 1
#define DF_CFtp_List_CClient_Connect                                 DF_CFtp_List_PasvConnect - 1
#define DF_CFtp_List_WriteAndReadForCmmd_Big_300                     DF_CFtp_List_CClient_Connect - 1
#define DF_CFtp_List_ReadStr                                         DF_CFtp_List_WriteAndReadForCmmd_Big_300 - 1
#define DF_CFtp_List_ReadForCmmd_Not_226                             DF_CFtp_List_ReadStr - 1

#define DF_CFtp_DownLoad_PasvConnect                                 DF_CFtp_List_ReadForCmmd_Not_226 - 1
#define DF_CFtp_DownLoad_CClient_Connect                             DF_CFtp_DownLoad_PasvConnect - 1
#define DF_CFtp_DownLoad_WriteAndReadForCmmd_Not_200                 DF_CFtp_DownLoad_CClient_Connect - 1
#define DF_CFtp_DownLoad_WriteAndReadForCmmd_Big_300                 DF_CFtp_DownLoad_WriteAndReadForCmmd_Not_200 - 1
#define DF_CFtp_DownLoad_CClient_ReadToFile                          DF_CFtp_DownLoad_WriteAndReadForCmmd_Big_300 - 1
#define DF_CFtp_DownLoad_ReadForCmmd_Not_226                         DF_CFtp_DownLoad_CClient_ReadToFile - 1

#define DF_CFtp_UpLoad_PasvConnect                                   DF_CFtp_DownLoad_ReadForCmmd_Not_226 - 1
#define DF_CFtp_UpLoad_CClient_Connect                               DF_CFtp_UpLoad_PasvConnect - 1
#define DF_CFtp_UpLoad_WriteAndReadForCmmd_Not_200                   DF_CFtp_UpLoad_CClient_Connect - 1
#define DF_CFtp_UpLoad_WriteAndReadForCmmd_Not_300                   DF_CFtp_UpLoad_WriteAndReadForCmmd_Not_200 - 1
#define DF_CFtp_UpLoad_Fopen                                         DF_CFtp_UpLoad_WriteAndReadForCmmd_Not_300 - 1
#define DF_CFtp_UpLoad_ReadForCmmd_Not_226                           DF_CFtp_UpLoad_Fopen - 1

#define DF_CFtp_RmDir_WriteAndReadForCmmd_Not_250                    DF_CFtp_UpLoad_ReadForCmmd_Not_226 - 1

#define DF_CFtp_RmFile_WriteAndReadForCmmd_Not_250                   DF_CFtp_RmDir_WriteAndReadForCmmd_Not_250 - 1

#define DF_CFtp_ReName_WriteAndReadForCmmd_Not_350                   DF_CFtp_RmFile_WriteAndReadForCmmd_Not_250 - 1
#define DF_CFtp_ReName_WriteAndReadForCmmd_Not_250                   DF_CFtp_ReName_WriteAndReadForCmmd_Not_350 - 1

#define DF_CFtp_ChangeCwd_WriteAndReadForCmmd_Not_250                DF_CFtp_ReName_WriteAndReadForCmmd_Not_250 - 1

#define DF_CFtp_Mkdir_WriteAndReadForCmmd_Not_257                    DF_CFtp_ChangeCwd_WriteAndReadForCmmd_Not_250 - 1

#define DF_CFtp_Quit_WriteAndReadForCmmd_Not_221                     DF_CFtp_Mkdir_WriteAndReadForCmmd_Not_257 - 1


// CGPIO //
#define DF_CGpio_SetupGpio_Setup_Gpio_Type                           DF_CGpio - 1
#define DF_CGpio_RtnInterruptFd_Open                                 DF_CGpio_SetupGpio_Setup_Gpio_Type - 1

#define DF_CGpio_SetupGpio_Fopen_Export                              DF_CGpio_RtnInterruptFd_Open - 1
#define DF_CGpio_SetupGpio_Fopen_Direction                           DF_CGpio_SetupGpio_Fopen_Export - 1

#define DF_CGpio_WriteToGpio_Fopen_Value                             DF_CGpio_SetupGpio_Fopen_Direction - 1

#define DF_CGpio_ReadFromeGpio_Fopen_Value                           DF_CGpio_WriteToGpio_Fopen_Value - 1


#define DF_CGpio_ForcePin_Fopen_Edge                                 DF_CGpio_ReadFromeGpio_Fopen_Value - 1
#define DF_CGpio_ForcePin_PinType                                    DF_CGpio_ForcePin_Fopen_Edge - 1
#define DF_CGpio_ForcePin_Fopen_Direction                            DF_CGpio_ForcePin_PinType - 1
#define DF_CGpio_ForcePin_Fopen_Export                               DF_CGpio_ForcePin_Fopen_Direction - 1
#define DF_CGpio_ForcePin_PinNum                                     DF_CGpio_ForcePin_Fopen_Export - 1

#define DF_CGpio_ForcePin_Never                                      DF_CGpio_ForcePin_PinNum - 1



// CRDWRTMO //
#define DF_CRdWrTmO_SetsockoptMethod_Setsockopt_Write                DF_CRdWrTmO - 1
#define DF_CRdWrTmO_SetsockoptMethod_Setsockopt_Read                 DF_CRdWrTmO_SetsockoptMethod_Setsockopt_Write - 1




// CCLIENT //
#define DF_CClient_WriteBin_Write                                    DF_CClient - 1

#define DF_CClient_Connect_Getaddrinfo                               DF_CClient_WriteBin_Write - 1
#define DF_CClient_Connect_Socket                                    DF_CClient_Connect_Getaddrinfo - 1
#define DF_CClient_Connect_Connect                                   DF_CClient_Connect_Socket - 1

#define DF_CClient_WriteStr_Write                                    DF_CClient_Connect_Connect - 1

#define DF_CClient_ReadStr_Read                                      DF_CClient_WriteStr_Write - 1

#define DF_CClient_ReadBin_Read                                      DF_CClient_ReadStr_Read - 1

#define DF_CClient_ReadToFile_Fopen                                  DF_CClient_ReadBin_Read - 1
#define DF_CClient_ReadToFile_Read                                   DF_CClient_ReadToFile_Fopen - 1

#define DF_CClient_SslConnect_Ssl_Ctx_New                            DF_CClient_ReadToFile_Read - 1
#define DF_CClient_SslConnect_Ssl_New                                DF_CClient_SslConnect_Ssl_Ctx_New - 1
#define DF_CClient_SslConnect_Ssl_Set_fd                             DF_CClient_SslConnect_Ssl_New - 1
#define DF_CClient_SslConnect_Ssl_Connect                            DF_CClient_SslConnect_Ssl_Set_fd - 1

#define DF_CClient_SslWrite_Ssl_Write                                DF_CClient_SslConnect_Ssl_Connect - 1

#define DF_CClient_SslWriteStr_Ssl_Write                             DF_CClient_SslWrite_Ssl_Write - 1

#define DF_CClient_SslRead_Ssl_Read                                  DF_CClient_SslWriteStr_Ssl_Write - 1

#define DF_CClient_SslReadNew_CMem_Realloc                           DF_CClient_SslRead_Ssl_Read - 1

#define DF_CClient_SslReadNew_Ssl_Read                               DF_CClient_SslReadNew_CMem_Realloc - 1





// CUXSKTSRV //
#define DF_CUxsktSrv_ServerBegin_Socket                              DF_CUxsktSrv - 1
#define DF_CUxsktSrv_ServerBegin_Bind                                DF_CUxsktSrv_ServerBegin_Socket - 1
#define DF_CUxsktSrv_ServerBegin_Listen                              DF_CUxsktSrv_ServerBegin_Bind - 1

#define DF_CUxsktSrv_SetupWrAndRdTimeout_AcptFd_Err                  DF_CUxsktSrv_ServerBegin_Listen - 1
#define DF_CUxsktSrv_SetupWrAndRdTimeout_CRdWrTmO_SetsockoptMethod   DF_CUxsktSrv_SetupWrAndRdTimeout_AcptFd_Err - 1

#define DF_CUxsktSrv_DetectBreakCnnt_IipcSrvFd_Err                   DF_CUxsktSrv_SetupWrAndRdTimeout_CRdWrTmO_SetsockoptMethod - 1

#define DF_CUxsktSrv_ReCnntIpcSrv_ServerBegin                        DF_CUxsktSrv_DetectBreakCnnt_IipcSrvFd_Err - 1

#define DF_CUxsktSrv_Accept_IipcSrvFd_Err                            DF_CUxsktSrv_ReCnntIpcSrv_ServerBegin - 1
#define DF_CUxsktSrv_Accept_Accept                                   DF_CUxsktSrv_Accept_IipcSrvFd_Err - 1
#define DF_CUxsktSrv_Accept_CRdWrTmO_SetsockoptMethod                DF_CUxsktSrv_Accept_Accept - 1

#define DF_CUxsktSrv_WriteStrToAccetpFd_Write                        DF_CUxsktSrv_Accept_CRdWrTmO_SetsockoptMethod - 1

#define DF_CUxsktSrv_ReadUxScktFd_Read_Write_Side_Break              DF_CUxsktSrv_WriteStrToAccetpFd_Write - 1
#define DF_CUxsktSrv_ReadUxScktFd_Read_No_Data                       DF_CUxsktSrv_ReadUxScktFd_Read_Write_Side_Break - 1


// DF_CUXSKTCLNT //
#define DF_CUxsktClnt_ConnectServer_Socket                           DF_CUxsktClnt - 1
#define DF_CUxsktClnt_ConnectServer_Connect                          DF_CUxsktClnt_ConnectServer_Socket - 1
#define DF_CUxsktClnt_ConnectServer_CRdWrTmO_SetsockoptMethod        DF_CUxsktClnt_ConnectServer_Connect - 1

#define DF_CUxsktClnt_SetupWrAndRdTimeout_IipcFd_Err                 DF_CUxsktClnt_ConnectServer_CRdWrTmO_SetsockoptMethod - 1
#define DF_CUxsktClnt_SetupWrAndRdTimeout_CRdWrTmO_SetsockoptMethod  DF_CUxsktClnt_SetupWrAndRdTimeout_IipcFd_Err - 1

#define DF_CUxsktClnt_DetectBreakCnnt_IipcFd_Err                     DF_CUxsktClnt_SetupWrAndRdTimeout_CRdWrTmO_SetsockoptMethod - 1

#define DF_CUxsktClnt_ReCnntIpcSrv_ConnectServer                     DF_CUxsktClnt_DetectBreakCnnt_IipcFd_Err - 1

#define DF_CUxsktClnt_WriteStr_Write                                 DF_CUxsktClnt_ReCnntIpcSrv_ConnectServer - 1

#define DF_CUxsktClnt_ReadStr_Read_Write_Side_Break                  DF_CUxsktClnt_WriteStr_Write - 1
#define DF_CUxsktClnt_ReadStr_Read_No_Data                           DF_CUxsktClnt_ReadStr_Read_Write_Side_Break - 1




// CJsonPar //
// success //
#define DF_Succ_CJsonPar_SearchJson_Value                            2        // 回吐是一個值 //
#define DF_Succ_CJsonPar_SearchJson_String                           3        // 回吐是一個字串 //
#define DF_Succ_CJsonPar_SearchJson_Array                            4        // 回吐是一個 array //
#define DF_Succ_CJsonPar_SearchJson_Object                           5        // 回吐是一個物件 //
// error //
#define DF_CJsonPar_CompileJsonFormat_Object                         DF_CJsonPar - 1
#define DF_CJsonPar_CompileJsonFormat_Array                          DF_CJsonPar_CompileJsonFormat_Object - 1
#define DF_CJsonPar_CompileJsonFormat_Object_Not_Match               DF_CJsonPar_CompileJsonFormat_Array - 1
#define DF_CJsonPar_CompileJsonFormat_Array_Not_Match                DF_CJsonPar_CompileJsonFormat_Object_Not_Match - 1
#define DF_CJsonPar_CompileJsonFormat_Qmarks                         DF_CJsonPar_CompileJsonFormat_Array_Not_Match - 1
#define DF_CJsonPar_CompileJsonFormat_Format                         DF_CJsonPar_CompileJsonFormat_Qmarks - 1

#define DF_CJsonPar_SearchJson_Strstr                                DF_CJsonPar_CompileJsonFormat_Format - 1
#define DF_CJsonPar_SearchJson_Strpbrk                               DF_CJsonPar_SearchJson_Strstr - 1
#define DF_CJsonPar_SearchJson_IsearchJson_SZ_0                      DF_CJsonPar_SearchJson_Strpbrk - 1
#define DF_CJsonPar_SearchJson_Object                                DF_CJsonPar_SearchJson_IsearchJson_SZ_0 - 1
#define DF_CJsonPar_SearchJson_Array                                 DF_CJsonPar_SearchJson_Object - 1
#define DF_CJsonPar_SearchJson_Format                                DF_CJsonPar_SearchJson_Array - 1




///////

#define DF_CJsonParV1_Type_Value                                     "Value"        // 回吐是一個值 //
#define DF_CJsonParV1_Type_String                                    "String"       // 回吐是一個字串 //
#define DF_CJsonParV1_Type_Array                                     "Array"        // 回吐是一個 array //
#define DF_CJsonParV1_Type_Object                                    "Object"       // 回吐是一個物件 //



#define DF_CJsonParV1_ParserAll_InputData_0                          DF_CJsonParV1 - 1
#define DF_CJsonParV1_ParserAll_Key                                  DF_CJsonParV1_ParserAll_InputData_0 - 1
#define DF_CJsonParV1_ParserAll_Value                                DF_CJsonParV1_ParserAll_Key - 1
#define DF_CJsonParV1_ParserAll_Object_Not_Match                     DF_CJsonParV1_ParserAll_Value - 1
#define DF_CJsonParV1_ParserAll_Array_Not_Match                      DF_CJsonParV1_ParserAll_Object_Not_Match - 1

#define DF_CJsonParV1_SearchKey_Not_Search_Key                       DF_CJsonParV1_ParserAll_Array_Not_Match - 1

#define DF_CJsonParV1_DeleteKey_Not_Search_Key                       DF_CJsonParV1_SearchKey_Not_Search_Key - 1






// CLog //
#define DF_CLog_DefaultSetupFile_Fopen                               DF_CLog - 1

#define DF_CLog_ParserSetup_CPgmPath_TryGetPgmFileLock               DF_CLog_DefaultSetupFile_Fopen - 1
#define DF_CLog_ParserSetup_DefaultSetupFile                         DF_CLog_ParserSetup_CPgmPath_TryGetPgmFileLock - 1
#define DF_CLog_ParserSetup_CMyStrPar_ReadFile                       DF_CLog_ParserSetup_DefaultSetupFile - 1
#define DF_CLog_ParserSetup_Fopen                                    DF_CLog_ParserSetup_CMyStrPar_ReadFile - 1



// CStrType //
#define DF_CStrType_Assign_CMem_New                                  DF_CStrType - 1
#define DF_CStrType_Assign_SZ_Big_Source                             DF_CStrType_Assign_CMem_New - 1


// CMyStrPar //
#define DF_CMyStrPar_ReadFile_Fopen                                  DF_CMyStrPar - 1
#define DF_CMyStrPar_ReadFile_FREAD                                  DF_CMyStrPar_ReadFile_Fopen - 1

#define DF_CMyStrPar_ReadFileBin_Fopen                               DF_CMyStrPar_ReadFile_FREAD - 1
#define DF_CMyStrPar_ReadFileBin_Ftell                               DF_CMyStrPar_ReadFileBin_Fopen - 1
#define DF_CMyStrPar_ReadFileBin_CMem_New                            DF_CMyStrPar_ReadFileBin_Ftell - 1
#define DF_CMyStrPar_ReadFileBin_Fread                               DF_CMyStrPar_ReadFileBin_CMem_New - 1


#define D_CMyStrPar_open_file_error                                  DF_CMyStrPar_ReadFileBin_Fread - 1
#define DF_CMyStrPar_DecodeMyIpcFormat_Data_Less                     D_CMyStrPar_open_file_error - 1
#define DF_CMyStrPar_DecodeMyIpcFormat_No_Head                       DF_CMyStrPar_DecodeMyIpcFormat_Data_Less - 1
#define DF_CMyStrPar_DecodeMyIpcFormat_Data_SZ_0                     DF_CMyStrPar_DecodeMyIpcFormat_No_Head - 1

#define DF_CMyStrPar_SerchMyFormat_Find                              DF_CMyStrPar_DecodeMyIpcFormat_Data_SZ_0 - 1

#define DF_CMyStrPar_MyStrchr                                        DF_CMyStrPar_SerchMyFormat_Find - 1

#define DF_CMyStrPar_MyStrrchr                                       DF_CMyStrPar_MyStrchr - 1

#define DF_CMyStrPar_MyStrstr                                        DF_CMyStrPar_MyStrrchr - 1

#define DF_CMyStrPar_MyStrrstr                                       DF_CMyStrPar_MyStrstr - 1

#define DF_CMyStrPar_MyMkdir                                         DF_CMyStrPar_MyStrrstr - 1

#define DF_CMyStrPar_DelFileTooBig_Fopen                             DF_CMyStrPar_MyMkdir - 1
#define DF_CMyStrPar_DelFileTooBig_File_SZ_Err                       DF_CMyStrPar_DelFileTooBig_Fopen - 1
#define DF_CMyStrPar_DelFileTooBig_File_SZ_Not_Match                 DF_CMyStrPar_DelFileTooBig_File_SZ_Err - 1

#define DF_CMyStrPar_TarFileTooBig_Ftell                             DF_CMyStrPar_DelFileTooBig_File_SZ_Not_Match - 1
#define DF_CMyStrPar_TarFileTooBig_System                            DF_CMyStrPar_TarFileTooBig_Ftell - 1
#define DF_CMyStrPar_TarFileTooBig_Fopen                             DF_CMyStrPar_TarFileTooBig_System - 1



// CPgmPath //
#define DF_CPgmPath_TryGetPgmFileLock_Fopen                          DF_CPgmPath - 1
#define DF_CPgmPath_TryGetPgmFileLock_Flock                          DF_CPgmPath_TryGetPgmFileLock_Fopen - 1

// CCmprt //
#define DF_CCmprt_OenComport_Open                                    DF_CCmprt - 1
#define DF_CCmprt_OenComport_Tcsetattr                               DF_CCmprt_OenComport_Open - 1

#define DF_CCmprt_WriteBin_Write                                     DF_CCmprt_OenComport_Tcsetattr - 1

#define DF_CCmprt_ReadBin_Read                                       DF_CCmprt_WriteBin_Write - 1



// CCmprt //
#define DF_CIoctl_GetNetInfo_SocketFd                             DF_CIoctl - 1
#define DF_CIoctl_GetNetInfo_ioctl_SIOCGIFADDR                    DF_CIoctl_GetNetInfo_SocketFd - 1
#define DF_CIoctl_GetNetInfo_ioctl_SIOCGIFHWADDR                  DF_CIoctl_GetNetInfo_ioctl_SIOCGIFADDR - 1




// CMqtt //
#define DF_CMqtt_Connect_MQTTClient_create                        DF_CMqtt - 1
#define DF_CMqtt__import_never                                    DF_CMqtt_Connect_MQTTClient_create - 1
#define DF_CMqtt_Connect_MQTTClient_connect                       DF_CMqtt__import_never - 1

#define DF_CMqtt_SetSubscriber_Connect_Never                      DF_CMqtt_Connect_MQTTClient_connect - 1
#define DF_CMqtt_SetSubscriber_MQTTClient_subscribe               DF_CMqtt_SetSubscriber_Connect_Never - 1
#define DF_CMqtt_SetPublisher_Connect_Never                       DF_CMqtt_SetSubscriber_MQTTClient_subscribe - 1

#define DF_CMqtt_ReadBin_Connect_Never                            DF_CMqtt_SetPublisher_Connect_Never - 1
#define DF_CMqtt_ReadBin_SetSubscriber                            DF_CMqtt_ReadBin_Connect_Never - 1
#define DF_CMqtt_ReadBin_MQTTClient_receive                       DF_CMqtt_ReadBin_SetSubscriber - 1
#define DF_CMqtt_ReadBin_MQTTClient_receive_Timeout               DF_CMqtt_ReadBin_MQTTClient_receive - 1

#define DF_CMqtt_ReadStr_Connect_Never                            DF_CMqtt_ReadBin_MQTTClient_receive_Timeout - 1
#define DF_CMqtt_ReadStr_SetSubscriber                            DF_CMqtt_ReadStr_Connect_Never - 1
#define DF_CMqtt_ReadStr_MQTTClient_receive                       DF_CMqtt_ReadStr_SetSubscriber - 1
#define DF_CMqtt_ReadStr_MQTTClient_receive_Timeout               DF_CMqtt_ReadStr_MQTTClient_receive - 1


#define DF_CMqtt_WriteBin_Connect_Never                           DF_CMqtt_ReadStr_MQTTClient_receive_Timeout - 1
#define DF_CMqtt_WriteBin_SetSubscriber                           DF_CMqtt_WriteBin_Connect_Never - 1
#define DF_CMqtt_WriteBin_MQTTClient_publish                      DF_CMqtt_WriteBin_SetSubscriber - 1
#define DF_CMqtt_WriteBin_MQTTClient_waitForCompletion            DF_CMqtt_WriteBin_MQTTClient_publish - 1


#define DF_CMqtt_WriteStr_Connect_Never                           DF_CMqtt_WriteBin_MQTTClient_waitForCompletion - 1
#define DF_CMqtt_WriteStr_SetSubscriber                           DF_CMqtt_WriteStr_Connect_Never - 1
#define DF_CMqtt_WriteStr_MQTTClient_publish                      DF_CMqtt_WriteStr_SetSubscriber - 1
#define DF_CMqtt_WriteStr_MQTTClient_waitForCompletion            DF_CMqtt_WriteStr_MQTTClient_publish - 1

#define DF_CMqtt_ReConnect_Connect_Never                          DF_CMqtt_WriteStr_MQTTClient_waitForCompletion - 1

#define DF_CMqtt_RtnSocketFd_Connect_Never                        DF_CMqtt_ReConnect_Connect_Never - 1



// CLokc //
#define D_CLokc_Import_open_error                                 D_CLokc - 1
#define D_CLokc_Import_never                                      D_CLokc_Import_open_error - 1

#define D_CLokc_Lock_ex_try_timing__timeout                       D_CLokc_Import_never - 1
#define D_CLokc_Lock_sh_try_timing__timeout                       D_CLokc_Lock_ex_try_timing__timeout - 1

#define D_CLokc_Lock_ex_timing__timeout                           D_CLokc_Lock_sh_try_timing__timeout - 1
#define D_CLokc_Lock_sh_timing__timeout                           D_CLokc_Lock_ex_timing__timeout - 1


// C_timer //
#define D_C_timer__Import_timer__timer_num_error                  D_C_timer - 1
#define D_C_timer__Import_timer__new_error                        D_C_timer__Import_timer__timer_num_error - 1


typedef const          char            CChar;

typedef const unsigned char            CUChar;
typedef       unsigned char            UChar;

typedef const          int             CInt;

typedef const unsigned int             CUInt;
typedef       unsigned int             UInt;

typedef const long int                 CLInt;
typedef       long int                 LInt;

typedef const unsigned long int        CULInt;
typedef       unsigned long int        ULInt;

typedef const short int                CSInt;
typedef       short int                SInt;

typedef const unsigned short int       CUSInt;
typedef       unsigned short int       USInt;

typedef       long double              LDouble;

typedef const std::string              CStr ;
typedef       std::string              Str ;
#endif /* SETUPFILE_H_ */
