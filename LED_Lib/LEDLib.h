#ifndef __LEDLIB_H
#define __LEDLIB_H

#ifdef __cplusplus
extern "C"
{
#endif

void setLedInit(void);
void setLedValue(char setGpio, char setOnOff);

void setCinInit(void);
void setCinValue(int setDelayTime, char setOnOff);

void setUsbPowerInit(void);
void setUsbPowerValue(int setDelayTime, char setOnOff);

#ifdef __cplusplus
}
#endif

#endif

