#include <stdio.h>
#include <stdint.h>

#include "LEDLib.h"

void setLedInit(void)
{
    /*
    int8_t i = 18, setCommandData[50+1] = {0};
    for(i=18; i<22; i++)
    {
        sprintf(setCommandData, "echo %d > /sys/class/gpio/export", i);
        system(setCommandData);
        sprintf(setCommandData, "echo out > /sys/class/gpio/gpio%d/direction", i);
        system(setCommandData);
    }
    */
}

void setLedValue(char setGpio, char setOnOff)
{
    int8_t setCommandData[64+1] = {0};
    switch (setGpio)
    {
      case 18:
        sprintf(setCommandData, "echo %d > /sys/class/leds/mp1807r-blue/brightness", setOnOff );
        system(setCommandData);
        break;

      case 19:
        sprintf(setCommandData, "echo %d > /sys/class/leds/mp1807r-green/brightness", setOnOff );
        system(setCommandData);
        break;
        
      case 20:
        sprintf(setCommandData, "echo %d > /sys/class/leds/mp1807r-yellow/brightness", setOnOff );
        system(setCommandData);
        break;

      case 21:
        sprintf(setCommandData, "echo %d > /sys/class/leds/mp1807r-red/brightness", setOnOff );
        system(setCommandData);
        break;

      default:
        break;
    }
    /*
    int8_t setCommandData[40+1] = {0};
    sprintf(setCommandData, "echo %d > /sys/class/gpio/gpio%d/value", setOnOff, setGpio);
    system(setCommandData);
    */
}

void setCinInit(void)
{
    system("echo 0 > /sys/class/gpio/export");
    
    system("echo out > /sys/class/gpio/gpio0/direction");
}

void setCinValue(int setDelayTime, char setOnOff)
{
    int8_t setCommandData[40+1] = {0};
    sprintf(setCommandData, "echo %d > /sys/class/gpio/gpio0/value", setOnOff);
    system(setCommandData);
    usleep((setDelayTime * 1000 ));
}

void setUsbPowerInit(void)
{
    /*
    system("echo 15 > /sys/class/gpio/export");
    
    system("echo out > /sys/class/gpio/gpio15/direction");
    */
    system("echo 1 > /sys/class/pwm/pwmchip0/export");
    system("echo 1000000 > /sys/class/pwm/pwmchip0/pwm1/period");
    system("echo 1000000 > /sys/class/pwm/pwmchip0/pwm1/duty_cycle");


}

void setUsbPowerValue(int setDelayTime, char setOnOff)
{
    int8_t setCommandData[40+1] = {0};
    sprintf(setCommandData, "echo %d > /sys/class/gpio/gpio15/value", setOnOff);
    sprintf(setCommandData, "echo %d > /sys/class/pwm/pwmchip0/pwm1/enable", setOnOff);
    system(setCommandData);
    usleep((setDelayTime * 1000 ));
}

