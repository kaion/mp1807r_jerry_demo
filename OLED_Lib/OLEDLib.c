#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include "OLEDLib.h"

#define I2C_ADDR (0x78 >> 1)
#define FONT_MAX 41
#define WORD_MAX 128

#define SETFONTS0 "系統啟動中，請稍後"
#define SETFONTS1 "歡迎光臨，請靠卡"
#define SETFONTS2 "扣款中，請勿移動卡片"
#define SETFONTS3 "如需再次交易，請移開卡片再靠卡"
#define SETFONTS4 "扣款完成，請取回卡片"
#define SETFONTS5 "加值並扣款完成，請取回卡片"
#define SETFONTS6 "交易處理中，請稍後"
#define SETFONTS7 "請以相同卡片靠卡"

// Fone
#define A161 0x00,0x00,0x00,0x80,0xE0,0x70,0x10,0x18,0x18,0x10,0x70,0xE0,0x80,0x00,0x00,0x00,0x00,0x00,0xFC,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xF0,0x00,0x00,0x00,0x00,0x1F,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x80,0xFF,0xFF,0x07,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x07,0x04,0x04,0x04,0x06,0x07,0x03,0x00,0x00,0x00,0x00
#define A152 0x00,0x00,0x00,0x20,0x20,0x20,0xE0,0xF0,0xF8,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x04,0x07,0x07,0x07,0x04,0x04,0x04,0x00,0x00,0x00,0x00
#define A153 0x00,0x00,0xC0,0x60,0x30,0x10,0x10,0x18,0x18,0x10,0x30,0xF0,0xE0,0x80,0x00,0x00,0x00,0x00,0x0F,0x0E,0x0E,0x00,0x00,0x00,0x80,0xC0,0xF0,0x7F,0x3F,0x0F,0x00,0x00,0x00,0x00,0x00,0x80,0x60,0x38,0x0C,0x06,0x03,0x01,0x00,0x00,0x00,0xE0,0x00,0x00,0x00,0x00,0x06,0x07,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x06,0x07,0x03,0x00,0x00
#define A154 0x00,0x00,0x80,0xE0,0x30,0x10,0x10,0x18,0x18,0x18,0x30,0xF0,0xE0,0x00,0x00,0x00,0x00,0x00,0x01,0x07,0x07,0x02,0x40,0x40,0x40,0xE0,0xB0,0xBF,0x1F,0x03,0x00,0x00,0x00,0x00,0xF0,0xF0,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0xC7,0xFF,0xFE,0x00,0x00,0x00,0x00,0x01,0x03,0x06,0x04,0x04,0x04,0x04,0x04,0x06,0x03,0x03,0x00,0x00,0x00
#define A155 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0xE0,0xF8,0xF8,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xC0,0x70,0x1C,0x07,0x01,0xFF,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x30,0x3C,0x37,0x31,0x30,0x30,0x30,0x30,0xFF,0xFF,0xFF,0x30,0x30,0x30,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x04,0x04,0x07,0x07,0x07,0x04,0x04,0x00,0x00
#define A156 0x00,0x00,0x00,0xF0,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x10,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0x60,0x20,0x10,0x10,0x10,0x30,0x70,0xE0,0xC0,0x00,0x00,0x00,0x00,0x00,0xE0,0xE0,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x7F,0x00,0x00,0x00,0x00,0x01,0x03,0x06,0x04,0x04,0x04,0x04,0x06,0x07,0x03,0x01,0x00,0x00,0x00
#define A157 0x00,0x00,0x00,0xC0,0xE0,0x70,0x10,0x18,0x18,0x10,0x90,0xF0,0xC0,0x00,0x00,0x00,0x00,0x00,0xFC,0xFF,0xFF,0x40,0x20,0x20,0x20,0x20,0x61,0xE3,0xC1,0x00,0x00,0x00,0x00,0x00,0x3F,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0x7F,0x00,0x00,0x00,0x00,0x00,0x01,0x03,0x07,0x04,0x04,0x04,0x04,0x06,0x03,0x01,0x00,0x00,0x00
#define A158 0x00,0x00,0xF0,0x70,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0x30,0xB0,0x70,0x30,0x00,0x00,0x00,0x03,0x00,0x00,0x00,0x00,0x00,0x80,0xF0,0x3C,0x07,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xF8,0xFF,0x07,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x07,0x00,0x00,0x00,0x00,0x00,0x00,0x00
#define A159 0x00,0x00,0x80,0xE0,0xF0,0x30,0x10,0x18,0x18,0x10,0x30,0xF0,0xC0,0x00,0x00,0x00,0x00,0x00,0x07,0x0F,0x3F,0xB8,0xF0,0xE0,0xC0,0xE0,0xB0,0x1F,0x0F,0x00,0x00,0x00,0x00,0x00,0xFC,0xFF,0x03,0x01,0x00,0x00,0x00,0x01,0x03,0x8F,0xFE,0xFC,0x00,0x00,0x00,0x00,0x01,0x03,0x07,0x06,0x04,0x04,0x04,0x04,0x06,0x07,0x03,0x00,0x00,0x00
#define A160 0x00,0x00,0x00,0xC0,0xF0,0x30,0x10,0x18,0x18,0x10,0x70,0xE0,0xC0,0x00,0x00,0x00,0x00,0x00,0x7F,0xFF,0xFF,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFF,0xFE,0x00,0x00,0x00,0x00,0x00,0xC1,0xE3,0xC3,0x06,0x02,0x02,0x02,0x01,0xFF,0xFF,0x3F,0x00,0x00,0x00,0x00,0x00,0x01,0x03,0x04,0x04,0x04,0x04,0x04,0x07,0x03,0x00,0x00,0x00,0x00
#define A162 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x70,0xF0,0xF0,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x0F,0x0F,0x06,0x00,0x00,0x00,0x00,0x00,0x00

uint8_t g_Fone[11][64+1] ={{A161}, {A152}, {A153}, {A154}, {A155}, {A156}, {A157}, {A158}, {A159}, {A160}, {A162}};

char g_flagMarqueeStop = 0;
int32_t g_fd = 0;
uint8_t g_Clear_One_Data[WORD_MAX+1] = {0x00};
uint8_t g_Marquee_Data[FONT_MAX][WORD_MAX+1] ={0x00};

typedef enum {
  FONT_NONE,
  STDFONT,
  SPCFONT,
  SPCFSUPP,
  ASCFONT
} FONTTYPE;
int showWord(char *sz, int8_t setAdder);
int getFontIndex(unsigned char *sz, int *p_font_index);

#include <sys/time.h>
#include <time.h>
#include <math.h>

//#define __write(...) thistime(); printf(", %s(%d), %s :\n",__FILE__, __LINE__, __FUNCTION__);_write(__VA_ARGS__);
#define __write(...) write(__VA_ARGS__);
void thistime( void ){
  char buffer[26];
  int millisec;
  struct tm* tm_info;
  struct timeval tv;

  gettimeofday(&tv, NULL);

  millisec = lrint(tv.tv_usec/1000.0); // Round to nearest millisec
  if (millisec>=1000) { // Allow for rounding up to nearest second
    millisec -=1000;
    tv.tv_sec++;
  }

  tm_info = localtime(&tv.tv_sec);

  strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);
  printf("%s.%03d", buffer, millisec);

}

void _write(int fd,const void *data,size_t size){
  char ascii[17];
  size_t i, j;
  ascii[16] = '\0';
  /*
  ((unsigned char*)data)[0] = 0x12;
  size = 1;
  */
  for (i = 0; i < size; ++i) {
    printf("%02X ", ((unsigned char*)data)[i]);
    if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
      ascii[i % 16] = ((unsigned char*)data)[i];
    } else {
      ascii[i % 16] = '.';
    }
    if ((i+1) % 8 == 0 || i+1 == size) {
      printf(" ");
      if ((i+1) % 16 == 0) {
        printf("|  %s \n", ascii);
      } else if (i+1 == size) {
        ascii[(i+1) % 16] = '\0';
        if ((i+1) % 16 <= 8) {
          printf(" ");
        }
        for (j = (i+1) % 16; j < 16; ++j) {
          printf("   ");
        }
        printf("|  %s \n", ascii);
      }
    }
  }
  printf("(%2d -> %2d)\n\n", size, write(fd, data, size));
 
}
void inital(void)
{
    uint8_t OLED_init_Data[23+1] = {0x00, 0xae, 0xa8, 0x1f, 0xd3, 0x23, 0xa1, 0xc0, 0xa6, 0x40, 0xa4, 0x81, 0x75, 0xd9, 0x11, 0xd5, 0x81, 0xda, 0x12, 0xdb, 0x30, 0x00, 0xaf};
    
//    system("mt7688_pinmux set spi_s gpio");
    system("echo 41 > /sys/class/gpio/export");
    system("echo out > /sys/class/gpio/gpio41/direction");

    system("echo 0 > /sys/class/gpio/gpio41/value");
    system("echo 0 > /sys/class/pwm/pwmchip0/export");
    system("echo 0 > /sys/class/pwm/pwmchip0/pwm0/enable");
    system("echo 1000000 > /sys/class/pwm/pwmchip0/pwm0/period");
    system("echo 1000000 > /sys/class/pwm/pwmchip0/pwm0/duty_cycle");

//    system("echo 14 > /sys/class/gpio/export");
//    system("echo out > /sys/class/gpio/gpio14/direction");
//    system("echo 0 > /sys/class/gpio/gpio14/value");
    usleep(100000);
    system("echo 1 > /sys/class/gpio/gpio41/value");


    //__write(g_fd, OLED_init_Data, sizeof(OLED_init_Data)-1);
    __write(g_fd, &OLED_init_Data[0], sizeof(OLED_init_Data)-3);
//    system("echo 1 > /sys/class/gpio/gpio14/value");
    system("echo 1 > /sys/class/pwm/pwmchip0/pwm0/enable");
    usleep(100000);
    __write(g_fd, &OLED_init_Data[sizeof(OLED_init_Data)-3], 2);
    
    return ;
}

int8_t OLED_Open(void)
{
    g_fd = open("/dev/i2c-0", O_RDWR);
    if(g_fd < 0)
    {
        printf("Error i2c-0\n");
        return -1;
    }
    
    if(ioctl(g_fd, I2C_SLAVE, I2C_ADDR) < 0)
    {
        printf("ERROR ADDR\n");
        return -2;
    }
    
    return 0;
}

void OLED_Close(void)
{
    close(g_fd);
}

void CleanDDR(void)
{
    int32_t i=0, j=0;
    uint8_t Tmp = 0xb0, Clean_Data[4+1] = {0x00, 0xb0, 0x00, 0x10}, Show_Data[256+1] = {0};
    for(i=0; i<4; i++)
    {
        Clean_Data[1] = Tmp + i;
        __write(g_fd, Clean_Data, sizeof(Clean_Data)-1);
        Show_Data[0] = 0x40;
        __write(g_fd, Show_Data, 33);
        __write(g_fd, Show_Data, 33);
        __write(g_fd, Show_Data, 33);
        __write(g_fd, Show_Data, 33);
        __write(g_fd, Show_Data, 33);
        __write(g_fd, Show_Data, 33);
        __write(g_fd, Show_Data, 33);
        __write(g_fd, Show_Data, 33);
        /*
        Show_Data[0] = 0x40;
        __write(g_fd, Show_Data, sizeof(Show_Data)-1);
        Show_Data[0] = 0x40;
        __write(g_fd, Show_Data, 3);
        */
    }
    
    return ;
}

void OLEDShowFont_16X32(uint8_t cLine, uint8_t cColumn, uint32_t iLen, uint8_t *cpString)
{
    uint8_t i = 0, Init_Data[4+1] = {0x00, 0xb0, 0x00, 0x10}, Show_Data[17+1] = {0};
    
    for(i=0; i<4; i++)
    {
        Init_Data[1] = (0xb0 + i);
        Init_Data[2] = (0x00 + cLine);
        Init_Data[3] = (0x10 + cColumn);
        __write(g_fd, Init_Data, sizeof(Init_Data)-1);
        Show_Data[0] = 0x40;
        memcpy(&Show_Data[1], &cpString[( i * 16 )], sizeof(Show_Data)-1);
        __write(g_fd, Show_Data, sizeof(Show_Data)-1);
    }
    
    return ;
}

void OLEDShowFont_32X32(uint8_t cLine, uint8_t cColumn, uint32_t iLen, uint8_t *cpString)
{
    uint8_t i = 0, Init_Data[4+1] = {0x00, 0xb0, 0x00, 0x10}, Show_Data[33+1] = {0};
    
    for(i=0; i<4; i++)
    {
        Init_Data[1] = (0xb0 + i);
        Init_Data[2] = (0x00 + cLine);
        Init_Data[3] = (0x10 + cColumn);
        __write(g_fd, Init_Data, sizeof(Init_Data)-1);
        Show_Data[0] = 0x40;
        memcpy(&Show_Data[1], &cpString[( i * 32 )], sizeof(Show_Data)-1);
        __write(g_fd, Show_Data, sizeof(Show_Data)-1);
    }
    
    return ;
}

#define SHOWDELAYTIME 150000

void Run_Marquee_32X32_API(int8_t Word_conut, int8_t * Word)
{
    int8_t i = 0, n = 0, nn = 0, t = 0, tt = 0, ct = 0, setAdder = 0, Tmp_Word = 0;
    
    g_flagMarqueeStop = 0;
    memset(g_Marquee_Data, 0, sizeof(g_Marquee_Data));
    Tmp_Word = Word[0];
    
    switch( Tmp_Word )
    {
        case '0':
            Word_conut = strlen(SETFONTS0);
            memcpy(Word, SETFONTS0, Word_conut);
        break;
        case '1':
            Word_conut = strlen(SETFONTS1);
            memcpy(Word, SETFONTS1, Word_conut);
        break;
        case '2':
            Word_conut = strlen(SETFONTS2);
            memcpy(Word, SETFONTS2, Word_conut);
        break;
        case '3':
            Word_conut = strlen(SETFONTS3);
            memcpy(Word, SETFONTS3, Word_conut);
        break;
        case '4':
            Word_conut = strlen(SETFONTS4);
            memcpy(Word, SETFONTS4, Word_conut);
        break;
        case '5':
            Word_conut = strlen(SETFONTS5);
            memcpy(Word, SETFONTS5, Word_conut);
        break;
        case '6':
            Word_conut = strlen(SETFONTS6);
            memcpy(Word, SETFONTS6, Word_conut);
        break;
        case '7':
            Word_conut = strlen(SETFONTS7);
            memcpy(Word, SETFONTS7, Word_conut);
        break;
    }
    
    for(i=0; i<Word_conut; i+=2)
    {
        showWord(&Word[i], setAdder++);
    }
    
    for(i=0; i<Word_conut/2; i++)
    {
        ct = 4;
        for(n=i, t=6; n>=0; n--, t-=2)
        {
            OLEDShowFont_32X32(0, t, 0, g_Marquee_Data[ n ]);
            ct--;
            if( ct == 0)
            {
                n = 0;
            }
        }
        if(g_flagMarqueeStop)
        {
            return;
        }
        usleep(SHOWDELAYTIME);
        if(g_flagMarqueeStop)
        {
            return;
        }
    }
    OLEDShowFont_32X32(0, 6, 0, g_Clear_One_Data);
    for(nn=3, tt = 4; nn>0; nn--, tt-=2)
    {
        ct = nn;
        for(n=i-1, t=tt; n>=0; n--, t-=2)
        {
            OLEDShowFont_32X32(0, t, 0, g_Marquee_Data[ n ]);
            ct--;
            if( ct == 0)
            {
                n = 0;
            }
        }
        if(g_flagMarqueeStop)
        {
            return;
        }
        usleep(SHOWDELAYTIME);
        if(g_flagMarqueeStop)
        {
            return;
        }
        OLEDShowFont_32X32(0, tt, 0, g_Clear_One_Data);
    }
    if(!g_flagMarqueeStop)
    {
        Word[0] = Tmp_Word;
    }
}

void runBalance_API(int Balance)
{
    int8_t setAdder = 0;
    showWord("餘", setAdder++);
    OLEDShowFont_32X32(0, 0, 0, g_Marquee_Data[0]);
    OLEDShowFont_16X32(0, 2, 0, g_Fone[10]);
    OLEDShowFont_16X32(0, 3, 0, g_Fone[Balance/10000]);
    Balance %= 10000;
    OLEDShowFont_16X32(0, 4, 0, g_Fone[Balance/1000]);
    Balance %= 1000;
    OLEDShowFont_16X32(0, 5, 0, g_Fone[Balance/100]);
    Balance %= 100;
    OLEDShowFont_16X32(0, 6, 0, g_Fone[Balance/10]);
    Balance %= 10;
    OLEDShowFont_16X32(0, 7, 0, g_Fone[Balance/1]);
}

int showWord(char *sz, int8_t setAdder)
{
  int col_count = 0;
  int offset = 0;
  int index = 0;
  int sign_num = 0;

  FILE *fp = NULL;

#define PREFIX_PATH "./fonts/"
#define EXT  "32s"


//#define FONT_SIZE   15 // 16x15
//#define FONT_SIZE   16 // 16x16
//#define FONT_SIZE   24 // 24x24
#define FONT_SIZE   32 // 32x32
//#define FONT_SIZE   40 // 40x40
//#define FONT_SIZE   48 // 48x48
//#define FONT_SIZE   72 // 72x72

#if FONT_SIZE == 15
  #define FONT_ARRAY_SIZE  30
#elif FONT_SIZE == 16
  #define FONT_ARRAY_SIZE  32
#elif FONT_SIZE == 24
  #define FONT_ARRAY_SIZE  72
#elif FONT_SIZE == 32
  #define FONT_ARRAY_SIZE  128
#elif FONT_SIZE == 40
  #define FONT_ARRAY_SIZE  200
#elif FONT_SIZE == 48
  #define FONT_ARRAY_SIZE  288
#elif FONT_SIZE == 72
  #define FONT_ARRAY_SIZE  648
#endif


  int font_size = FONT_SIZE;
  unsigned char ch[FONT_ARRAY_SIZE];
  unsigned char ch_transform[FONT_ARRAY_SIZE];

  int part_size = 0;

  
  int font_type = getFontIndex((unsigned char *)sz, &index);

  switch(font_type)
  {
  case STDFONT:
    fp = fopen(PREFIX_PATH "STDFONT." EXT, "rb");
    break;

  case SPCFONT:
    fp = fopen(PREFIX_PATH "SPCFONT." EXT, "rb");
    break;

  case SPCFSUPP:
    fp = fopen(PREFIX_PATH "SPCFSUPP." EXT, "rb");
    break;

  case ASCFONT:
    fp = fopen(PREFIX_PATH "ASCFONT." EXT, "rb");
    break;

  default:
    return -1;
  }

  if(fp == NULL)
  {
    return -2;
  }

  switch(font_size)
  {
  case 15: // 16x15
  case 16: // 16x16
    col_count = (font_type == ASCFONT) ? 1 : 2;
    break;

  case 24: // 24x24
    col_count = (font_type == ASCFONT) ? 2 : 3;
    break;

  case 32: // 32x32
    col_count = (font_type == ASCFONT) ? 3 : 4;
    break;

  case 40: // 40x40
    col_count = (font_type == ASCFONT) ? 5 : 6;
    break;

  case 48: // 48x48
    col_count = (font_type == ASCFONT) ? 5 : 6;
    break;

  case 72: // 72x72
    col_count = (font_type == ASCFONT) ? 8 : 9;
    break;

  default:
    return -5;
  }

  offset = col_count * font_size * index;

  if(fseek(fp, offset, SEEK_SET) != 0)
  {
    return -3;
  }

  if(fread(ch, 1, col_count * font_size, fp) != (col_count * font_size))
  {
    return -4;
  }

  fclose(fp);
  
  // 轉置矩陣
  // 數據水平，位元組垂直
  part_size = (font_size / col_count);
  memset(ch_transform, 0, sizeof(ch_transform));
    int part, i, j, k; 
  for(part=0; part<col_count; part++)
  {
    for(i=(part * part_size); i<((part + 1) * part_size); i++)
    {
      for(j=0; j<col_count; j++)
      {
        for(k=0; k<8; k++)
        {
          int v = !(!(ch[(i * col_count) + j] & (1 << (7-k))));

          if( v != 0)
          {
            ch_transform[((part_size * part) * col_count) + (j * 8) + k] |= (1 << (i % 8));
          }
        }
      }
    }
  }

  sign_num = -1;
  //memcpy(ch, ch_transform, sizeof(ch_transform));
    memcpy(&g_Marquee_Data[setAdder], ch_transform, sizeof(ch_transform));
    //memcpy(&g_Marquee_Data[i], &g_Font_Data[Word[i]], 128);
  
  return 0;
}

int getFontIndex(unsigned char *sz, int *p_font_index)
{
  int index = 0;

  
  if(sz[0] < 0x80)
  {
    *p_font_index = sz[0];
    return ASCFONT;
  }
  else if((sz[0] >= 0xA1) && (sz[0] <= 0xA3))
  {
    index += (sz[0] - 0xA1) * ((0x7E - 0x40 + 1) + (0xFE - 0xA1 + 1));

    if(sz[1] < 0xA1)
    {
      index += sz[1] - 0x40;
    }
    else
    {
      index += sz[1] - 0xA1 + (0x7E - 0x40 + 1);
    }

    *p_font_index = index;
    return SPCFONT;
  }
  else if((sz[0] >= 0xA4) && (sz[0] <= 0xC6))
  {
    index += (sz[0] - 0xA4) * ((0x7E - 0x40 + 1) + (0xFE - 0xA1 + 1));

    if(sz[1] < 0xA1)
    {
      index += sz[1] - 0x40;
    }
    else
    {
      index += sz[1] - 0xA1 + (0x7E - 0x40 + 1);
    }

    if(sz[0] == 0xC6)
    {
      index = sz[1] - 0xA1;

      *p_font_index = index;
      return SPCFSUPP;
    }

    *p_font_index = index;
    return STDFONT;
  }
  else if((sz[0] >= 0xC9) && (sz[0] <= 0xF9))
  {
    index += (0xC6 - 0xA4) * ((0x7E - 0x40 + 1) + (0xFE - 0xA1 + 1));
    index += (0x7E - 0x40 + 1);


    index += (sz[0] - 0xC9) * ((0x7E - 0x40 + 1) + (0xFE - 0xA1 + 1));
    if(sz[1] < 0xA1)
    {
      index += sz[1] - 0x40;
    }
    else
    {
      index += sz[1] - 0xA1 + (0x7E - 0x40 + 1);
    }

    *p_font_index = index;
    return STDFONT;
  }
  else if((sz[0] >= 0xC6) && (sz[0] <= 0xC8))
  {
    index += (0xFE - 0xA1 + 1);
    index += (sz[0] - 0xC7) * ((0x7E - 0x40 + 1) + (0xFE - 0xA1 + 1));


    if(sz[1] < 0xA1)
    {
      index += sz[1] - 0x40;
    }
    else
    {
      index += sz[1] - 0xA1 + (0x7E - 0x40 + 1);
    }

    *p_font_index = index;
    return SPCFSUPP;
  }

  *p_font_index = 0;
  return FONT_NONE;
}

