#ifndef __OLEDLIB_H
#define __OLEDLIB_H

#ifdef __cplusplus
extern "C"
{
#endif
extern char g_flagMarqueeStop;
extern int8_t OLED_Open(void);
extern void OLED_Close(void);
extern void inital(void);
extern void CleanDDR(void);
extern void OLEDShowFont_16X32(uint8_t cLine, uint8_t cColumn, uint32_t iLen, uint8_t *cpString);
extern void OLEDShowFont_32X32(uint8_t cLine, uint8_t cColumn, uint32_t iLen, uint8_t *cpString);
extern void Run_Marquee_32X32_API(int8_t Word_conut, int8_t * Word);
extern void runBalance_API(int Balance);
#ifdef __cplusplus
}
#endif

#endif

